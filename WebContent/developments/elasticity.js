var circleRadius = 20;
var intersectionsId = 0;

var circleDict = {};
var markerDict = {};

var mymap;
var ctsLayerGroup,bgsLayerGroup;
var pendingCTAjax,pendingBGAjax;
var selectedDevUnit = "";
var selectedDevLayer;

var ctSelected = true;
// keep track of counties and their ajax calls
var counties = {};
var info;

var MAX_POP = 10000;
var MAX_JOBS = 10000;

var geoserverUrl = '/geoserver/ows';

$(document).ready(function() {
	// set various listeners ...

	/*   Unit Type Listener */
	$('input[type=radio][name=unitType]').change(function() {
		ctSelected = (this.value == 'ct');		 	
		clearDevUnit();
		if (ctSelected) {
			bgsLayerGroup.removeFrom(mymap);
			ctsLayerGroup.addTo(mymap);
			$("#bgRow").hide();
        }
        else  {
        	ctsLayerGroup.removeFrom(mymap);
        	bgsLayerGroup.addTo(mymap);
			$("#bgRow").show();
        }
	});
	
	/* populate counties on state choice  */
	$('#stateExtentSelect').change(function() {
		$('#countyExtentSelect').empty();
		loadCounties();
	});

	function loadCounties() {
		var stateFP = $('#stateExtentSelect').val();
		$.get({
			url : geoserverUrl,
			data : {
				service : 'WFS',
				version : '1.0.0',
				typeName : 'elasticity:cb_2013_us_county',
				request : 'GetFeature',
				PROPERTYNAME : 'NAME,GEOID,STATEFP',
				CQL_FILTER : 'STATEFP=' + stateFP,
				outputFormat : 'application/json',
			},
			dataType : 'json',		
			success : function(response) {
				var array = response.features;
				array.sort(function(o1, o2) { 
					return o1.properties.NAME > o2.properties.NAME ? 1 : 
						o1.properties.NAME < o2.properties.NAME ? -1 : 
							0; 
					});
				
				// add a 'no-choice' option
				$('#countyExtentSelect').append("<option selected disabled></option>");
				for (var i = 0; i < array.length; i++)
					$('#countyExtentSelect').append($("<option />").val(array[i].properties.GEOID).text(array[i].properties.NAME));
			},		
			error : function(result) {
				showDialog("dlgErrorLoadCounties");
			}
		});
	}
		
	/************************************************************/
	/*   	County Selection for Extent 						*/
	/************************************************************/
	
	$('#addCounty').click(function() {
		//console.log("nb units before " + getNbUnits());
		var stateVal = $('#stateExtentSelect option:selected').val();
		var stateText = $('#stateExtentSelect option:selected').text();
		var countyVal = $('#countyExtentSelect option:selected').val();
		var countyText = $('#countyExtentSelect option:selected').text();

		if (stateVal == null || stateVal == '' || countyVal == null || countyVal == '')
			showDialog("dlgNoCountySelected");
		else {			
			if (countyVal in counties)
				showDialog("dlgCountyDuplicate");		
			// this is a new county, add it
			else {			
				// clear all selections from selectedCountiesAnalysis
				$("#selectedCountiesAnalysis option:selected").prop("selected", false);
				// add new item as selected (to help the immediate removal if wrong)
				$('#selectedCountiesAnalysis').append($('<option selected></option>').val(countyVal).html(countyText + " (" + stateText + ")"));
				
				counties[countyVal] = new Object();
				// get CTs for this county
				addCountyUnitsToLayerGroup(countyVal,stateText,countyText,true);
				// get BGs for this county
				addCountyUnitsToLayerGroup(countyVal,stateText,countyText,false);
				info.update();
			}
		}
	});
	
	// when a county is removed from the list of counties defining the analysis extent
	$('#removeCounties').click(function() {
		var visibleLayerGroup = ctSelected?ctsLayerGroup:bgsLayerGroup; 
		
		// for each county selected for removal (there could be many)
		$("#selectedCountiesAnalysis option:selected").each(function() {
			var countyFips = $(this).val();					
			$(this).remove();
			
			// abort the ajax call in case it hasn't completed. If it has, nothing will happen
			counties[countyFips].ajaxCT.abort();
			counties[countyFips].ajaxBG.abort();
			// remove the county from the dictionary
			delete counties[countyFips];
			
			// remove selected from both groups
			ctsLayerGroup.removeLayer(countyFips);				
			bgsLayerGroup.removeLayer(countyFips);				
			
			// clear the dev unit (only) if its county is removed
			// if (selectedDevUnit.startsWith(countyFips))  --> doesn't work on IE
			if (selectedDevUnit.lastIndexOf(countyFips, 0) === 0)				
				clearDevUnit();
		});

		// reset the zoom to what is left
		if (visibleLayerGroup.getLayers().length>0)
			mymap.fitBounds(visibleLayerGroup.getBounds());
		else {
			mymap.setView(new L.LatLng(38.0, -97.0),4);
			info.update();
		}
		// set the last option as selected
		$("#selectedCountiesAnalysis option:last").prop("selected", true);
	});

	/* End County Selection */
	
	// when a county is removed from the list of counties defining the analysis extent
	$('#clearDevUnit').click(clearDevUnit);
	
	function clearDevUnit() {
		// if a unit is selected ... (this way we can call this method anytime
		if (selectedDevUnit!="") {
			selectedDevLayer.setStyle(getDefaultStyle());
			selectedDevUnit = "";
			selectedDevLayer = null;

			$("#devUnitState").html("");
			$("#devUnitCounty").html("");
			$("#devUnitCT").html("");
			$("#devUnitBG").html("");

			mymap.off('click', intersectionClick);
			info.update();
		}
	}

	// make an image click in the intersection table remove its row	
	$('#intersectionsTable').on('click', 'img', function(e){
		var id = $(this).closest('td').next().html();
		deleteIntersectionFromTable(this);
		mymap.removeLayer(circleDict[id]);
		delete circleDict[id];
		mymap.removeLayer(markerDict[id]);
		delete markerDict[id];
	});

	/************************************************************/
	/*   					Form Submission 					*/
	/************************************************************/
	$('#submit').click(function() {
		if ($('#selectedCountiesAnalysis  > option').length < 1)
			showDialog("dlgSubmitNoCounty");
		
		else if (selectedDevUnit=="")
			showDialog("dlgSubmitNoDevUnit");
		
		else if (!/^[0-9]+$/.test($('#popInput').val()) || $('#popInput').val()>MAX_POP)
			showDialog("dlgSubmitInvPop","maxPop",MAX_POP);

		else if (!/^[0-9]+$/.test($('#jobsInput').val()) || $('#jobsInput').val()>MAX_JOBS)
			showDialog("dlgSubmitInvJobs","maxJobs",MAX_JOBS);
		
		else if ($('#popInput').val()==0 && $('#jobsInput').val()==0)
			showDialog("dlgSubmitJobsPopZero");

		else if (jQuery.isEmptyObject(markerDict)) 
			showDialog("dlgSubmitNoIntersection");
	
		else if (!allIntersectionsHaveValidDelays())
			showDialog("dlgEmptyDelay");

		else if (!isEmail($('#email').val())) 
			showDialog("dlgSubmitNoEmail");

		else if (!jQuery.isNumeric($('#impedance').val()) || $('#impedance').val()<=0 || $('#impedance').val()>1) 
			showDialog("dlgSubmitInvImpedance");
		
		else {
			// count directly the number of units. Could do this from layers I think ...
			var nbUnits = getNbUnits();		
			if (nbUnits>1500 && nbUnits<=5000)
				showDialogWithLimitOptionAndSubmit();
			else if (nbUnits>5000)
				showDialog("dlgOversizeJob","nbUnits",nbUnits);
			else
				submitForm();
		}
	});

	function submitForm() {
		// set the 2 hidden fields of the form, the 3rd one gets set only if needed
		$("#devUnitFips").val(selectedDevUnit);
		$("#maxIntersectionIndex").val(intersectionsId);
		// select all elements in the multi-select so that they get sent properly
		$('#selectedCountiesAnalysis option').prop('selected', true);
		$('#theForm').submit();		
	}
	
	function allIntersectionsHaveValidDelays() {
		var allValid = true;
		$("input[name^='delay']").each(function(index,value) {
			allValid = jQuery.isNumeric($(this).val());
		    return allValid;
		});
		return allValid;
	}

	function isEmail(email) {
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
	}	
	
	
	/* End Form Submission */

	/************************************************************/
	/*   					End Listeners	 					*/
	/************************************************************/

	/************************************************************/
	/*   					Load Map		 					*/
	/************************************************************/
	
	// load the map
	mymap = L.map('mapid',{center: new L.LatLng(38.0, -97.0), zoom: 4});
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={token}',
		{
			maxZoom : 18,
			attribution : 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, '
					+ '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, '
					+ 'Imagery © <a href="http://mapbox.com">Mapbox</a>',
			id : 'mapbox.streets',
			token: 'pk.eyJ1IjoiZ3VpYmFyIiwiYSI6ImNqMWZma2JjYjAwMTIzM25xM3h2c3Z5ZXQifQ.F7BsyHbihysrgg6G0FMaSQ'
		}).addTo(mymap);

	
//	L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
//	    maxZoom: 18,
//	    subdomains:['mt0','mt1','mt2','mt3']
//	}).addTo(mymap);
		
	/************************************************************/
	/*   					Info Window		 					*/
	/************************************************************/
	// control that gives instructions
	info = L.control();
	info.onAdd = function (map) {
		this._div = L.DomUtil.create('div', 'info');
		this.update();
		return this._div;
	};

	// control that shows state info on hover
	info2 = L.control();
	info2.onAdd = function (map) {
		this._div = L.DomUtil.create('div', 'info2');
		return this._div;
	};

	info.update = function (layer) {
		if  ($('#selectedCountiesAnalysis  > option').length < 1 ) 
			this._div.innerHTML = 'Add one or more counties to the Analysis';
		else if (selectedDevUnit!="")
			this._div.innerHTML = 'Zoom in and click on delayed intersections';
		else if (layer)
			this._div.innerHTML = 'Click on unit to select it';
		else 
			this._div.innerHTML = 'Hover over displayed units';			
		info2.update(layer,selectedDevUnit=="");
	};
	info2.update = function (layer,show) {
		if (show && layer) {
			$('.info2').css("visibility", "visible");
			this._div.innerHTML = getText(layer);
		}
		else {
			$('.info2').css("visibility", "hidden");
		}
	}
	
	info.addTo(mymap);
	info2.addTo(mymap)

	function getText(layer) {
		var props = layer.feature.properties;
		if (layer.ISCT)
			return 'State: ' + layer.STATENAME + '<br>County: ' + layer.COUNTYNAME 
			+ '<br>Census Tract: ' + props.NAME;
		else
			return 'State: ' + layer.STATENAME + '<br>County: ' + layer.COUNTYNAME 
			+ '<br>Census Tract: ' + props.TRACTCE + '<br>Block Group: ' + props.BLKGRPCE;
	}
	
	/************************************************************/
	/*   					Init On Census Tracts				*/
	/************************************************************/

	ctsLayerGroup = L.featureGroup();
	bgsLayerGroup = L.featureGroup();
    ctsLayerGroup.addTo(mymap);
	$("#bgRow").hide();

	/************************************************************/
	/*   					Annoyance							*/
	/************************************************************/
    
	// need this to fix the bug whereby the map loads only half way and then the drawing of CTs gets problematic too ..
	setTimeout(function() {
	    mymap.invalidateSize();
	}, 100);
	
});

// can be called on submit to check how big the job is.
function getNbUnits() {
	var nbUnits = 0;
	var layerGroup = ctSelected?ctsLayerGroup:bgsLayerGroup;
	
	layerGroup.eachLayer(function (countyLayer) {
		nbUnits += countyLayer.getLayers().length;
	});		
	return nbUnits;
}

/************************************************************/
/*   					Dialogs 							*/
/************************************************************/

function showDialog(idSelector,spanClass,spanValue) {
	var dialog = $("#"+idSelector)
	.dialog({
	    autoOpen: false,
	    modal: true,
	    buttons: {
	      "OK": function() {$(this).dialog("close");}
	    }
	});
	if (!(typeof spanClass === 'undefined')) { 
		$("#"+idSelector + " ." + spanClass).html(spanValue);
	}
	dialog.dialog('open');
}

function showDialogWithLimitOptionAndSubmit() {
	var dialog = $("#dlgSampleUnits")
	.dialog({
	    autoOpen: false,
	    modal: true,
	    buttons: {
	    	"Submit Job": function() {
	    		// set the hidden value of the form if the cb was ticked
	    		if ($("#sampleCheckbox").checked)
	    			$("#sampleHidden").val("yes");
	    		submitForm();
	    	},
	    	"Cancel": function() {$(this).dialog("close");},
	    }
	});
	$("#dlgSampleUnits" + " .nbUnits").html(ctSelected?nbCTs:nbBGs);
	dialog.dialog('open');	
}


/************************************************************/
/*   					Map functions 						*/
/************************************************************/

function addCountyUnitsToLayerGroup(countyFP,stateName,countyName,unitCT) {
	// only display the spin on the layer group currently displayed otherwise
	// it might be displayed after the visible group has loaded and the invisible hasn't
	if (unitCT==ctSelected)
		mymap.spin(true);
	
	var typeNameV = unitCT?'elasticity:cb_2013_us_cts':'elasticity:cb_2013_us_bg';
	var propertyNameV = unitCT?'the_geom,NAME,GEOID,STATEFP,COUNTYFP':'the_geom,GEOID,STATEFP,COUNTYFP,TRACTCE,BLKGRPCE';
	// the layer group to which layers are added depends on the type of unit
	var layerGroup = unitCT?ctsLayerGroup:bgsLayerGroup;

	// the ajax to be stored in the county object
	var ajax = $.get({
		url : geoserverUrl,
		data : {
			service : 'WFS',
			version : '1.0.0',
			typeName : typeNameV,
			request : 'GetFeature',
			PROPERTYNAME : propertyNameV,
			CQL_FILTER : 'STATEFP=' + countyFP.substring(0,2) + ' and COUNTYFP=' + countyFP.substring(2,5),
			outputFormat : 'application/json',
		},
		dataType : 'json',
		success : function(response) {
			var countyLayer = L.geoJson(response,{
				style: getDefaultStyle,
				onEachFeature: onEachUnit
			});
			countyLayer._leaflet_id = countyFP;
						
			layerGroup.addLayer(countyLayer);
			
			// if we are dealing with the visible group, stop the spin and adjust the bounds
			if (unitCT==ctSelected) {
				mymap.spin(false);
				mymap.fitBounds(layerGroup.getBounds());
			}
			// console.log("nb units after " + getNbUnits());
		},
		error: function (xhr, text_status, error_thrown) {
			if (unitCT==ctSelected)
				mymap.spin(false);
			// only show if the error is not called because of the abort call in "remove"
            if (text_status != "abort") {
            	showDialog("dlgErrorLoadMap");
            }
        }
	});
	// add the ajax objects to the county object
	if (unitCT)
		counties[countyFP].ajaxCT=ajax;
	else
		counties[countyFP].ajaxBG=ajax;
	
	function onEachUnit(feature, layer) {
		// each unit get the state, the county and its type
		layer.STATENAME = stateName;
		layer.COUNTYNAME = countyName;
		layer.ISCT = unitCT;
		
		layer.on({
			mouseover: function (e) {
				var layer = e.target;
				if (selectedDevUnit == "")
					layer.setStyle(getHighlightedStyle());
				info.update(layer);
		    },
			mouseout: function (e) {
				var layer = e.target;
				if (selectedDevUnit == "")
					layer.setStyle(getDefaultStyle());
				info.update();
		    },
			click: clickOnUnit
		});	
	}
}

function clickOnUnit(e) {
	// if a unit has already been selected, do nothing
	if (selectedDevUnit == "") {
		var layer = e.target;
		selectedDevUnit = layer.feature.properties.GEOID;
		selectedDevLayer = layer;
		
		// highlight choice on map
		layer.setStyle(getSelectedStyle());
	
		// not sure if I need this ...
		if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
			layer.bringToFront();
		}
		
		// indicate value in box
		$("#devUnitState").html(layer.STATENAME);
		$("#devUnitCounty").html(layer.COUNTYNAME);
		$("#devUnitCT").html(layer.ISCT?layer.feature.properties.NAME:layer.feature.properties.TRACTCE);
		$("#devUnitBG").html(layer.ISCT?"":layer.feature.properties.BLKGRPCE);
		
		// zoom on unit
		mymap.fitBounds(layer.getBounds());
		
		// new clicks will define intersections
		mymap.on('click', intersectionClick);
		info.update();
	}
}

function getDefaultStyle() {
	return {
		weight: 2,
		opacity: 0.8,
		color: '#555',
//		dashArray: '3',
		fillOpacity: 0,
	};
}

function getHighlightedStyle() {
	return {
		weight: 5,
		color: '#666',
		dashArray: '',
		fillOpacity: 0.3
	};
}

function getSelectedStyle() {
	return {
		weight: 5,
		color: 'red',
		dashArray: '',
//		fillColor: 'red',
		fillOpacity: 0
	};
}

/************************************************************/
/*   					Intersections 						*/
/************************************************************/

function addIntersection(id,latlng) {	
	var circle = L.circle(latlng, circleRadius, {
		color: 'red',
		fillColor: 'red',
		fillOpacity : 0.2
	}).addTo(mymap);

	var myIcon = L.divIcon({
		className: 'inters-icon',
	    iconSize: [16, 26],
		html: id
	});

	var marker = L.marker(latlng, {icon: myIcon}).addTo(mymap);
	circleDict[id] = circle;
	markerDict[id] = marker;
	addIntersectionToTable(id,latlng);
}

/* Table functions */
function addIntersectionToTable(id,latlng) {
	// if 1st td of last row is empty
	if ($('#intersectionsTable tr:last td:first').find("img").length == 0)
		$('#intersectionsTable tr:last').remove();
	
	var row = "<tr><td><img class=\"delete\" src=\"./bin.png\" width=\"22px\"></td>" +
			"<td class=\"grey\">" + id + "</td>" +
			"<td><input name=\"desc" + id + "\" size=\"30\" type=\"text\" value=\"\" ></td>" +
			"<td class=\"grey\">" + latlng.lat.toFixed(6) + 
			" <input type=\"hidden\" name=\"lat" + id + "\" value=\"" + latlng.lat.toFixed(6) + "\"></td>" +
			"<td class=\"grey\">" + latlng.lng.toFixed(6) + 
			" <input type=\"hidden\" name=\"long" + id + "\" value=\"" + latlng.lng.toFixed(6) + "\"></td>" +
			"<td><input name=\"delay" + id + "\" size=\"3\" type=\"text\" required > (s)</td></tr>";
	$('#intersectionsTable tr:last').after(row);
}

function deleteIntersectionFromTable(imgClicked) {
   	$(imgClicked).closest('tr').remove();
   	// if this was the last intersection, add an empty line
	if ($('#intersectionsTable tr').length==1) {
		$('#intersectionsTable tr:last').after("<tr><td></td><td></td><td></td><td></td><td </td><td>&nbsp;</td></tr>");		
		intersectionsId=0;
	}
}

/* define intersection with click */
function intersectionClick(e) {
	intersectionsId++;
	addIntersection(intersectionsId,e.latlng);				
	mymap.spin(true);
	
	// check if we can find an intersection relating to this point
	$.get({
		url : 'http://api.geonames.org/findNearestIntersectionJSON',
		data : {
			username : 'elasticity',
			lat : e.latlng.lat,
			lng : e.latlng.lng
		},
		dataType : 'json',
		success : function(data) {
			mymap.spin(false);
			if (data.hasOwnProperty('intersection')) {
				$("input[name=\"desc" + intersectionsId + "\"]").val(data.intersection.street1 + '/' + data.intersection.street2);
				if (data.intersection.distance > 0.01) 
					showDialog("dlgTooFarIntersection","distIntersection",data.intersection.distance*1000);
			}
			else 
				showDialog("dlgInvalidIntersection");
		},
		// if we can't check the intersection, add it "as is".
		error : function(result) {
			mymap.spin(false);
			showDialog("dlgIntersectionNoCheck");
			circle.setStyle({
				color: 'red',
				fillColor: 'red',
				fillOpacity : 0.2
			});
		},
		// ensures that this returns. but it is deprecated 
		// async: false
	});
}
