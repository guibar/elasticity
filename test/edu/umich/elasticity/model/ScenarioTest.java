
package edu.umich.elasticity.model;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.umich.elasticity.NoPathFoundException;
import edu.umich.elasticity.OSMService;
import edu.umich.elasticity.OTPWrapper;
import edu.umich.elasticity.TestCentral;
import edu.umich.elasticity.model.Coordinate;
import edu.umich.elasticity.model.DelayedIntersectionZone;
import edu.umich.elasticity.model.Scenario;
import edu.umich.elasticity.model.UnknownIntersectionException;
import junitx.framework.FileAssert;

public class ScenarioTest {
    private static final Logger LOG = LoggerFactory.getLogger(ScenarioTest.class);
    static double highImpedance = 0.28/60;
	static String testEmail = "g.barreau@gmail.com";
	File washtenawTestFiles = new File(TestCentral.testResources,"washtenawCounty");		


	@Before
	public void setDir() {
	    Scenario.workDir = new File(TestCentral.testOutput,"work");
	}

	/* Fips 18127980002 has no value for jobs and population is 0 */
	@Test
	public void testNullJobsValueScenario() throws Exception {
		Scenario s = new Scenario(18127980002L);
		s.run();
	}

	@Test
	public void testDiff() throws IOException, ClassNotFoundException, SQLException, UnknownIntersectionException {
		long[][] a = new long[][]{{3L,3L,3L},{3L,3L,3L},{3L,3L,3L}};
		long[][] b = new long[][]{{3L,3L,3L},{2L,2L,2L},{2L,2L,2L}};
		long[][] c = Results.diff(a, b);
		assertEquals(0,c[0][2]);
		assertEquals(1,c[1][2]);
		assertEquals(1,c[1][1]);
		assertEquals(1,c[2][2]);
		// check that a hasn't been modified
		assertEquals(3,a[2][2]);
	}

	@Test
	public void testCalcAccessibility() throws IOException, ClassNotFoundException, SQLException, UnknownIntersectionException {
		long[][] travelTimes = new long[][]{{3L,3L,3L},{3L,3L,3L},{3L,3L,3L}};
		int[] jobs = new int[]{2,2,2};
		int[] population = new int[]{3,3,3};
		double[] result = new double[3];
		double impedance = 1.0;
		
		Results.calculateSumsAndAccessibility(travelTimes,jobs,population,result,impedance);
		double r = Math.exp(-3*impedance)*2;
		for(int i=0;i<3;i++)
			assertEquals(3*r, result[i],0.0001);

		impedance = 2.0;
		Results.calculateSumsAndAccessibility(travelTimes,jobs,population,result,impedance);
		r = Math.exp(-3*impedance)*2;
		for(int i=0;i<3;i++)
			assertEquals(3*r, result[i],0.0001);
		
		travelTimes = new long[][]{{1L,2L,3L},{4L,5L,6L},{7L,8L,9L}};
		jobs = new int[]{12,13,14};
		impedance = 2.0;
		Results.calculateSumsAndAccessibility(travelTimes,jobs,population,result,impedance);
		r = Math.exp(-1*impedance)*jobs[0] + Math.exp(-2*impedance)*jobs[1] + Math.exp(-3*impedance)*jobs[2];
		assertEquals(r, result[0],0.0001);
		r = Math.exp(-4*impedance)*jobs[0] + Math.exp(-5*impedance)*jobs[1] + Math.exp(-6*impedance)*jobs[2];
		assertEquals(r, result[1],0.0001);
		r = Math.exp(-7*impedance)*jobs[0] + Math.exp(-8*impedance)*jobs[1] + Math.exp(-9*impedance)*jobs[2];
		assertEquals(r, result[2],0.0001);
	}
	
	@Test
	public void testSetBoundingBoxAndStates() throws Exception {
		Scenario dummyScenario;
		ArrayList<Integer> counties = new ArrayList<Integer>();
		//Steuben, Indiana
		counties.add(18151);
		// Williams, Ohio
		counties.add(39171);
		//Hillsdale, MI
		counties.add(26059);
		ArrayList<DelayedIntersectionZone> dummy = new ArrayList<DelayedIntersectionZone>();
		dummyScenario = new Scenario("g.barreau@gmail.com",counties,39171950100L,3,3,highImpedance,dummy,true,false);
		
		assertEquals(-85.196774-Scenario.degreesLonBuffer,dummyScenario.boundingBox.lonWest,0.00001);
		assertEquals(41.426027-Scenario.degreeLatBuffer,dummyScenario.boundingBox.latSouth,0.00001);
		assertEquals(-84.341664+Scenario.degreesLonBuffer,dummyScenario.boundingBox.lonEast,0.00001);
		assertEquals(42.073456+Scenario.degreesLonBuffer,dummyScenario.boundingBox.latNorth,0.00001);
		
		ArrayList<String> expectedStates = new ArrayList<String>();
		expectedStates.add("in");expectedStates.add("mi");expectedStates.add("oh");
		assertEquals(expectedStates,dummyScenario.statesUSPS);
	}
	
	@Test
	public void testPopulateAnalysisUnits() throws Exception {
		Scenario scenario;
		ArrayList<Integer> counties = new ArrayList<Integer>();
		// Cook county has 3993 BGs
		counties.add(17031);
		
		// Census Tracts
		// 17031804310L is 5th most populous CT
		scenario = new Scenario("g.barreau@gmail.com",counties,17031804310L,10,0,highImpedance,null,true,false);
		assertEquals(1319,scenario.nbUnits);
		assertEquals(786,scenario.modUnitIndex);

		scenario = new Scenario("g.barreau@gmail.com",counties,17031804310L,10,0,highImpedance,null,true,true);
		assertEquals(1000,scenario.nbUnits);
		assertEquals(4,scenario.modUnitIndex);

		// 17031835700L is not within the 1000 most populous CTs
		scenario = new Scenario("g.barreau@gmail.com",counties,17031835700L,10,0,highImpedance,null,true,false);
		assertEquals(1319,scenario.nbUnits);
		assertEquals(1247,scenario.modUnitIndex);

		// hence it gets added as the last of the 1000
		scenario = new Scenario("g.barreau@gmail.com",counties,17031835700L,10,0,highImpedance,null,true,true);
		assertEquals(1000,scenario.nbUnits);
		assertEquals(999,scenario.modUnitIndex);

		// Block Groups
		// this is number 10 as returned in order of decreasing pop 
		scenario = new Scenario("g.barreau@gmail.com",counties,170318300043L,10,0,highImpedance,null,false,false);
		assertEquals(3993,scenario.nbUnits);
		assertEquals(3689,scenario.modUnitIndex);

		scenario = new Scenario("g.barreau@gmail.com",counties,170318300043L,10,0,highImpedance,null,false,true);
		assertEquals(1000,scenario.nbUnits);
		assertEquals(9,scenario.modUnitIndex);

		// 170318357001L is not within the 1000 most populous CTs
		scenario = new Scenario("g.barreau@gmail.com",counties,170318357001L,10,0,highImpedance,null,false,false);
		assertEquals(3993,scenario.nbUnits);
		assertEquals(3827,scenario.modUnitIndex);

		// hence it gets added as the last of the 1000
		scenario = new Scenario("g.barreau@gmail.com",counties,170318357001L,10,0,highImpedance,null,false,true);
		assertEquals(1000,scenario.nbUnits);
		assertEquals(999,scenario.modUnitIndex);

	}
	
	@Test
	public void testMultipleScenarios() throws Exception {
		Scenario s2 = LakeCountyTest.setLakeCountyScenario(Scenario.defaultImpedance,true);
		Thread.sleep(3000);
		Scenario s1 = WashtenawCountyTest.setWashtenawCounty();
		
		ExecutorService es = Executors.newCachedThreadPool();
		es.execute(s1);
		Thread.sleep(10000);
		es.execute(s2);
		es.shutdown();
		es.awaitTermination(15, TimeUnit.MINUTES);
		
		File lakeTestFiles = new File(TestCentral.testResources,"lakeCounty/ct");		

		FileAssert.assertEquals(new File(lakeTestFiles,Results.fileNames[0]), s2.results.outputFiles[0]);
		FileAssert.assertEquals(new File(lakeTestFiles,Results.fileNames[1]), s2.results.outputFiles[1]);
		FileAssert.assertEquals(new File(lakeTestFiles,Results.fileNames[2]), s2.results.outputFiles[2]);
		FileAssert.assertEquals(new File(lakeTestFiles,Results.fileNames[3]), s2.results.outputFiles[3]);
	}
	
	/* I had an issue whereby the adding of delays was causing a quicker route to be found. The reason was 
	 * that it is not the duration that is being minimized but the weight. And the weigth differed from the time
	 * in 2 ways. It was not being rounded as the time is and the walkreluctance factor was interfering with it by
	 * multiplying it by 2. It was still roughly proportional but the small differences were enough to generate
	 * routes that were not optimal with respect to time while being optimal with respect to weight.
	 */
	@Test
	public void testMininumFound() throws Exception {
		int washtenawFips = 26161;
		long modCtFips = 26161403600L;

		ArrayList<Integer> counties = new ArrayList<Integer>();
		counties.add(washtenawFips);
		
		Scenario s = new Scenario("g.barreau@gmail.com",counties,modCtFips,0,0,highImpedance,null,true,false);
		OSMService.generateOSMFile(s);
		s.router = new OTPWrapper(s.directory, s.getName(),s.ps);
		Coordinate A = new Coordinate(42.227651,-83.589049);
		Coordinate B = new Coordinate(42.323806,-83.698481);
		Coordinate C = new Coordinate(42.294117,-83.736184);
		
		assertTrue(s.router.calculateTrip(A,C)<=
				s.router.calculateTrip(A,B) + s.router.calculateTrip(B,C));
	}
	
	
	@Test(expected=NoPathFoundException.class)
	public void testIncompleteGraph() throws Exception {
		int washtenawFips = 26161;
		long modCtFips = 26161403600L;
		int popAdded = 738;
		int jobsAdded = 0;
		ArrayList<Integer> counties = new ArrayList<Integer>();
		counties.add(washtenawFips);
		Scenario s = new Scenario("g.barreau@gmail.com",counties,modCtFips,jobsAdded,popAdded,highImpedance,null,true,false);
		// the west of the bounding box must be set to -84.26 for this to work. See test below.
		// gh, hi and no also fail
		s.boundingBox = new BoundingBox(42.28,-83.90, 42.32,-84.259);
		OSMService.generateOSMFile(s);
		s.router = new OTPWrapper(s.directory, s.getName(),s.ps);
		Coordinate g = new Coordinate(42.292444100000004,-84.0695108);
		Coordinate h = new Coordinate(42.292143,-84.07381550000001);
		LOG.info("gh " + s.router.calculateTrip(g,h));					
	}

	@Test
	public void testCompleteGraphGoingFurtherWest() throws Exception {
		int washtenawFips = 26161;
		long modCtFips = 26161403600L;
		int popAdded = 738;
		int jobsAdded = 0;
		ArrayList<Integer> counties = new ArrayList<Integer>();
		counties.add(washtenawFips);
		
		Scenario s = new Scenario("g.barreau@gmail.com",counties,modCtFips,jobsAdded,popAdded,highImpedance,null,true,false);
		s.boundingBox = new BoundingBox(42.28,-83.90, 42.32,-84.26);
		
		OSMService.generateOSMFile(s);
		s.router = new OTPWrapper(s.directory, s.getName(),s.ps);

		Coordinate a = new Coordinate(42.298303700000005,-83.96987100000001);
		Coordinate b = new Coordinate(42.2977733,-83.9779917);
		Coordinate c = new Coordinate(42.2947032,-84.0270618);
		Coordinate d = new Coordinate(42.294593000000006,-84.03403580000001);
		Coordinate e = new Coordinate(42.2928307,-84.0641027);
		Coordinate f = new Coordinate(42.2924978,-84.06880070000001);
		Coordinate g = new Coordinate(42.292444100000004,-84.0695108);
		Coordinate h = new Coordinate(42.292143,-84.07381550000001);
		Coordinate i = new Coordinate(42.296442400000004,-84.089923);
		Coordinate j = new Coordinate(42.298562100000005,-84.0939115);
		Coordinate k = new Coordinate(42.2977422,-84.09392190000001);
		Coordinate l = new Coordinate(42.2970842,-84.09391310000001);
		Coordinate m = new Coordinate(42.2959319,-84.0938809);
		Coordinate n = new Coordinate(42.2946168,-84.0894279);
		Coordinate o = new Coordinate(42.293481500000006,-84.08827210000001);
		Coordinate p = new Coordinate(42.2930198,-84.08768);
		Coordinate q = new Coordinate(42.2916879,-84.0768247);
		Coordinate r = new Coordinate(42.2916879,-84.0768247);
		
		LOG.info("ab " + s.router.calculateTrip(a,b));
		LOG.info("bc " + s.router.calculateTrip(b,c));
		LOG.info("cd " + s.router.calculateTrip(c,d));
		LOG.info("de " + s.router.calculateTrip(d,e));
		LOG.info("ef " + s.router.calculateTrip(e,f));
		LOG.info("fg " + s.router.calculateTrip(f,g));
		LOG.info("gh " + s.router.calculateTrip(g,h));			
		LOG.info("hi " + s.router.calculateTrip(h,i));
		LOG.info("ij " + s.router.calculateTrip(i,j));
		LOG.info("jk " + s.router.calculateTrip(j,k));
		LOG.info("kl " + s.router.calculateTrip(k,l));
		LOG.info("lm " + s.router.calculateTrip(l,m));
		LOG.info("mn " + s.router.calculateTrip(m,n));
		LOG.info("no " + s.router.calculateTrip(n,o));
		LOG.info("op " + s.router.calculateTrip(o,p));
		LOG.info("pq " + s.router.calculateTrip(p,q));
		LOG.info("qr " + s.router.calculateTrip(q,r));		
	}


	@Test
	public void test3CountiesScenario() throws Exception {
		int lakeFips = 26085;
		long modCtFips = 26085961100L;
		int popAdded = 75;
		int jobsAdded = 0;
		List<DelayedIntersectionZone> intersections = new ArrayList<DelayedIntersectionZone>();	

		intersections.add(new DelayedIntersectionZone("Michigan and Washington",new Coordinate(43.900889, -85.850890),40));
		intersections.add(new DelayedIntersectionZone("Astor and Washington",new Coordinate(43.900950, -85.861533),6));
		intersections.add(new DelayedIntersectionZone("52 and Astor",new Coordinate(43.893451, -85.861747),27));
		intersections.add(new DelayedIntersectionZone("44 and Michigan",new Coordinate(43.908194, -85.851512),14));

		ArrayList<Integer> counties = new ArrayList<Integer>();
		counties.add(lakeFips);
		// to the east of Lake
		int osceolaFips = 26133;
		// to the west of Lake
		int masonFips = 26105;
		counties.add(osceolaFips);
		counties.add(masonFips);
		
		Scenario s = new Scenario("g.barreau@gmail.com",counties,modCtFips,jobsAdded,popAdded,highImpedance,intersections,true,false);		
		s.run();
	}


	
	@Test
	public void testMultiStateScenario() throws Exception {
		ArrayList<Integer> counties = new ArrayList<Integer>();
		//Steuben, Indiana
		counties.add(18151);
		// Williams, Ohio
		//counties.add(39171);
		//Hillsdale, MI
		counties.add(26059);
		
		//CTs at the border of the 3 states
		long modCtFips = 18151970800L;
//		long modCtFips = 39171950200;
//		long modCtFips = 26059051200;
		int popAdded = 85;
		int jobsAdded = 233;
		
		Scenario s = new Scenario("g.barreau@gmail.com",counties,modCtFips,jobsAdded,popAdded,highImpedance,null,true,false);
		OSMService.generateOSMFile(s);
		// In the test routines where we generate random scenarios, 
		// the router has already been set in the constructor
		s.router = new OTPWrapper(s.directory,s.getName(),s.ps);
		s.intersections = s.router.getRandomIntersections(50);
		s.run();
	}

}
