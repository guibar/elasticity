package edu.umich.elasticity.model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.opentripplanner.routing.vertextype.StreetVertex;

import static org.junit.Assert.*;
import edu.umich.elasticity.OSMService;
import edu.umich.elasticity.OTPWrapper;
import edu.umich.elasticity.TestCentral;

public class IntersectionDelayTest {
    static File washtenawDir = new File(TestCentral.testResources,"tc-huron-nixon");		
    static File osmFile = new File(washtenawDir,"tc.osm.pbf");
    static OTPWrapper otp;
	static List<DelayedIntersectionZone> intersections;
	static DelayedIntersectionZone trafficCircleHuronNixon;
	static int delay = 20;
	static Coordinate[] testPointsAround;
	static StreetVertex[] testPointsAroundSV;
	
	long[][] expUndelayed;
	long[][] expDelayed;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		if (!osmFile.exists()) {
			// BB around the Traffic Circle located at (42.305166,-83.707469)
			// This is a pain!! It works with these values but not 42.29,-83.69,42.32,-83.73 which should be
			// plenty of margin around the traffic circle. See Screenshots of BBs
			// BoundingBox bb = new BoundingBox(42.29,-83.69,42.32,-83.73);
			BoundingBox bb = new BoundingBox(42.28,-83.68,42.33,-83.74);
			OSMService.extractAndFilterWithOsmosis(new File(OSMService.osmDataDir,"mi.osm.pbf"),osmFile,bb,true,System.out);
		}
		intersections = new ArrayList<DelayedIntersectionZone>();	
		trafficCircleHuronNixon = new DelayedIntersectionZone("Nixon and Huron",new Coordinate(42.305166,-83.707469),delay);
		intersections.add(trafficCircleHuronNixon);
		
		// OSM:n1556169019
		//Coordinate southNixon = new Coordinate(42.30466,-83.70734);
		// OSM:n62477148
		Coordinate southNixon = new Coordinate(42.302535,-83.707244);

		// OSM:n3290361130
		//Coordinate westHuron = new Coordinate(42.30584,-83.70938);
		//OSM:n62500788
		Coordinate westHuron = new Coordinate(42.308283,-83.713543);

		// OSM:n62525802	
		Coordinate northNixon = new Coordinate(42.30595,-83.70765);
		// OSM:n62500824
		Coordinate eastHuron = new Coordinate(42.30259,-83.70437);
		testPointsAround = new Coordinate[] {northNixon,eastHuron,southNixon,westHuron};
	}

	@Before
    public void setUp() throws IOException, UnknownIntersectionException {
		otp = new OTPWrapper(washtenawDir,"washtenaw",System.out);
		otp.findVertices(intersections);

		testPointsAroundSV = new StreetVertex[testPointsAround.length];
		for(int i=0;i<testPointsAround.length;i++)
			testPointsAroundSV[i] = otp.getNearbyStreetVertex(testPointsAround[i]);
    }
	
	@After
	public void tearDown() throws Exception {
		otp.close();
	}
	
    public void setExpectedResultsNotBatch() throws Exception {
//		  N  E  S  W
//		N 29,86,45,45,
//		E 63,34,60,82,
//		S 40,59,23,43,
//		W 61,80,38,29,		
  	expUndelayed = new long[][]{{2,86,45,45},{63,2,60,82},{40,59,2,43},{61,80,38,2}};

  	// default hypothesis, no delay in diagonals
  	expDelayed = new long[4][4];
  	for(int i=0;i<expDelayed.length;i++)
  		for(int j=0;j<expDelayed[i].length;j++)
  			expDelayed[i][j] = expUndelayed[i][j] + (i==j?0:delay);
  			

  	// actual result differs because better options are found
//* = bypasses
//		N 38,98*,65,65,   N->E
//		E 83,40,60*,102,  E->S
//		S 60,60*,27,43*,  S->E,S->W
//		W 81,100,43*,37,  W->S

		// N -> E it goes via BriarCliff Street -> 98 instead of 86 + 20
		expDelayed[0][1] = expUndelayed[0][1] + (int)trafficCircleHuronNixon.delay - 8;
		// E -> S Going via Plymouth Road and road bordering Plymouth Mall even without delay 	
		expDelayed[1][2] = expUndelayed[1][2];
		// S -> E Going via Plymouth Road and road bordering Plymouth Mall even without delay
		expDelayed[2][1] = expUndelayed[2][1] + (int)trafficCircleHuronNixon.delay - 19;
		// S -> W going via 3290361131 after delay, 60 instead of 59 + 20
		expDelayed[2][3] = expUndelayed[2][3];
		// W -> S, with delay, goes via 3290361129 -> 43 instead of 38 + 20
		expDelayed[3][2] = expUndelayed[3][2] + (int)trafficCircleHuronNixon.delay - 15;
		
		// diagonal terms are calculated as 1/2 average of other 3
  }

    public void correctExpectedResultsBatch() throws Exception {
//		  N  E  S  W
//    	N 28,85,44,44,
//    	E 62,33,59,81,
//    	S 39,58,23,42,
//    	W 60,79,37,29,
    	// actual result differs because better options are found
    	//* = bypasses
//    	N 37,97*,64,64,   N->E
//    	E 82,40,59*,101,  E->S
//    	S 59,59*,26,42*,  S->E,S->W
//    	W 80,99,42*,36,  W->S
    	
    	for(int i=0;i<expDelayed.length;i++)
    		for(int j=0;j<expDelayed[i].length;j++) {
    			expUndelayed[i][j]--;
    			expDelayed[i][j]--;
    		}
    }
    
    @Test
	public void testDelaysOnTrafficCircle() throws Exception {
    	setExpectedResultsNotBatch();
		long[][] results = otp.calculateTimeMatrix(testPointsAround);
		System.out.println(Arrays.deepToString(results));
//		assertArrayEquals(expUndelayed,results);
		
		otp.setDelays(intersections);
		results = otp.calculateTimeMatrix(testPointsAround);		
		System.out.println(Arrays.deepToString(results));
//		assertArrayEquals(expDelayed,results);

		// if we reset the delays, we should get back to starting point
//		otp.zeroDelays(intersections);
//		results = otp.calculateTimeMatrix(testPointsAround);
//		System.out.println(Arrays.deepToString(results));
//		assertArrayEquals(expUndelayed,results);
	}
    
    @Test
	public void testDelaysOnTrafficCircleBatch() throws Exception {
    	setExpectedResultsNotBatch();
    	correctExpectedResultsBatch();
    	
		long[][] results = otp.calculateTimeMatrixBatch(testPointsAround);
		assertArrayEquals(expUndelayed,results);

		otp.setDelays(intersections);
		results = otp.calculateTimeMatrixBatch(testPointsAround);		
		assertArrayEquals(expDelayed,results);

		// if we reset the delays, we should get back to starting point
		otp.zeroDelays(intersections);
		results = otp.calculateTimeMatrixBatch(testPointsAround);
//		for(long[] a : results) {
//			for(long b : a)
//				System.out.print(b + ",");
//			System.out.println();
//		}		
		assertArrayEquals(expUndelayed,results);

	}    

    @Test
	public void testDelaysOnTrafficCircleWithVertices() throws Exception {
    	setExpectedResultsNotBatch();
    	correctExpectedResultsBatch();
    	
		long[][] results = otp.calculateTimeMatrixBatch(testPointsAround);
		assertArrayEquals(expUndelayed,results);

		otp.setDelays(intersections);
		results = otp.calculateTimeMatrix(testPointsAroundSV);		
		assertArrayEquals(expDelayed,results);

		// if we reset the delays, we should get back to starting point
		otp.zeroDelays(intersections);
		results = otp.calculateTimeMatrix(testPointsAroundSV);		
		assertArrayEquals(expUndelayed,results);

	}
}
