package edu.umich.elasticity.model;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import edu.umich.elasticity.TestCentral;
import junitx.framework.FileAssert;

public class LakeCountyTest {
    static double highImpedance = 0.28/60;
	File lakeCtTestFiles = new File(TestCentral.testResources,"lakeCounty/ct");		
	File lakeBgTestFiles = new File(TestCentral.testResources,"lakeCounty/bg");		

	public static Scenario setLakeCountyScenario(double impedance,boolean isCT) throws Exception {
		Scenario.workDir = new File(TestCentral.testOutput,"work");
		
		int lakeFips = 26085;
		long modCtFips = isCT?26085961100L:260859611003L;
		int popAdded = 75;
		int jobsAdded = 0;
		List<DelayedIntersectionZone> intersections = new ArrayList<DelayedIntersectionZone>();	

		intersections.add(new DelayedIntersectionZone("Michigan and Washington",new Coordinate(43.900889, -85.850890),40));
		intersections.add(new DelayedIntersectionZone("Astor and Washington",new Coordinate(43.900950, -85.861533),6));
		intersections.add(new DelayedIntersectionZone("52 and Astor",new Coordinate(43.893451, -85.861747),27));
		intersections.add(new DelayedIntersectionZone("44 and Michigan",new Coordinate(43.908194, -85.851512),14));

		ArrayList<Integer> counties = new ArrayList<Integer>();
		counties.add(lakeFips);
		
		return new Scenario("g.barreau@gmail.com",counties,modCtFips,jobsAdded,popAdded,impedance,intersections,isCT,false);
	}


	@Test
	public void testLakeCountyBG1() throws Exception {
		Scenario s = setLakeCountyScenario(highImpedance,false);
		assertEquals(43.564,s.boundingBox.latSouth,0.001);
		assertEquals(44.417,s.boundingBox.latNorth,0.001);
		assertEquals(-86.292,s.boundingBox.lonWest,0.001);
		assertEquals(-85.312,s.boundingBox.lonEast,0.001);
		assertEquals(12,s.nbUnits);
		assertEquals(1,s.statesUSPS.size());
		assertEquals("mi",s.statesUSPS.get(0));
		assertEquals(5,s.modUnitIndex);
		// fips "26085961100" pop:"3545" jobs:"456"
		assertEquals(426,s.unitPopulationBefore[5]);
		assertEquals(426+75,s.unitPopulationAfter[5]);
		assertEquals(757,s.unitPopulationBefore[6]);
		assertEquals(757,s.unitPopulationAfter[6]);
		assertEquals(88,s.unitJobsBefore[5]);
		assertEquals(88,s.unitJobsAfter[5]);

		s.run();

		FileAssert.assertEquals(new File(lakeBgTestFiles,Results.fileNames[0]), s.results.outputFiles[0]);
		FileAssert.assertEquals(new File(lakeBgTestFiles,Results.fileNames[1]), s.results.outputFiles[1]);
		FileAssert.assertEquals(new File(lakeBgTestFiles,Results.fileNames[2]), s.results.outputFiles[2]);
		FileAssert.assertEquals(new File(lakeBgTestFiles,Results.fileNames[3]), s.results.outputFiles[3]);
		FileAssert.assertEquals(new File(lakeBgTestFiles,Results.fileNames[4]), s.results.outputFiles[4]);
		
		FileUtils.deleteDirectory(s.directory);
		
	}

	@Test
	public void testLakeCounty1() throws Exception {
		Scenario s = setLakeCountyScenario(highImpedance,true);
		assertEquals(43.564,s.boundingBox.latSouth,0.001);
		assertEquals(44.417,s.boundingBox.latNorth,0.001);
		assertEquals(-86.292,s.boundingBox.lonWest,0.001);
		assertEquals(-85.312,s.boundingBox.lonEast,0.001);
		assertEquals(4,s.nbUnits);
		assertEquals(1,s.statesUSPS.size());
		assertEquals("mi",s.statesUSPS.get(0));
		assertEquals(1,s.modUnitIndex);
		// fips "26085961100" pop:"3545" jobs:"456"
		assertEquals(3545,s.unitPopulationBefore[1]);
		assertEquals(3545+75,s.unitPopulationAfter[1]);
		assertEquals(2882,s.unitPopulationBefore[3]);
		assertEquals(2882,s.unitPopulationAfter[3]);
		assertEquals(456,s.unitJobsBefore[1]);
		assertEquals(456,s.unitJobsAfter[1]);

		s.run();
		
		FileAssert.assertEquals(new File(lakeCtTestFiles,"main.0.28.csv"), s.results.outputFiles[0]);
		FileAssert.assertEquals(new File(lakeCtTestFiles,Results.fileNames[1]), s.results.outputFiles[1]);
		FileAssert.assertEquals(new File(lakeCtTestFiles,Results.fileNames[2]), s.results.outputFiles[2]);
		FileAssert.assertEquals(new File(lakeCtTestFiles,Results.fileNames[3]), s.results.outputFiles[3]);
		FileAssert.assertEquals(new File(lakeCtTestFiles,"details.0.28.csv"), s.results.outputFiles[4]);
		
		//FileUtils.deleteDirectory(s.directory);
	}

	@Test
	public void testLakeCounty2() throws Exception {
		Scenario s = setLakeCountyScenario(Scenario.defaultImpedance,true);
		assertEquals(43.56405,s.boundingBox.latSouth,0.00001);
		assertEquals(44.41709,s.boundingBox.latNorth,0.00001);
		assertEquals(-86.292218,s.boundingBox.lonWest,0.00001);
		assertEquals(-85.312319,s.boundingBox.lonEast,0.00001);
		assertEquals(4,s.nbUnits);
		assertEquals(1,s.statesUSPS.size());
		assertEquals("mi",s.statesUSPS.get(0));
		assertEquals(1,s.modUnitIndex);
		assertEquals(3545,s.unitPopulationBefore[1]);
		assertEquals(3545+75,s.unitPopulationAfter[1]);

		s.run();
		
		FileAssert.assertEquals(new File(lakeCtTestFiles,"main.0.125.csv"), s.results.outputFiles[0]);
		FileAssert.assertEquals(new File(lakeCtTestFiles,Results.fileNames[1]), s.results.outputFiles[1]);
		FileAssert.assertEquals(new File(lakeCtTestFiles,Results.fileNames[2]), s.results.outputFiles[2]);
		FileAssert.assertEquals(new File(lakeCtTestFiles,Results.fileNames[3]), s.results.outputFiles[3]);
		FileAssert.assertEquals(new File(lakeCtTestFiles,"details.0.125.csv"), s.results.outputFiles[4]);

		FileUtils.deleteDirectory(s.directory);
	}

}
