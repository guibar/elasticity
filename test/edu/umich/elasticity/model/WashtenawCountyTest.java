package edu.umich.elasticity.model;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import edu.umich.elasticity.TestCentral;
import junitx.framework.FileAssert;

public class WashtenawCountyTest {
    static double highImpedance = 0.28/60;
	File washtenawTestFiles = new File(TestCentral.testResources,"washtenawCounty");		

	static Scenario setWashtenawCounty() throws Exception {
		Scenario.workDir = new File(TestCentral.testOutput,"work");
		
		int washtenawFips = 26161;
		long modCtFips = 26161403600L;
		int popAdded = 738;
		int jobsAdded = 0;
		List<DelayedIntersectionZone> intersections = new ArrayList<DelayedIntersectionZone>();	

		intersections.add(new DelayedIntersectionZone("Nixon and Dhu Varren/Green",new Coordinate(42.317298,-83.707713),40));
		intersections.add(new DelayedIntersectionZone("Nixon and Meade/Bluett",new Coordinate(42.309948,-83.707495),6));
		intersections.add(new DelayedIntersectionZone("Nixon and Huron",new Coordinate(42.305166,-83.707469),27));
		intersections.add(new DelayedIntersectionZone("Nixon and Plymouth",new Coordinate(42.302515,-83.707250),14));
		intersections.add(new DelayedIntersectionZone("Huron and Plymouth",new Coordinate(42.302609,-83.704390),14));
		intersections.add(new DelayedIntersectionZone("Nixon and Barclays",new Coordinate(42.320014, -83.707736),9));
		
		ArrayList<Integer> counties = new ArrayList<Integer>();
		counties.add(washtenawFips);
		
		return new Scenario("g.barreau@gmail.com",counties,modCtFips,jobsAdded,popAdded,highImpedance,intersections,true,false);
	}


	@Test
	public void testWashtenawCounty1() throws Exception {
		Scenario.workDir = new File(TestCentral.testOutput,"work");

		Scenario s = setWashtenawCounty();
		County cty = County.getCounty(s.countiesFips.get(0));
		assertEquals(351454,cty.population);
		assertEquals(351454+738,cty.getPopulationAfter(s));
		assertEquals(200430,cty.jobs);
		assertEquals(200430,cty.getJobsAfter(s));
		s.run();
		
		File test1Dir = new File(washtenawTestFiles,"test1");
		FileAssert.assertEquals(new File(test1Dir,Results.fileNames[0]), s.results.outputFiles[0]);
		FileAssert.assertEquals(new File(test1Dir,Results.fileNames[1]), s.results.outputFiles[1]);
		FileAssert.assertEquals(new File(test1Dir,Results.fileNames[2]), s.results.outputFiles[2]);
		FileAssert.assertEquals(new File(test1Dir,Results.fileNames[3]), s.results.outputFiles[3]);
		FileAssert.assertEquals(new File(test1Dir,Results.fileNames[4]), s.results.outputFiles[4]);
		
		FileUtils.deleteDirectory(s.directory);

	}

	@Test
	public void testWashtenawCounty2() throws Exception {
		int washtenawFips = 26161;
		long modCtFips = 26161405400L;
		int popAdded = 0;
		int jobsAdded = 178;
		List<DelayedIntersectionZone> intersections = new ArrayList<DelayedIntersectionZone>();	

		intersections.add(new DelayedIntersectionZone("Washtenaw & Manchester",new Coordinate(42.25903,-83.709137),3.0));
		intersections.add(new DelayedIntersectionZone("Washtenaw & Platt",new Coordinate(42.257723,-83.700489),4.0));
		intersections.add(new DelayedIntersectionZone("Washtenaw & Huron",new Coordinate(42.256723,-83.695726),6));
		intersections.add(new DelayedIntersectionZone("Washtenaw & Pittsfield",new Coordinate(42.255837,-83.690522),1));
		intersections.add(new DelayedIntersectionZone("Platt & Huron",new Coordinate(42.25247,-83.700047),0));

		ArrayList<Integer> counties = new ArrayList<Integer>();
		counties.add(washtenawFips);
		
		Scenario s = new Scenario("g.barreau@gmail.com",counties,modCtFips,jobsAdded,popAdded,highImpedance,intersections,true,false);
		s.run();

		File test2Dir = new File(washtenawTestFiles,"test2");
		FileAssert.assertEquals(new File(test2Dir,Results.fileNames[0]), s.results.outputFiles[0]);
		FileAssert.assertEquals(new File(test2Dir,Results.fileNames[1]), s.results.outputFiles[1]);
		FileAssert.assertEquals(new File(test2Dir,Results.fileNames[2]), s.results.outputFiles[2]);
		FileAssert.assertEquals(new File(test2Dir,Results.fileNames[3]), s.results.outputFiles[3]);
		FileAssert.assertEquals(new File(test2Dir,Results.fileNames[4]), s.results.outputFiles[4]);
		
		FileUtils.deleteDirectory(s.directory);

	}
	
	@Test
	public void testBigJob() throws Exception {
		long modCtFips = 26161403600L;
		int popAdded = 738;
		int jobsAdded = 0;
		List<DelayedIntersectionZone> intersections = new ArrayList<DelayedIntersectionZone>();	

		intersections.add(new DelayedIntersectionZone("Nixon and Barclays",new Coordinate(42.320005,-83.707747),1.4));
		intersections.add(new DelayedIntersectionZone("Nixon and Green",new Coordinate(42.317281,-83.707667),12.1));
		intersections.add(new DelayedIntersectionZone("Nixon and Bluett",new Coordinate(42.309856,-83.707506),2.2));
		intersections.add(new DelayedIntersectionZone("Nixon and Huron 4",new Coordinate(42.305048,-83.707384),5.0));
		intersections.add(new DelayedIntersectionZone("Nixon and Plymouth",new Coordinate(42.302528,-83.707238),0.6));
		intersections.add(new DelayedIntersectionZone("Plymouth and Huron",new Coordinate(42.302598,-83.704382),3.3));

		ArrayList<Integer> counties = new ArrayList<Integer>();
		counties.add(26161);
		counties.add(26163);
		
		Scenario s = new Scenario("g.barreau@gmail.com",counties,modCtFips,jobsAdded,popAdded,highImpedance,intersections,true,false);
		s.run();
	}

	static Scenario setWashtenawCounty2() throws Exception {
		Scenario.workDir = new File(TestCentral.testOutput,"work");
		
		int washtenawFips = 26161;
		long modCtFips = 26161403600L;
		int popAdded = 738;
		int jobsAdded = 0;
		List<DelayedIntersectionZone> intersections = new ArrayList<DelayedIntersectionZone>();	

		intersections.add(new DelayedIntersectionZone("Nixon and Barclays",new Coordinate(42.320005,-83.707747),1.4));
		intersections.add(new DelayedIntersectionZone("Nixon and Green",new Coordinate(42.317281,-83.707667),12.1));
		intersections.add(new DelayedIntersectionZone("Nixon and Bluett",new Coordinate(42.309856,-83.707506),2.2));
		intersections.add(new DelayedIntersectionZone("Nixon and Huron 1",new Coordinate(42.305286,-83.707561),5));
		intersections.add(new DelayedIntersectionZone("Nixon and Huron 2",new Coordinate(42.305148,-83.707667),5));
		intersections.add(new DelayedIntersectionZone("Nixon and Huron 3",new Coordinate(42.305174,-83.707286),5));
		intersections.add(new DelayedIntersectionZone("Nixon and Huron 4",new Coordinate(42.305048,-83.707384),5));
		intersections.add(new DelayedIntersectionZone("Nixon and Plymouth",new Coordinate(42.302528,-83.707238),0.6));
		intersections.add(new DelayedIntersectionZone("Plymouth and Huron",new Coordinate(42.302598,-83.704382),3.3));
		
		ArrayList<Integer> counties = new ArrayList<Integer>();
		counties.add(washtenawFips);
		
		return new Scenario("g.barreau@gmail.com",counties,modCtFips,jobsAdded,popAdded,highImpedance,intersections,true,false);
	}

}
