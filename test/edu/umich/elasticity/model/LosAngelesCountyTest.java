package edu.umich.elasticity.model;


import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import edu.umich.elasticity.TestCentral;

public class LosAngelesCountyTest {
	File laCtTestFiles = new File(TestCentral.testResources,"laCounty/ct");		
	File laBgTestFiles = new File(TestCentral.testResources,"laCounty/bg");		
    static double highImpedance = 0.28/60;

	public static Scenario setLosAngelesCountyScenario(boolean fastAnalysis) throws Exception {
		Scenario.workDir = new File(TestCentral.testOutput,"work");
		
		int laCountyFips = 6037;
		// "6037102105","34.209876","-118.3492653","1880","195"

		long modCtFips = 6037102105L;
		int popAdded = 333;
		int jobsAdded = 0;
		List<DelayedIntersectionZone> intersections = new ArrayList<DelayedIntersectionZone>();	

		intersections.add(new DelayedIntersectionZone("Glenoakes and Shadyspring Dr",new Coordinate(34.212994, -118.348217),40));
		intersections.add(new DelayedIntersectionZone("Glenoakes and Shadyspring Pl",new Coordinate(34.211681, -118.346769),6));
		intersections.add(new DelayedIntersectionZone("North Hollywood way",new Coordinate(34.211610, -118.348486),27));
		intersections.add(new DelayedIntersectionZone("Gleanoakes and Lanark",new Coordinate(34.218362, -118.354473),14));

		ArrayList<Integer> counties = new ArrayList<Integer>();
		counties.add(laCountyFips);
		
		return new Scenario("g.barreau@gmail.com",counties,modCtFips,jobsAdded,popAdded,highImpedance,intersections,true,fastAnalysis);
	}

	
	//@Test
	public void testLosAngelesCounty1() throws Exception {
		Scenario s = setLosAngelesCountyScenario(true);
//		assertEquals(43.564,s.boundingBox.latSouth,0.001);
//		assertEquals(44.417,s.boundingBox.latNorth,0.001);
//		assertEquals(-86.292,s.boundingBox.lonWest,0.001);
//		assertEquals(-85.312,s.boundingBox.lonEast,0.001);
//		assertEquals(4,s.nbUnits);
//		assertEquals(1,s.statesUSPS.size());
//		assertEquals("mi",s.statesUSPS.get(0));
//		assertEquals(1,s.modUnitIndex);
//		// fips "26085961100" pop:"3545" jobs:"456"
//		assertEquals(3545,s.unitPopulationBefore[1]);
//		assertEquals(3545+75,s.unitPopulationAfter[1]);
//		assertEquals(2882,s.unitPopulationBefore[3]);
//		assertEquals(2882,s.unitPopulationAfter[3]);
//		assertEquals(456,s.unitJobsBefore[1]);
//		assertEquals(456,s.unitJobsAfter[1]);

		s.run();
		
//		FileAssert.assertEquals(new File(laCtTestFiles,"main.0.28.csv"), s.results.outputFiles[0]);
//		FileAssert.assertEquals(new File(laCtTestFiles,Results.fileNames[1]), s.results.outputFiles[1]);
//		FileAssert.assertEquals(new File(laCtTestFiles,Results.fileNames[2]), s.results.outputFiles[2]);
//		FileAssert.assertEquals(new File(laCtTestFiles,Results.fileNames[3]), s.results.outputFiles[3]);
//		FileAssert.assertEquals(new File(laCtTestFiles,"details.0.28.csv"), s.results.outputFiles[4]);
//		
//		FileUtils.deleteDirectory(s.directory);
	}

	@Test
	public void testLosAngelesCounty2() throws Exception {
		Scenario s = setLosAngelesCountyScenario(false);
		s.run();
	}
}
