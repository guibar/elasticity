package edu.umich.elasticity;

import java.io.File;
import java.util.ArrayList;


import edu.umich.elasticity.model.AnalysisUnit;
import edu.umich.elasticity.model.AnalysisUnitDefinitionException;
import edu.umich.elasticity.model.County;
import edu.umich.elasticity.model.Scenario;

public class TestCentral {
	public static File testResources = new File(Scenario.root,"test/resources");
	public static File testOutput = new File(Scenario.root,"test/output");

    
	public static void main(String[] args) throws Exception {
		Scenario.workDir = new File(TestCentral.testOutput,"work");
		while(true) {
			AnalysisUnit unit = AnalysisUnit.getRandomUnit(true);
			ArrayList<Integer> list = new ArrayList<Integer>();
			list.add(unit.getCountyFIPS());
			if (County.getNbCTs(list)>200)
				continue;
			try {
				Scenario s = new Scenario(unit.fips);
				s.run();
			} catch(AnalysisUnitDefinitionException e) {
				continue;
			}
		}
	}

}