package edu.umich.elasticity.nbmodel;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.umich.elasticity.TestCentral;

public class TabularDataTest {
	File timesBuildFile = new File(TestCentral.testResources,"buildNoBuild/skims_build.csv");		
	File timesNoBuildFile = new File(TestCentral.testResources,"buildNoBuild/skims_no_build.csv");		
	File unitsNoBuildFile = new File(TestCentral.testResources,"buildNoBuild/Zones 2020 Build.csv");		
	File unitsBuildFile = new File(TestCentral.testResources,"buildNoBuild/Zones 2020 No Build.csv");		

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testUnitsData() throws Exception {
		TabularData td = new TabularData(new BufferedReader(new FileReader(unitsNoBuildFile)));
		assertEquals(1, td.data[0][0]);
		assertEquals(45, td.data[2][294]);
	}

	
	// Testing Memory Usage
	@Test
	public void testBuildTimes() throws Exception {		
	    TabularData t1 = new TabularData(new BufferedReader(new BufferedReader(new FileReader(unitsNoBuildFile))));
	    TabularData t2 = new TabularData(new BufferedReader(new BufferedReader(new FileReader(unitsBuildFile))));

		TabularData td1 = new TabularData(new BufferedReader(new FileReader(timesBuildFile)));
		int[] points = new int[436];
		for(int i=0;i<436;i++)
			points[i]=i+1;
		float[][] matrix1 = td1.getMatrix(points, 0, 1, 2);
		// point 5,41
		assertEquals(34.57666667, matrix1[4][40],0.0001);
		
		TabularData td2 = new TabularData(new BufferedReader(new FileReader(timesNoBuildFile)));
		float[][] matrix2 = td2.getMatrix(points, 0, 1, 2);
		// point 5,41
		assertEquals(35.05, matrix2[4][40],0.0001);
	}
}
