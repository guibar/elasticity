package edu.umich.elasticity.nbmodel;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.umich.elasticity.TestCentral;
import edu.umich.elasticity.model.Scenario;
import junitx.framework.FileAssert;

public class NBScenarioTest {

	@Before
	public void setUp() throws Exception {
		NBScenario.workDir = new File(TestCentral.testOutput,"work");
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void test3UnitsScenario() throws Exception {
		File[] timesFiles = new File[] {
				new File(TestCentral.testResources,"buildNoBuild/3units-times-nobuild.txt"),		
				new File(TestCentral.testResources,"buildNoBuild/3units-times-build.txt")};
		
		File[] unitsFiles = 
			new File[] {new File(TestCentral.testResources,"buildNoBuild/3units.txt"),
						new File(TestCentral.testResources,"buildNoBuild/3units.txt")};						
				
		NBScenario scenario = new NBScenario("g.barreau@gmail.com",unitsFiles,timesFiles,Scenario.defaultImpedance);
		FileAssert.assertEquals(new File(TestCentral.testResources,"buildNoBuild/3unitsMainSame.csv"), scenario.results.outputFiles[0]);
	}

	@Test
	public void test3UnitsScenarioDiff() throws Exception {
		File[] timesFiles = new File[] {
				new File(TestCentral.testResources,"buildNoBuild/3units-times-nobuild.txt"),		
				new File(TestCentral.testResources,"buildNoBuild/3units-times-build.txt")};
		
		File[] unitsFiles = 
			new File[] {new File(TestCentral.testResources,"buildNoBuild/3units.txt"),
						new File(TestCentral.testResources,"buildNoBuild/3unitsAfter.txt")};						
				
		NBScenario scenario = new NBScenario("g.barreau@gmail.com",unitsFiles,timesFiles,Scenario.defaultImpedance);
		FileAssert.assertEquals(new File(TestCentral.testResources,"buildNoBuild/3unitsMainDiff.csv"), scenario.results.outputFiles[0]);
	}

	@Test
	public void testManyUnitsScenario() throws Exception {
		File[] unitsFiles = new File[] {
				new File(TestCentral.testResources,"buildNoBuild/Zones 2020 No Build.csv"),		
				new File(TestCentral.testResources,"buildNoBuild/Zones 2020 Build.csv")};
			
			File[] timesFiles = 
				new File[] {new File(TestCentral.testResources,"buildNoBuild/skims_no_build.csv"),
							new File(TestCentral.testResources,"buildNoBuild/skims_build.csv")};											
			NBScenario scenario = new NBScenario("g.barreau@gmail.com",unitsFiles,timesFiles,Scenario.defaultImpedance);

		FileAssert.assertEquals(new File(TestCentral.testResources,"buildNoBuild/436unitsMain.csv"), scenario.results.outputFiles[0]);

	}
}
