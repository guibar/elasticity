package edu.umich.elasticity;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import org.junit.Test;

import edu.umich.elasticity.OSMService;
import edu.umich.elasticity.model.BoundingBox;

public class OSMServiceTest {
	
	@Test
	public void testExtraction() throws IOException, ClassNotFoundException, SQLException {
		File resultFile = new File(TestCentral.testOutput,"osm/mi-extract.pbf");
		BoundingBox bb = new BoundingBox(42.07,-84.70,42.42,-85.30);
		OSMService.extractAndFilterWithOsmosis(new File(OSMService.osmDataDir,"mi.osm.pbf"),resultFile,bb,true,System.out);
		assertEquals(resultFile.length(),700001);
		fail("I need to automate the testing of this pbf file. Checking in QGIS at the moment");
	}

	@Test
	public void testFilteredExtraction() throws IOException, ClassNotFoundException, SQLException {
		File extractFile = new File(TestCentral.testOutput,"osm/mi-extract.osm.xml");
		File filteredFile = new File(TestCentral.testOutput,"osm/mi-filtered.osm.xml");
		BoundingBox bb = new BoundingBox(42.07,-84.70,42.42,-85.30);
		OSMService.extractAndOsmFilter(new File(OSMService.osmDataDir,"mi.osm.pbf"),extractFile,filteredFile,bb,System.out);
	}
}
