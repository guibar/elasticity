package edu.umich.elasticity;


import java.sql.SQLException;
import org.junit.Test;

public class MailTest {

	@Test
	public void testMailService() throws ClassNotFoundException, SQLException {		
		MailService.send(new String[]{"g.barreau@gmail.com"}, "Junit test 1", "This is a test", null);
	}

	@Test
	public void testMailService2() throws ClassNotFoundException, SQLException {		
		MailService.send(new String[]{"guibargui@gmail.com","g.barreau@gmail.com"}, "Junit test 2", 
				"Should receive this mail both at g.barreau and guibargui", null);
	}
}
