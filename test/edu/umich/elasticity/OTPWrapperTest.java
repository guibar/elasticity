package edu.umich.elasticity;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.opentripplanner.routing.graph.Vertex;
import org.opentripplanner.routing.vertextype.StreetVertex;
import org.opentripplanner.standalone.OTPMain;

import edu.umich.elasticity.model.BoundingBox;
import edu.umich.elasticity.model.Coordinate;
import edu.umich.elasticity.model.Results;

public class OTPWrapperTest {
	static File hillsdaleDir = new File(TestCentral.testResources,"hillsdale");
	static File pbfExtractFile = new File(hillsdaleDir,"hillsdale.osm.pbf");

	static Coordinate testPoint1 = new Coordinate(41.9199839651493,-84.635290700569);  //1, n184449765
	static Coordinate testPoint2 = new Coordinate(41.9199897901849,-84.6336596906116); //2, n184445712 
	static Coordinate testPoint3 = new Coordinate(41.9199956152204,-84.632046155761);  //3, n184449762
	static Coordinate testPoint4 = new Coordinate(41.9188946834992,-84.632046155761);  //4, n184480919
	static Coordinate testPoint5 = new Coordinate(41.9176073506401,-84.6320170305832); //5, n184453418
	static Coordinate testPoint6 = new Coordinate(41.9176073506401,-84.6336363904694); //6, n184453416
	static Coordinate testPoint7 = new Coordinate(41.9176190007112,-84.6352790504978); //7  n184453414
	static Coordinate testPoint8 = new Coordinate(41.9188888584637,-84.6352732254623); //8  n184470229
	static Coordinate testPoint9 = new Coordinate(41.9188946834992,-84.6336480405405); //9  n184462219
	static Coordinate testPoint10 = new Coordinate(41.9212305227596,-84.6352965256045);//10, n184467310
	static Coordinate testPoint11 = new Coordinate(41.9212363477951,-84.6336480405405);//11, n184445715
	static Coordinate testPoint12 = new Coordinate(41.9212421728307,-84.6320636308677);//12, n184467308
	static Coordinate[] testPoints = new Coordinate[]{testPoint1,testPoint2,testPoint3,testPoint4,testPoint5,
			testPoint6,testPoint7,testPoint8,testPoint9,testPoint10,testPoint11,testPoint12};
	
	@Test
	public void testUpdateDiagonal() throws IOException {
		long[][] travelTimes = new long[][]{{400L,300L,200L,100L},{400L,300L,200L,100L},{400L,300L,200L,100L},{400L,300L,200L,100L}};
		Results.updateDiagonal(travelTimes);
		assertEquals(100, travelTimes[0][0]);
		assertEquals(700/6, travelTimes[1][1]);
		assertEquals(800/6, travelTimes[2][2]);
		assertEquals(900/6, travelTimes[3][3]);
		assertEquals(300, travelTimes[0][1]);
		assertEquals(200, travelTimes[0][2]);
		assertEquals(100, travelTimes[0][3]);
	}

		
	// Need to fix this test: @Test
	public void testDetailsHillsdale() throws Exception {
		//		10 ===  11 ===  12
		//		|		|		||
		//		^       v       ||
		//		|		|		||
		//		1  ===  2  ===  3
		//		|		|		||
		//		^       v	    ||
		//		|		|		||
		//		8  ===  9  ===  4
		
		BoundingBox bb = new BoundingBox(41.9076,-84.6206,41.9315,-84.6457);
		if (!pbfExtractFile.exists())
			OSMService.extractAndFilterWithOsmosis(OSMService.getStateFile("mi",false),pbfExtractFile,bb,true,System.out);	
		// For some reason, if the bbox is too close around the points of interest, the graph doesn't get built properly
		// OSMScenario.extractWithOsmosis(OSMScenario.getOSMStateFile("mi"),pbfExtractFile,-84.6357,41.9176,-84.6306,41.9215);

		OTPWrapper routerWrapper = new OTPWrapper(hillsdaleDir,"hillsdale",System.out);

		String id = "osm:node:184478934";
		Vertex v = routerWrapper.otpRouter.graph.getVertex(id);
		assertEquals(id,v.getLabel());

		assertTrue(routerWrapper.calculateTrip(testPoint2, testPoint12)!=0);

		
		Coordinate[] points = new Coordinate[2];
		points[0] = testPoints[8];
		points[1] = testPoints[10];
				
		// without any delays 11 -> 9 is faster than 9 -> 11
		long[][] res = routerWrapper.calculateTimeMatrix(points);
		assertEquals(69,res[0][1]);
		assertEquals(30,res[1][0]);
		System.out.println(Results.formatMatrix(res));

		// with delay at point 2, both trip go via the right side and have equal times
//		routerWrapper.setDelay("osm:node:184445712", 100);
		res = routerWrapper.calculateTimeMatrix(points);
		assertEquals(69,res[0][1]);
		assertEquals(69,res[1][0]);
		System.out.println(Results.formatMatrix(res));

		// adding delay at point 1 doesn't change anything
//		routerWrapper.setDelay("osm:node:184449765", 100);
		res = routerWrapper.calculateTimeMatrix(points);
		assertEquals(69,res[0][1]);
		assertEquals(69,res[1][0]);
		
		// adding delay at point 3 (remove delay at point 1) makes 9->11 go via left
		// and remain nearly the same but 11->9 has to go via some way outside this points (further to the left)
		// and becomes significantly longer
//		routerWrapper.removeDelay("osm:node:184449765");
//		routerWrapper.setDelay("osm:node:184449762", 100);
		res = routerWrapper.calculateTimeMatrix(points);
		assertEquals(70,res[0][1]);
		assertEquals(99,res[1][0]);
		System.out.println(Results.formatMatrix(res));

		// now both 1 2 and 3 are delayed
		// no change for 11->9 which was not going via 1 but extra delay
		// for 9->11 which cannot go via left anymore
//		routerWrapper.setDelay("osm:node:184449765", 200);
		res = routerWrapper.calculateTimeMatrix(points);
		assertEquals(99,res[0][1]);
		assertEquals(99,res[1][0]);
		System.out.println(Results.formatMatrix(res));
	}

	@Test
	public void testWeight() throws Exception {
		File dir = new File(TestCentral.testResources,"otp");
		OTPWrapper routerWrapper = new OTPWrapper(dir,"test",System.out);
		assertEquals(routerWrapper.otpRouter,OTPMain.graphService.getRouter("test"));

// <osm:node:62613091 lat,lng=42.238852900000005,-83.67866620000001> in router test
// <osm:node:305612114 lat,lng=42.273215300000004,-83.69364870000001> in router test
		
		StreetVertex[] points = new StreetVertex[]{
				routerWrapper.getNearbyStreetVertex(new Coordinate(42.23885,-83.67866)),
				routerWrapper.getNearbyStreetVertex(new Coordinate(42.27321,-83.69364))};
		routerWrapper.calculateTimeMatrix(points);

	}

	
	@Test
	public void testRandomPointsRouterIdAndTreeSet() throws Exception {
		File dir = new File(TestCentral.testResources,"otp");
	    File osmFile = new File(dir,"bbox.osm.pbf");
		if (!osmFile.exists()) {
			BoundingBox bb = new BoundingBox(42.22,-83.65,42.28,-83.71);
			OSMService.extractAndFilterWithOsmosis(new File(OSMService.osmDataDir,"mi.osm.pbf"),osmFile,bb,true,System.out);
		}
		OTPWrapper routerWrapper = new OTPWrapper(dir,"test",System.out);
		assertEquals(routerWrapper.otpRouter,OTPMain.graphService.getRouter("test"));

		assertEquals(18,routerWrapper.getRandomIntersections(18).size());

		// there are 2119 streetvertices in this graph, because of the overlap, it should return less
		assertTrue(routerWrapper.getRandomIntersections(2119).size()<2119);

		List<StreetVertex> ts = routerWrapper.getStreetVerticesInBuffer(new Coordinate(42.25948,-83.69044), 20.0);
		assertEquals(2, ts.size());
		assertTrue(ts.get(0).getLabel().equals("osm:node:1121114076"));
		assertTrue(ts.get(1).getLabel().equals("osm:node:62499725"));
		
		ts = routerWrapper.getStreetVerticesInBuffer(new Coordinate(42.25948,-83.69044), 45.0);
		assertEquals(3, ts.size());
		assertTrue(ts.get(0).getLabel().equals("osm:node:1121114076"));
		assertTrue(ts.get(1).getLabel().equals("osm:node:62499725"));
		assertTrue(ts.get(2).getLabel().equals("osm:node:1121114070"));

	}
	

	@Test
	public void testMultipleRouters() throws NoPathFoundException, IOException {
		Coordinate[] points = new Coordinate[2];
		points[0]= testPoint2;
		points[1] = testPoint12;
		
		File dir = new File(TestCentral.testResources,"hillsdale");
		OTPWrapper routerWrapper1 = new OTPWrapper(dir,"test1",System.out);
		OTPWrapper routerWrapper2 = new OTPWrapper(dir,"test2",System.out);
		
		assertEquals(routerWrapper1.otpRouter,OTPMain.graphService.getRouter("test1"));
		assertEquals(routerWrapper2.otpRouter,OTPMain.graphService.getRouter("test2"));
		
		long[][] a1 = routerWrapper1.calculateTimeMatrix(points);
		long[][] a2 = routerWrapper2.calculateTimeMatrix(points);
		assertArrayEquals(a1,a2);
		
	}
}
