package edu.umich.elasticity.setup;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

import edu.umich.elasticity.TestCentral;
import edu.umich.elasticity.model.AnalysisUnit;
import edu.umich.elasticity.setup.LodesFile;
import edu.umich.elasticity.setup.LodesFileYearComparator;
import edu.umich.elasticity.setup.NoLodesDataException;

public class LodesFileTest {

	@BeforeClass
	public static void useTestDirectory()  {
		LodesFile.lodesDirectory = new File(TestCentral.testResources,"lodes");		
	}

	@Test
    public void testDirectory() throws Exception {
    	assertTrue(LodesFile.lodesDirectory.exists());    	
    }

	
	@Test
    public void testComparator() throws Exception {
		ArrayList<LodesFile> list = new ArrayList<LodesFile>();
		list.add(new LodesFile(2013, "ar"));
		list.add(new LodesFile(2014, "id"));
		
		// without comparator
    	assertEquals(2013,list.get(0).year);
    	// after sorting with comparator, most recent is first
    	Collections.sort(list,new LodesFileYearComparator());
    	assertEquals(2014,list.get(0).year);    	
    }

	@Test
    public void testGetArizonaLodes() throws Exception {
		LodesFile arFile = LodesFile.getLodesFile("ar");
    	assertNotNull(arFile);
		assertEquals(2014,arFile.year);
		assertEquals(arFile.getName(),"ar_wac_S000_JT00_2014.csv.gz");
		
		arFile = LodesFile.getLatestLocalLodesFile("ar");
    	assertNotNull(arFile);
		assertEquals(2014,arFile.year);
		assertEquals(arFile.getName(),"ar_wac_S000_JT00_2014.csv.gz");
		
		arFile = new LodesFile(2014, "ar");
    	assertNotNull(arFile);
		assertEquals("ar_wac_S000_JT00_2014.csv.gz",arFile.getName());
    }

	@Test
    public void testYearOfFile() throws Exception {
		LodesFile idFile = LodesFile.getLodesFile("id");
    	assertNotNull(idFile);
    	// 2014 is the most recent year
    	assertEquals(2014,idFile.year);
    	idFile.delete();
    }

	/**
	 * Should find the 2013 locally and only look for 2015 and 2014 (check LOG.info)
	 * @throws Exception
	 */
	@Test
    public void testYearOfFile2() throws Exception {
		// check in dir
		LodesFile idFile = LodesFile.getLatestLocalLodesFile("ks");
    	assertEquals((new File(LodesFile.lodesDirectory,"ks_wac_S000_JT00_2014.csv.gz")),idFile);
    	assertEquals(2014,idFile.year);

    	// check online
    	idFile = LodesFile.getLodesFile("ks");
    	assertNotNull(idFile);
    	assertEquals(2014,idFile.year);
    }

	/* Needs to be fixed to use a toy db, not the main one */
	public void testReadArkansasData() throws IOException, NoLodesDataException, SQLException, ClassNotFoundException {
		LodesFile arFile = LodesFile.getLodesFile("ar");
		HashMap<Long,Integer> result1 = new HashMap<Long,Integer>();
		HashMap<Long,Integer> result = new HashMap<Long,Integer>();
		arFile.getEmploymentData(result1,result);
		// first 2
		assertEquals(result.get(5001480100L).intValue(),11+1+3+1+1+6+1+2+1+6+8+6+1+4+1);
		assertEquals(result.get(5001480200L).intValue(),21+23+1+12+50+10+4+13+2+3+3+54+20+2+4+2+20+2+5+3+3+2+2+13+7+10+7+2+1+6);
		// last
		assertEquals(result.get(5149952600L).intValue(),9+5+11+1+70+1+11+11+1+12+14+21+11+11+1+1+5);
		
		// put the results in the database, and collect missing points
		Set<Long> missing = new HashSet<Long>();
		DatabaseBuilder.loadColumnDataInDb("census_tract","jobs", result);
		
		// look up the same 3 as above but from the db this time
		AnalysisUnit ct = AnalysisUnit.getUnit(5001480100L,true);		
		assertEquals(ct.jobs,11+1+3+1+1+6+1+2+1+6+8+6+1+4+1);
		ct = AnalysisUnit.getUnit(5001480200L,true);
		assertEquals(ct.jobs,21+23+1+12+50+10+4+13+2+3+3+54+20+2+4+2+20+2+5+3+3+2+2+13+7+10+7+2+1+6);
		ct = AnalysisUnit.getUnit(5149952600L,true);
		assertEquals(ct.jobs,9+5+11+1+70+1+11+11+1+12+14+21+11+11+1+1+5);
		
		// CT 05069000102 is missing from this dataset
		assertEquals(missing.size(),1);
		assertTrue(missing.contains(5069000102L));
	}
}
