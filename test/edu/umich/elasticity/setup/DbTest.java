package edu.umich.elasticity.setup;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;
import com.vividsolutions.jts.util.Assert;

import edu.umich.elasticity.TestCentral;
import edu.umich.elasticity.model.Scenario;


public class DbTest {

	@Test
	public void testSQLiteDriver() throws ClassNotFoundException, SQLException {
		File sqlFile = new File(TestCentral.testOutput,"sample.sqlite");
		sqlFile.delete();
		
		// load the sqlite-JDBC driver using the current class loader
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		// create a database connection
		// could also be DriverManager.getConnection("jdbc:sqlite:C:/work/mydatabase.db");
		connection = DriverManager.getConnection("jdbc:sqlite:test/output/sample.sqlite");
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30); // set timeout to 30 sec.
	
		statement.executeUpdate("drop table if exists person");
		statement.executeUpdate("CREATE TABLE person (id integer, name string)");
		statement.executeUpdate("insert into person values(1, 'leo')");
		statement.executeUpdate("insert into person values(2, 'yui')");
		ResultSet rs = statement.executeQuery("select * from person");
		while (rs.next()) {
			// read the result set
			if (rs.getInt("id")==1)
				assertEquals("leo",rs.getString("name"));
			if (rs.getInt("id")==2)
				assertEquals("yui",rs.getString("name"));
		}
		if (connection != null)
			connection.close();
		assertTrue(sqlFile.exists());
	}

	@Test
	public void testGeoDb() throws ClassNotFoundException, SQLException {
		// load the sqlite-JDBC driver using the current class loader
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		connection = DriverManager.getConnection(Scenario.dbUrl);
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30); // set timeout to 30 sec.
	
		ResultSet rs = statement.executeQuery("select count(*) from state");
		rs.next();
		Assert.equals(51,rs.getInt(1));
		
		rs = statement.executeQuery("select count(*) from county");
		rs.next();
		Assert.equals(3142,rs.getInt(1));
				
		rs = statement.executeQuery("SELECT count(*) FROM census_tract,county,state "
				+ "where census_tract.county_fips = county.fips and county.state_fips = state.fips");
		rs.next();
		Assert.equals(73056,rs.getInt(1));

		rs = statement.executeQuery("SELECT count(*) FROM block_group,census_tract,county,state "
				+ "where block_group.ct_fips = census_tract.fips and census_tract.county_fips = county.fips and county.state_fips = state.fips");
		rs.next();
		Assert.equals(217739,rs.getInt(1));

		if (connection != null)
			connection.close();
	}
}
