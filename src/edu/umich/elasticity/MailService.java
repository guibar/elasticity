package edu.umich.elasticity;

import java.io.File;
import java.util.Arrays;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MailService {
	static final Logger LOG = LoggerFactory.getLogger(MailService.class);
	// these are initialiszed by the ServletContextListener
	static String fromEmail,username,password,host,port;
	public static String adminEmail;

	public static void send(String[] emails,String subject,String body, File attachment) {
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		try {
			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromEmail));
			for(String email: emails)
				message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			message.setSubject(subject);

			if (attachment==null || !attachment.exists())
				message.setText(body);
			else {
				// Create the message part
				BodyPart messageBodyPart = new MimeBodyPart();
	
				// Now set the actual message
				messageBodyPart.setText(body);
	
				// Create a multipart message
				Multipart multipart = new MimeMultipart();
				// Set text message part
				multipart.addBodyPart(messageBodyPart);
	
				// Part two is attachment
				messageBodyPart = new MimeBodyPart();
				DataSource source = new FileDataSource(attachment);
				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName(attachment.getName());
				multipart.addBodyPart(messageBodyPart);
	
				// Send the complete message parts
				message.setContent(multipart);
			}
			
			// Send message
			Transport.send(message);

			LOG.info("Message to " + Arrays.toString(emails) + " was sent successfully.");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}