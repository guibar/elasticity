package edu.umich.elasticity;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.opentripplanner.common.geometry.SphericalDistanceLibrary;
import org.opentripplanner.common.model.GenericLocation;
import org.opentripplanner.graph_builder.GraphBuilder;
import org.opentripplanner.routing.algorithm.AStar;
import org.opentripplanner.routing.core.RoutingRequest;
import org.opentripplanner.routing.core.TraverseMode;
import org.opentripplanner.routing.edgetype.StreetEdge;
import org.opentripplanner.routing.graph.Edge;
import org.opentripplanner.routing.graph.Graph;
import org.opentripplanner.routing.graph.Vertex;
import org.opentripplanner.routing.impl.DefaultStreetVertexIndexFactory;
import org.opentripplanner.routing.impl.MemoryGraphSource;
import org.opentripplanner.routing.services.GraphService;
import org.opentripplanner.routing.spt.GraphPath;
import org.opentripplanner.routing.spt.ShortestPathTree;
import org.opentripplanner.routing.vertextype.StreetVertex;
import org.opentripplanner.standalone.CommandLineParameters;
import org.opentripplanner.standalone.OTPMain;
import org.opentripplanner.standalone.Router;
import org.opentripplanner.routing.core.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vividsolutions.jts.geom.Envelope;

import edu.umich.elasticity.model.Coordinate;
import edu.umich.elasticity.model.DelayedIntersectionZone;
import edu.umich.elasticity.model.UnknownIntersectionException;

public class OTPWrapper {	

	private static final Logger LOG = LoggerFactory.getLogger(OTPWrapper.class);

	private static final int NB_TEST_POINTS = 6;
	static final int MAX_POSSIBLE_DURATION = 20000;
	static final double SEARCH_RADIUS_IN_METERS = 20.0;
	static final int MAX_SEARCH_RADIUS_IN_METERS = 10000;
    
	// these are points which are known to be connected to the bulk of the network.
	StreetVertex[] testVertices;

	public Router otpRouter;
	PrintStream ps;
	
	/**
	 * This constructor creates a graph, registers it under its name and 
	 * finds some random points on the main graph known as testVertices.
	 * 
	 * @param directory the directory where the graph will be built
	 * @param name the name of the Graphh
	 * @param ps a stream to which messages can be written
	 */
	public OTPWrapper(File directory,String name,PrintStream ps) {
		this.ps = ps;
		// instantiate the static graphService
		if (OTPMain.graphService == null) {
			LOG.info("Instantiating  GraphService");
			OTPMain.graphService = new GraphService(false);
		}
		CommandLineParameters clp = new CommandLineParameters();
		clp.inMemory = true;
		
        GraphBuilder graphBuilder = GraphBuilder.forDirectory(clp,directory);
        graphBuilder.run();
        Graph graph = graphBuilder.getGraph();
        graph.index(new DefaultStreetVertexIndexFactory());
        OTPMain.graphService.registerGraph(name, new MemoryGraphSource(name, graph));
		otpRouter = OTPMain.graphService.getRouter(name);
		
		// these points are used to assess if CT points are reachable
		setTestPoints();		
	}

	/**
	 *  Dispose of the graph.
	 */
	public void close() {
		// unregister this router
		if (OTPMain.graphService.evictRouter(otpRouter.id))
			ps.println("Router was evicted successfully");
		else
			ps.println("Router eviction returned false");
		otpRouter = null;
		ps.close();
	}

	RoutingRequest getRoutingRequest(Coordinate from,Coordinate to) {
        RoutingRequest req = new RoutingRequest(TraverseMode.CAR);
        req.batch = false;
        req.routerId = otpRouter.id;
        req.walkReluctance = 1.0;
        req.turnReluctance = 1.0;
        req.from = new GenericLocation(from.latitude,from.longitude);
        if (to!=null)
            req.to = new GenericLocation(to.latitude,to.longitude);
        else
        	req.batch = true;
        req.setRoutingContext(otpRouter.graph);
		return req;
	}

	RoutingRequest getRoutingRequest(StreetVertex from,StreetVertex to) {
        RoutingRequest req = new RoutingRequest(TraverseMode.CAR);
        req.batch = false;
        req.routerId = otpRouter.id;
        req.walkReluctance = 1.0;
        req.turnReluctance = 1.0;
        req.from = new GenericLocation(from.getCoordinate().y,from.getCoordinate().x);
        if (to!=null)
            req.to = new GenericLocation(to.getCoordinate().y,to.getCoordinate().x);
        else
        	req.batch = true;
        req.setRoutingContext(otpRouter.graph);
		return req;
	}
	
	public long[][] calculateTimeMatrix(Coordinate[] points) throws NoPathFoundException {
		long[][] results = new long[points.length][points.length];
		AStar as = new AStar();
		
	    for(int i=0;i<points.length;i++) {
	    	LOG.info("Working on trips from " + points[i] + " in router " + otpRouter.id);
	        for(int j=0;j<points.length;j++) {
	        	RoutingRequest req = getRoutingRequest(points[i],points[j]);
		        	        
				ShortestPathTree spta = as.getShortestPathTree(req);
				
				if (spta.getPaths().isEmpty()) {
					ps.println(String.format("No path from %s to %s",points[i],points[j]));
					throw new NoPathFoundException(points[i],points[j],i,j);
				} else {
					results[i][j] = (long)spta.getPaths().get(0).getWeight();
		    		//spta.getPaths().get(0).dumpPathParser();
				}
				req.cleanup();
			}
	    }
		return results;
	}

	/** This is the method used by Scenario to calculate the time matrices.
	 * 
	 * @param vertices an array of vertices from the graph
	 * @return the matrix with the time travels between any 2 vertices
	 * @throws NoPathFoundException if no path can be found between the 2 points.
	 */
	public long[][] calculateTimeMatrix(StreetVertex[] vertices) throws NoPathFoundException {
		long[][] results = new long[vertices.length][vertices.length];		
		
	    for(int i=0;i<vertices.length;i++) {
	    	LOG.info("Working on trips from " + vertices[i] + " in router " + otpRouter.id);
	        RoutingRequest req = getRoutingRequest(vertices[i],null);	        
	    	ShortestPathTree spta = new AStar().getShortestPathTree(req);

	        for(int j=0;j<vertices.length;j++) {
	        	State stateForVertexJ = spta.getState(vertices[j]);
	        	if (stateForVertexJ!=null)
	        		results[i][j] = (long) stateForVertexJ.getWeight();
	        	else 
					throw new NoPathFoundException(vertices[i],vertices[j],i,j);
	        	//stateForVertexJ.dumpPath();
	        	if (results[i][j]> MAX_POSSIBLE_DURATION)
					throw new NoPathFoundException(vertices[i],vertices[j],i,j);
			}
	    	req.cleanup();
	    }
		return results;
	}

	public long[][] calculateTimeMatrixBatch(Coordinate[] points) throws NoPathFoundException {
		long[][] results = new long[points.length][points.length];		
		
	    for(int i=0;i<points.length;i++) {
	    	LOG.info("Working on trips from " + points[i] + " in router " + otpRouter.id);
	        RoutingRequest req = getRoutingRequest(points[i],null);
	        
	    	ShortestPathTree spta = new AStar().getShortestPathTree(req);

	        for(int j=0;j<points.length;j++) {
//	        	State s = spta.getState(samples[j].v0);
//	        	s.dumpPath();
	        	StreetVertex streetVertexFromPointJ = getNearbyStreetVertex(points[j]);
				results[i][j]= (long) spta.getState(streetVertexFromPointJ).getWeight();

				if (results[i][j]>MAX_POSSIBLE_DURATION) {
					throw new NoPathFoundException(points[i], points[j],i,j);
				}
			}
	    	req.cleanup();
	    }
		return results;
	}
	
	public long calculateTrip(Coordinate from,Coordinate to) 
			throws NoPathFoundException {
        RoutingRequest req = getRoutingRequest(from,to);

		ShortestPathTree spta = new AStar().getShortestPathTree(req);
		if (spta.getPaths().isEmpty())
			throw new NoPathFoundException(from,to,0,0);
		GraphPath gp = spta.getPaths().get(0);
		//gp.dumpPathParser();
		return (long) gp.getWeight();
	}

	/** This allows to test the intersections wihtout adding the delays to the graphs. It sets the 
	 * vertices of the intersection
	 * @param intersections
	 * @throws UnknownIntersectionException
	 */
	public void findVertices(List<DelayedIntersectionZone> intersections) throws UnknownIntersectionException {
		ArrayList<DelayedIntersectionZone> invalidIntersections = new ArrayList<DelayedIntersectionZone>();
		for(DelayedIntersectionZone anIntersection: intersections)
			findVertices(anIntersection);
		for(DelayedIntersectionZone anIntersection: intersections)
			if (anIntersection.streetVertices.isEmpty())
				invalidIntersections.add(anIntersection);
		if (!invalidIntersections.isEmpty())
			throw new UnknownIntersectionException(invalidIntersections);
	}
	
	/**
	 * Find all the street vertices that belong to a DelayedIntersectionZone and associate it with them. 
	 * @param anIntersection
	 * @throws UnknownIntersectionException
	 */
	public void findVertices(DelayedIntersectionZone anIntersection) throws UnknownIntersectionException {
		for(StreetVertex sVertex: getStreetVerticesInBuffer(
				anIntersection.coordinate,DelayedIntersectionZone.DELAYED_ZONE_RADIUS))
			anIntersection.streetVertices.add(sVertex);
	}
		                                           
	/** This sets the delays in the graph. It assumes that findVertices has been called before */
	public void setDelays(List<DelayedIntersectionZone> intersections) {
	for(DelayedIntersectionZone anIntersection: intersections) 
			setDelay(anIntersection);
	}

	/* Must make sure that findVertices has been called before this. */
	public void setDelay(DelayedIntersectionZone intersection)  {
		for(StreetVertex sVertex: intersection.streetVertices) {
			LOG.debug("Dealing with " + sVertex.getLabel());
			Iterator<Edge> it = sVertex.getIncoming().iterator();
			LOG.debug("Incoming edges:");
			while (it.hasNext()) {
				try {
					StreetEdge se = (StreetEdge) it.next();
					if (intersection.streetVertices.contains(se.getFromVertex())) {
						intersection.streetEdgesWithin.add(se);
						LOG.debug("Within ... From " + se.getFromVertex().getLabel() + " to " + se.getToVertex());
					}
					// the other end of the edge is not in the buffer zone
					else {
						LOG.debug("Coming in ... From " + se.getFromVertex().getLabel() + " to " + se.getToVertex());
						intersection.streetEdgesComingIn.add(se);
						se.delay = intersection.delay;
					}  
				} catch (ClassCastException e) {
					LOG.info("Ignoring edge of class" + e.getClass());
				}
			}
		}
	}

	/* Can be called to cancel the effect of setDelays. SetDelays can be called again. */ 
	public void zeroDelays(List<DelayedIntersectionZone> intersections) throws UnknownIntersectionException {
		for(DelayedIntersectionZone anIntersection: intersections)
			for(StreetEdge delayedEdge: anIntersection.streetEdgesComingIn) 
				delayedEdge.delay = 0;
	}

	public void zeroDelays() throws UnknownIntersectionException {
		for(Edge e: otpRouter.graph.getEdges())
			if (e instanceof StreetEdge)
				((StreetEdge)e).delay = 0;
	}

	public List<DelayedIntersectionZone> getRandomIntersections(int nbIntersections) throws Exception {
		int maxDelayValue = 11;
		Random delay = new Random();
		
		List<DelayedIntersectionZone> randomIntersections = new ArrayList<DelayedIntersectionZone>();
		Vertex[] vertices = otpRouter.graph.getVertices().toArray(new Vertex[0]);
		ps.format("Drawing %s random vertices from total of %s\n",nbIntersections,vertices.length);
		// draw a random positive number
		int start = (new Random()).nextInt(Integer.MAX_VALUE);
		HashSet<StreetVertex> allVerticesDelayed = new HashSet<StreetVertex>();
		int successes=0;
		int attempts = 0;
		
		while(successes<nbIntersections && attempts<2*nbIntersections) {
			attempts++;
			LOG.info("successes so far " + successes);
			StreetVertex sv = (StreetVertex) vertices[(start+attempts)%vertices.length];
			LOG.info("trying " + sv.getLabel());
			// we don't want to pick overlapping intersections
			if (allVerticesDelayed.contains(sv))
				continue;
			DelayedIntersectionZone di = new DelayedIntersectionZone("Random"+successes, 
					new Coordinate(sv.getCoordinate()),delay.nextInt(maxDelayValue));
			findVertices(di);
			allVerticesDelayed.addAll(di.streetVertices);
			randomIntersections.add(di);
			successes++;
		}
		
		return randomIntersections;
	}
	
	/**
	 * A method used to find some random test points which are all within reach of each other i.e
	 * which are all located within the main graph.
	 */
	private void setTestPoints() {
		Random random = new Random();
		
		ps.format("Setting up %s test points\n",NB_TEST_POINTS);
		StreetVertex[] arrayOfVertices = otpRouter.graph.getVertices().toArray(new StreetVertex[0]);		
		testVertices= new StreetVertex[NB_TEST_POINTS];
		
		for(int i=0;i<NB_TEST_POINTS;i++) {
			testVertices[i] = arrayOfVertices[random.nextInt(arrayOfVertices.length)];
			ps.println("Starting with test vertices: " + testVertices[i]);
		}
		while (true) {
			try {
				calculateTimeMatrix(testVertices);
			} catch (NoPathFoundException e) {
				ps.format("Replacing test points %s and %s and trying again\n", testVertices[e.fromIndex],testVertices[e.toIndex]);
				testVertices[e.fromIndex] = arrayOfVertices[random.nextInt(arrayOfVertices.length)];
				testVertices[e.toIndex] = arrayOfVertices[random.nextInt(arrayOfVertices.length)];
				continue;
			}
			break;
		}
		ps.format("%s test points have been set\n", NB_TEST_POINTS);
	}

	/**
	 * Given a coordinate and a search radius, this method returns all the street vertices which are at
	 * a distance of the coordinate smaller than the search radius.
	 * @param c
	 * @param searchRadiusInMeters
	 * @return
	 */
	public List<StreetVertex> getStreetVerticesInBuffer(Coordinate c, double searchRadiusInMeters) {
		com.vividsolutions.jts.geom.Coordinate vC = c.getVCoordinate();
		double dLon = SphericalDistanceLibrary.metersToLonDegrees(searchRadiusInMeters,vC.y);
        double dLat = SphericalDistanceLibrary.metersToDegrees(searchRadiusInMeters);        
        Envelope envelope = new Envelope(vC);
        envelope.expandBy(dLon, dLat);

        List<StreetVertex> sortedList = new ArrayList<StreetVertex>() ;
        for (Vertex v : otpRouter.graph.streetIndex.getVerticesForEnvelope(envelope)) {
            if (v instanceof StreetVertex)
                sortedList.add((StreetVertex)v);
            else
            	ps.format("Vertex %s not of type StreetVertex but %s\n",v.toString(),v.getClass());
        }
        Collections.sort(sortedList,new DistanceFromRefComparator(c));
		return sortedList;
	}

	
    public StreetVertex getNearbyStreetVertex(Coordinate c) {
		List<StreetVertex> nearbyList = null;
		double searchRadiusInMeters = SEARCH_RADIUS_IN_METERS;
		// double the search radius until you get a non-empty list or until it gets ridiculous
		while (nearbyList==null || nearbyList.isEmpty() || searchRadiusInMeters > MAX_SEARCH_RADIUS_IN_METERS) {
			nearbyList = getStreetVerticesInBuffer(c, searchRadiusInMeters);
			searchRadiusInMeters*=2;
		}
		return (nearbyList==null && nearbyList.isEmpty())?null:nearbyList.get(0);
    }

	public StreetVertex getNearestConnectedStreetVertex(Coordinate c) {
		double searchRadiusInMeters = SEARCH_RADIUS_IN_METERS;
		HashSet<StreetVertex> triedVertices = new HashSet<StreetVertex>();
		
		while(true) {
			List<StreetVertex> streetVerticesInBuffer = getStreetVerticesInBuffer(c, searchRadiusInMeters);
			streetVerticesInBuffer.remove(triedVertices);
			
			if (!streetVerticesInBuffer.isEmpty()) {
				ps.format("%s has %s intersections within %s meters. Checking each for connectivity ...\n",
							c,streetVerticesInBuffer.size(),searchRadiusInMeters);
				tryVertex:
				for (StreetVertex candidateVertex : streetVerticesInBuffer) {
					// keep track of already tried vertices
					triedVertices.add(candidateVertex);
					ps.format("Checking if vertex %s can reach and be reached from test points.\n",candidateVertex);
					for(Vertex testPoint: testVertices) {
						try {
							calculateTrip(new Coordinate(candidateVertex.getCoordinate()),new Coordinate(testPoint.getCoordinate()));
							calculateTrip(new Coordinate(testPoint.getCoordinate()),new Coordinate(candidateVertex.getCoordinate()));
						} catch (NoPathFoundException e) {
							ps.format("Candidate vertex %s discarded as it cannot reach test point %s\n",candidateVertex,testPoint);
							continue tryVertex;
						}
					}
					ps.format("Candidate vertex %s can reach all test points and will be used\n",candidateVertex);
					return candidateVertex;
				}
			}
			// nothing good found yet, double buffer size
			searchRadiusInMeters *= 2;
			ps.format("No satisfactory vertex found. Doubling search radius to %s\n",searchRadiusInMeters);
		}
	}
	
	/* Compare according to the distance to refPoint. Further away points are greater than close ones. */
	public static class DistanceFromRefComparator implements Comparator<Vertex> {
		com.vividsolutions.jts.geom.Coordinate refPoint;

		public DistanceFromRefComparator(Coordinate point) {
			refPoint = point.getVCoordinate();
		}

		public DistanceFromRefComparator(com.vividsolutions.jts.geom.Coordinate point) {
			refPoint = point;
		}
		
		/*  Returns a negative integer, zero, or a positive integer 
		 * as the first argument is less than, equal to, or greater than the second.*/
		@Override
		public int compare(Vertex arg0, Vertex arg1) {
			if (SphericalDistanceLibrary.distance(refPoint, arg0.getCoordinate()) > 
			SphericalDistanceLibrary.distance(refPoint, arg1.getCoordinate()))
				return 1;
			return -1;
		}
	}
}
