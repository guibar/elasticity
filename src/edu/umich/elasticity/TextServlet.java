package edu.umich.elasticity;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.umich.elasticity.model.AnalysisUnitDefinitionException;
import edu.umich.elasticity.model.DelayedIntersectionZone;
import edu.umich.elasticity.model.Scenario;

/**
 * Servlet for dealing with Text UI
 */
// @WebServlet("/MainServlet")
public class TextServlet extends HttpServlet {
	private static final long serialVersionUID = 3427067916096169575L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TextServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// get parameters, check them and a specific exception should be thrown
		// in case
		// there is a problem with the parameters
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Job Submission Response</title>");
        out.println("</head>");
        out.println("<body>");

        // form Parameters
		String emailP = request.getParameter("email");
		String countiesP = request.getParameter("counties");
		String intersectionsP = request.getParameter("intersections");
		String jobsP = request.getParameter("jobs");
		String ctDevP = request.getParameter("unitdev");
		String unitP = request.getParameter("unit");
		String populationP = request.getParameter("population");
		String impedanceP = request.getParameter("impedance");
		boolean fastAnalysisP = request.getParameterValues("fast")!=null;
		
		boolean errorsDetected = false;
		int population=0,jobs=0;
		long ctDev=0;
		ArrayList<Integer> counties = new ArrayList<Integer>();		
		double impedance=Scenario.defaultImpedance;
        
		for(String county:countiesP.split(",")) {
			if (county.length()!=5) {
				errorsDetected = true;
				out.append(county + " should be a 5 digits number to be a valid county FIPS<br>\n");
			} else try {
				counties.add(Integer.valueOf(county));
			} catch (NumberFormatException e) {
				errorsDetected = true;
				out.append(county + " should be a 5 digits number to be a valid county FIPS<br>\n");
			}
		}
		try {
			population = populationP.equals("")?0:Integer.valueOf(populationP);
		} catch (NumberFormatException e) {
			errorsDetected = true;
			out.append("Population should be a number<br>\n");
		}
		try {
			jobs = jobsP.equals("")?0:Integer.valueOf(jobsP);
		} catch (NumberFormatException e) {
			errorsDetected = true;
			out.append("Jobs should be a number<br>\n");
		}
		try {
			// convert impedance to inverse seconds
			impedance = impedanceP.equals("")?Scenario.defaultImpedance:(Double.valueOf(impedanceP)/60);
		} catch (NumberFormatException e) {
			errorsDetected = true;
			out.append("Impedance should be a number between 0.05 and 0.5<br>\n");
		}
		if (unitP.equals("ct") && ctDevP.length()!=11) {
			errorsDetected = true;
			out.append(ctDevP + " should be an 11 digit number to be a valid census tract FIPS<br>\n");
		} 
		if (unitP.equals("bg") && ctDevP.length()!=12) {
			errorsDetected = true;
			out.append(ctDevP + " should be a 12 digit number to be a valid block group FIPS<br>\n");
		} else try {
			ctDev = Long.valueOf(ctDevP);
		} catch (NumberFormatException e) {
			errorsDetected = true;
			out.append("Census Tract of development should be an 11 digit number<br>\n");
		}
		
		ArrayList<DelayedIntersectionZone> intersections = new ArrayList<DelayedIntersectionZone>();
		String[] lines = intersectionsP.split("\\r?\\n");
		for(String line:lines)
			try {
				intersections.add(new DelayedIntersectionZone(line));
			} catch (InvalidDelayFormatException e) {
				errorsDetected = true;
				out.append("Invalid intersection:<br>" + e.message + "<br>\\n");
			}
		if (!errorsDetected) {
			Scenario s;
			try {
				ExecutorService executor = (ExecutorService)getServletContext().getAttribute(MyServletContextListener.EXECUTOR_NAME);
				s = new Scenario(emailP,counties,ctDev,jobs,population,impedance,intersections,unitP.equals("ct"),fastAnalysisP);
				executor.submit(s);
				out.append("Your job has been successfully submitted with the id: " + s.directory.getName());
				out.append("<br>\n");
				out.append("When the job is completed, you will receive an email with the results.");
			} catch (AnalysisUnitDefinitionException e) {
				out.append(e.getMessage());
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
        out.println("</body>");
        out.println("</html>");
	}
}
