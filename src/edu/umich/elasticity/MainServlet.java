package edu.umich.elasticity;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.umich.elasticity.model.Coordinate;
import edu.umich.elasticity.model.DelayedIntersectionZone;
import edu.umich.elasticity.model.Scenario;

/**
 * Servlet implementation class MainServlet
 */
// @WebServlet("/MainServlet")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 3427067916096169575L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MainServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// get parameters, check them and a specific exception should be thrown
		// in case
		// there is a problem with the parameters
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Job Submission Response</title>");
        out.println("</head>");
        out.println("<body>");

//        for(String key: request.getParameterMap().keySet())
//        	out.println("<p>" + key + "," + request.getParameter(key) + "</p>");
//
//        out.println("</body>");
//        out.println("</html>");
//        if (true)
//        	return;
        
		boolean errorsDetected = false;

		// Unit Type
        boolean unitTypeisCtP = request.getParameter("unitType").equals("ct");

		ArrayList<Integer> counties = new ArrayList<Integer>();
		for(String county:request.getParameterValues("selectedCountiesAnalysis")) {
			counties.add(Integer.valueOf(county));
		}
        // Unit Dev Fips
		long devUnitFips = Long.valueOf(request.getParameter("devUnitFips"));
		// Population
		int	population = Integer.valueOf(request.getParameter("population"));
		// Jobs
		int jobs = Integer.valueOf(request.getParameter("jobs"));
		// Impedance
			// convert impedance to inverse seconds
		double impedance = Double.valueOf(request.getParameter("impedance"))/60;
				
		boolean fastAnalysisP = request.getParameterValues("sample").equals("yes");
		String emailP = request.getParameter("email");

    	ArrayList<DelayedIntersectionZone> intersections = new ArrayList<DelayedIntersectionZone>();

    	int maxIntersectionIndex = Integer.valueOf(request.getParameter("maxIntersectionIndex"));
    	
    	
		for(int i=1;i<=maxIntersectionIndex;i++) {
			// if this is a missing number, go to next one
			if (request.getParameter("desc"+i)==null)
				continue;
				Double latitude = Double.valueOf(request.getParameter("lat"+i));
				Double longitude = Double.valueOf(request.getParameter("long"+i));
				Double delay = Double.valueOf(request.getParameter("delay"+i));
		    	DelayedIntersectionZone intersection =  
		    			new DelayedIntersectionZone(request.getParameter("desc"+i),new Coordinate(latitude,longitude),delay);
		    	intersections.add(intersection); 
		}
		
		if (!errorsDetected) {
			Scenario s;
			try {
				ExecutorService executor = (ExecutorService)getServletContext().getAttribute(MyServletContextListener.EXECUTOR_NAME);
				s = new Scenario(emailP,counties,devUnitFips,jobs,population,impedance,intersections,unitTypeisCtP,fastAnalysisP);
				executor.submit(s);
				
				out.append("<h2>Job successfully submitted</h2>");
				out.append(String.format("<p>Your job has been successfully submitted and has been "
						+ "assigned the id <b>%s</b>.",s.directory.getName()));
				out.append(String.format("<p>When the job is completed, an email will be sent to <b>%s</b> with the results.<p>",s.userEmail));
				out.append("<h2>Job Recap</h2>");
				out.append(s.toHtml());
				out.append("<p><a href=\".\\index.html\">Submit another job</a></p>");
			} catch (Exception e) {
				out.append(e.getMessage());
			}
		}
        out.println("</body>");
        out.println("</html>");
	}
}
