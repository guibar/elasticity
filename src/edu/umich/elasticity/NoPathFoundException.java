package edu.umich.elasticity;

import org.opentripplanner.routing.vertextype.StreetVertex;

import edu.umich.elasticity.model.Coordinate;

public class NoPathFoundException extends Exception {
	private static final long serialVersionUID = 1L;
	Coordinate from,to;
	int fromIndex,toIndex;
	StreetVertex fromSV,toSV;
	
	public NoPathFoundException(Coordinate from, Coordinate to,int fromIndex,int toIndex) {
		super();
		this.from = from;
		this.to = to;
		this.fromIndex = fromIndex;
		this.toIndex = toIndex;
	}

	public NoPathFoundException(StreetVertex from, StreetVertex to,int fromIndex,int toIndex) {
		super();
		fromSV = from;
		toSV = to;
		this.fromIndex = fromIndex;
		this.toIndex = toIndex;
	}

	public String toString() {
		return String.format("NoPathFoundException from point %s (%s) to point %s (%s)",fromIndex,fromSV,toIndex,toSV);
	}
}
