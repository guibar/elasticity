package edu.umich.elasticity;

public class InvalidDelayFormatException extends Exception {
	public String message;
	public InvalidDelayFormatException(String message) {
		this.message=message;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1852378372840811308L;

}
