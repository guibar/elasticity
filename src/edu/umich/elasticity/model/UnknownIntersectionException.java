package edu.umich.elasticity.model;

import java.util.ArrayList;

public class UnknownIntersectionException extends Exception {
	private static final long serialVersionUID = 1L;
	ArrayList<DelayedIntersectionZone> invalidIntersections;
	
	public UnknownIntersectionException(ArrayList<DelayedIntersectionZone> invalidIntersections) {
		this.invalidIntersections = invalidIntersections;
	}

	public String resubmitMessage() {
		StringBuffer sb = new StringBuffer("The following intersections cannot be mapped to valid OpenStreetMap intersections.\n");
		for (DelayedIntersectionZone intersection: invalidIntersections)
			sb.append(intersection.lineWithTabs()); 
		sb.append("\nPlease provide more precise coordinates and resubmit your job.");
//		sb.append("\nGo to the submission page: " + Scenario.submissionPage);		
		return sb.toString();
	
	}

}
