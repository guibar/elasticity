package edu.umich.elasticity.model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class State {
	public int fips;
	public String usps;
	public String name;

	public State(int fips, String usps, String name) {
		super();
		this.fips = fips;
		this.usps = usps;
		this.name = name;
	}
	
	static List<String> getStatesForCounties(List<Integer> countiesFips) throws SQLException, ClassNotFoundException  {
		ArrayList<String> statesUSPS = new ArrayList<String>();
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(Scenario.dbUrl);
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.
	
			StringBuilder fromWherePartOfQuery = new StringBuilder("select usps FROM county,state "
					+ "WHERE state.fips = county.state_fips and county.fips in (");
			for(int fips: countiesFips)
				fromWherePartOfQuery.append(fips+",");
			// remove last ',' and add closing )
			fromWherePartOfQuery.deleteCharAt(fromWherePartOfQuery.length()-1);
			fromWherePartOfQuery.append(") group by state.usps");

			ResultSet rs = statement.executeQuery(fromWherePartOfQuery.toString());
			while (rs.next())
				statesUSPS.add(rs.getString("usps"));
			return statesUSPS;
		} finally {
		if (connection != null)
			connection.close();
		}		
	}
	
	public static ArrayList<State> getStatesFips() throws ClassNotFoundException,SQLException {
		ArrayList<State> stateList = new ArrayList<State>();
		
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(Scenario.dbUrl);
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.
	
			ResultSet rs = statement.executeQuery("select fips,usps,name from state order by usps");
			while (rs.next())
				stateList.add(new State(rs.getInt("fips"),rs.getString("usps"),rs.getString("name")));
			return stateList;
		} finally {
			if (connection != null)
				connection.close();
		}
	}

	/* We only want the 3 digits of the county code i.e. the last 3 digits of the fips */
	public List<Integer> getCountiesFips() throws ClassNotFoundException, SQLException, IOException {
		Class.forName("org.sqlite.JDBC");
		ArrayList<Integer> result = new ArrayList<Integer>();
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(Scenario.dbUrl);
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.
		
			ResultSet rs = statement.executeQuery(String.format("select fips from county where state_fips = '%s'",fips));
			while (rs.next())
				// mod 1000 to get only the last 3 digits
				result.add(rs.getInt("fips")%1000);
			return result;		
		} finally {
		if (connection != null)
			connection.close();
		}		
	}
	
	public static State getState(String usps) throws ClassNotFoundException,SQLException {		
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(Scenario.dbUrl);
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.
		
			ResultSet rs = statement.executeQuery(String.format("select fips,usps,name from state where usps = '%s'",usps));
			while (rs.next())
				return new State(rs.getInt("fips"),rs.getString("usps"),rs.getString("name"));
			return null;		
		} finally {
		if (connection != null)
			connection.close();
		}
	}

	static ArrayList<State> getStatesStartingWith(String s) throws ClassNotFoundException,SQLException {
		ArrayList<State> stateList = new ArrayList<State>();
		
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(Scenario.dbUrl);
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30); // set timeout to 30 sec.
	
		ResultSet rs = statement.executeQuery("select fips,usps,name from state where name LIKE '" + s + "%' order by usps");
		while (rs.next())
			stateList.add(new State(rs.getInt("fips"),rs.getString("usps"),rs.getString("name")));
		return stateList;
		
		} finally {
		if (connection != null)
			connection.close();
		}
	}

	public static int getNumberOfCensusTractForState(String usps) throws ClassNotFoundException, SQLException {
		int result = 0;
		// load the sqlite-JDBC driver using the current class loader
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		// create a database connection
		// could also be DriverManager.getConnection("jdbc:sqlite:C:/work/mydatabase.db");
		try {
			connection = DriverManager.getConnection(Scenario.dbUrl);
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select count(*) from census_tract,county,state "
					+ "where census_tract.county_fips=county.fips and county.state_fips=state.fips "
					+ "and state.usps='" + usps + "'");
			while (rs.next())
				result = rs.getInt(1);
			return result;
		} finally {
			if (connection != null)
				connection.close();
		}
	}
}
