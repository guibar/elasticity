package edu.umich.elasticity.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.StringJoiner;

public class County {
	int fips;
	String name;
	int population;
	int jobs;
	
	public static County getCounty(int fips) throws ClassNotFoundException, SQLException {
		// load the sqlite-JDBC driver using the current class loader
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(Scenario.dbUrl);
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(
					"SELECT county.fips, county.name, sum(population) as population, sum(jobs) as jobs "
					+ "FROM census_tract, county "
					+ "WHERE census_tract.county_fips = county.fips and county.fips = " + fips);
			County county = new County();
			while (rs.next()) {
				// read the result set
				county.fips = rs.getInt("fips");
				county.name = rs.getString("name");
				county.population = rs.getInt("population");
				county.jobs = rs.getInt("jobs");
			}
			return county;
		} finally {
		if (connection != null)
			connection.close();
		}
	}

	public static int getNbCTs(List<Integer> countiesFips) throws ClassNotFoundException, SQLException {
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;

		try {
			// create a database connection
			// could also be DriverManager.getConnection("jdbc:sqlite:C:/work/mydatabase.db");
			connection = DriverManager.getConnection(Scenario.dbUrl);
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.
			
			StringBuilder fromWherePartOfQuery = new StringBuilder("FROM census_tract "
					+ "WHERE county_fips in (");
			for(int fips: countiesFips)
				fromWherePartOfQuery.append(fips+",");
			// remove last ',' and add closing )
			fromWherePartOfQuery.deleteCharAt(fromWherePartOfQuery.length()-1);
			fromWherePartOfQuery.append(")");
			
			// find # of records first, so that we can use arrays
			ResultSet rs = statement.executeQuery("SELECT COUNT(*) " + fromWherePartOfQuery.toString());
			rs.next();
			return rs.getInt(1);
		}	finally {
		if (connection != null)
			connection.close();
		}
	}

	boolean hasCT(long ctFips) {
		return String.valueOf(ctFips).startsWith(String.valueOf(fips));
	}
	
	int getPopulationAfter(Scenario s) {
		return hasCT(s.modUnitFips)?population+s.populationAdded:population;
	}
	
	int getJobsAfter(Scenario s) {
		return hasCT(s.modUnitFips)?jobs+s.jobsAdded:jobs;
	}

	static String lineHeader() {
		StringJoiner joiner = new StringJoiner(Results.delimiter);
		joiner.add("Fips").add("Name");
		joiner.add("Population before").add("Population after");
		joiner.add("Jobs before").add("Jobs after");
		return joiner.toString() + Results.newLine;		
	}

	String lineOutput(Scenario s) {
		StringJoiner joiner = new StringJoiner(Results.delimiter);
		joiner.add(String.valueOf(fips)).add(name);
		joiner.add(String.valueOf(population)).add(String.valueOf(getPopulationAfter(s)));
		joiner.add(String.valueOf(jobs)).add(String.valueOf(getJobsAfter(s)));
		return joiner.toString() + Results.newLine;
	}

	public static BoundingBox getBoundingBox(List<Integer> countiesFips) throws ClassNotFoundException,SQLException {		
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(Scenario.dbUrl);
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.
	
			StringBuilder fromWherePartOfQuery = new StringBuilder("FROM county "
					+ "WHERE fips in (");
			for(int fips: countiesFips)
				fromWherePartOfQuery.append(fips+",");
			// remove last ',' and add closing )
			fromWherePartOfQuery.deleteCharAt(fromWherePartOfQuery.length()-1);
			fromWherePartOfQuery.append(")");
	
			double minLatSouth = 90,maxLatNorth=-90,maxLonEast=-180,minLonWest=180;
			ResultSet rs = statement.executeQuery("SELECT minLat,minLon,maxLat,maxLon " + fromWherePartOfQuery.toString());
			while (rs.next()) {
				minLatSouth = Math.min(rs.getDouble(1),minLatSouth); 
				minLonWest = Math.min(rs.getDouble(2),minLonWest); 
				maxLatNorth = Math.max(rs.getDouble(3),maxLatNorth); 
				maxLonEast = Math.max(rs.getDouble(4),maxLonEast); 
			}
			// add some buffering to avoid weird problems with missing parts of the graph
			return new BoundingBox(minLatSouth-Scenario.degreeLatBuffer, maxLonEast+Scenario.degreesLonBuffer,
									maxLatNorth+Scenario.degreeLatBuffer, minLonWest-Scenario.degreesLonBuffer);
		} finally {
		if (connection != null)
			connection.close();
		}
	}
}
