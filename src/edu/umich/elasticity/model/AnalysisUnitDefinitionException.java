package edu.umich.elasticity.model;

public class AnalysisUnitDefinitionException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public AnalysisUnitDefinitionException(String msg) {
		super(msg);
	}

}
