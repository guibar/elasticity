package edu.umich.elasticity.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.StringJoiner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Results {
	static final Logger LOG = LoggerFactory.getLogger(Results.class);
	public static String[] fileNames = new String[]{"main.csv","travel-times-before.csv",
			"travel-times-after.csv","travel-times-diff.csv","details.csv"};

	public static CharSequence delimiter = ",";
	public static String osmNodeLinkTemplate = "http://www.openstreetmap.org/node/%s";
	
	static CharSequence newLine = "\n";

	Scenario scenario;
	public File[] outputFiles;

	// travelTimeBefore[i][j] is the time it take to go from ct[i] to ct[j]
	// before the development and after
	long[][] travelTimeBefore;
	long[][] travelTimeAfter;
	long[][] diffMatrix;

	// stats
	public double[] accessBefore;
	public double[] accessAfter;
	double sumPiBefore,avgPiBefore,sumPiAfter,avgPiAfter;
	double sumJiBefore,avgJiBefore,sumJiAfter,avgJiAfter;
	double sumAiBefore,avgAiBefore,sumAiAfter,avgAiAfter;
	double sumAiPiBefore, sumAiPiAfter;
	double perCapitaAccessBefore, perCapitaAccessAfter;
	double elasticityP=Double.NaN,elasticityJ=Double.NaN;
	
	public Results(Scenario scenario) {
		this.scenario = scenario;

		accessBefore = new double[scenario.nbUnits];
		accessAfter = new double[scenario.nbUnits];

		outputFiles = new File[fileNames.length+1];
		for(int i=0;i<fileNames.length;i++)
			outputFiles[i] = new File(scenario.directory,fileNames[i]);
		outputFiles[outputFiles.length-1] = new File(scenario.directory,scenario.getName()+".zip");
	}

	void prepareOutput() throws ClassNotFoundException, IOException, SQLException {
		// check that values after are sensible compared to values before
		checkMatrices();
		updateDiagonal(travelTimeBefore);
		updateDiagonal(travelTimeAfter);
		
		// calculate stats before and after
		double[] statsBefore = calculateSumsAndAccessibility(travelTimeBefore, scenario.unitJobsBefore, scenario.unitPopulationBefore,
				accessBefore,scenario.impedance);
		sumAiPiBefore = statsBefore[0];
		sumPiBefore = statsBefore[1];
		avgPiBefore = sumPiBefore/scenario.nbUnits;
		perCapitaAccessBefore = statsBefore[2];
		sumAiBefore = statsBefore[3];
		avgAiBefore = sumAiBefore/scenario.nbUnits;
		sumJiBefore = statsBefore[4];
		avgJiBefore = sumJiBefore/scenario.nbUnits;

		double[] statsAfter = calculateSumsAndAccessibility(travelTimeAfter, scenario.unitJobsAfter, scenario.unitPopulationAfter, 
				accessAfter,scenario.impedance);
		sumAiPiAfter = statsAfter[0];
		sumPiAfter = statsAfter[1];
		avgPiAfter = sumPiAfter/scenario.nbUnits;
		perCapitaAccessAfter = statsAfter[2];
		sumAiAfter = statsAfter[3];
		avgAiAfter = sumAiAfter/scenario.nbUnits;
		sumJiAfter = statsAfter[4];
		avgJiAfter = sumJiAfter/scenario.nbUnits;

		calculateElasticity();
		saveResultsToFiles();
	}
	
	
	/**
	 * 
	 * @return an array with sumAiPi,sumPi,sumAi,sumPi and per capita accessibility which is sumAiPi/sumPi
	 */
	static double[] calculateSumsAndAccessibility(long[][] travelTimes, int[] jobs, int[] population, 
			double[] access, double impedance) {
		// Ai = sumj(exp(-impedance * travel timeij) * employmentj)
		for (int i = 0; i < jobs.length; i++) {
			access[i] = 0.0;
			for (int j = 0; j < jobs.length; j++)
				access[i] += Math.exp(-impedance * travelTimes[i][j]) * jobs[j];
		}

		// stats[0] is sumAiPi, stats[1] is sumPi, stats[3] is sumAi and stats[4] is sumJi
		double[] stats = new double[5];
		for (int i = 0; i < population.length; i++) {
			stats[0] += access[i] * population[i];
			stats[1] += population[i];
			stats[3] += access[i];
			stats[4] += jobs[i];
		}
		// Per capita accessibility = sumi(Ai * pi)/sumi(pi)
		stats[2] = stats[0] / stats[1];
		return stats;
	}

	/**
	 * Accessibility elasticity = 
	 * 		{ [sum(Ai after* pi after) - sum(Ai before* pi before)] / sum(Ai before * pi before) }
	 * 		/
	 *		{ [sum(pi after)- sum(pi before)]/ sum(pi before) }
	 */
	void calculateElasticity() {
		if (scenario.populationAdded!=0)
			elasticityP = ((sumAiPiAfter - sumAiPiBefore) / sumAiPiBefore) / ((sumPiAfter - sumPiBefore) / sumPiBefore);
		if (scenario.jobsAdded!=0)
			elasticityJ = ((sumAiPiAfter - sumAiPiBefore) / sumAiPiBefore) / ((sumJiAfter - sumJiBefore) / sumJiBefore);
	}

	
	/** 
	 * For travel time from a Census Tract to itself, calculate half the average time to the three closest 
	 * Census Tracts. Therefore all Census Tracts will have a positive, nonzero travel time to themselves 
	 * in this matrix. I added provisions in case we have less than 3 CTs to work with.
	 *
	 * @param matrix a matrix of travel times between CTs
	 */
	public static void updateDiagonal(long[][] matrix) {
		for(int row=0;row<matrix.length;row++) {
			// create an array with all non diagonal terms of row ...
			long[] toBeSorted = new long[matrix.length-1];
			for(int k=0,col=0;k<matrix.length-1;k++,col++) {
				// skip the diagonal
				if (col==row)
					col++;
				toBeSorted[k]=matrix[row][col];
			}
			// ... and sort it
			Arrays.sort(toBeSorted);
			
			matrix[row][row]= 0;
			// we never know, if we have a 3x3 matrix, we only want to use the average of the
			// other 2 CTs, not the 3, hence min(3,matrix.length-1)
			for(int j=0;j<Math.min(3, matrix.length-1);j++)
				matrix[row][row] += toBeSorted[j];
			matrix[row][row] /= (2*Math.min(3, matrix.length-1));
		}
	}
	
	void checkMatrices() {
		boolean error = false;
		StringBuilder message = new StringBuilder();
		for(int i=0;i<travelTimeBefore.length;i++)
			for(int j=0;j<travelTimeBefore[i].length;j++)
				if (travelTimeAfter[i][j]<travelTimeBefore[i][j]) {
					error = true;
					message.append(String.format(
							"Difference too small - Before:%s, After:%s from %s to %s \n",
							travelTimeBefore[i][j],travelTimeAfter[i][j],
							scenario.unitVertices[i].getLabel(),scenario.unitVertices[j].getLabel()));
					travelTimeAfter[i][j]=travelTimeBefore[i][j];
				}
				else if (travelTimeAfter[i][j]>travelTimeBefore[i][j]+scenario.totalDelays) {
					error = true;
					message.append(String.format(
							"Difference too big - Before:%s, After:%s from %s to %s \n",
							travelTimeBefore[i][j],travelTimeAfter[i][j],
							scenario.unitVertices[i].getLabel(),scenario.unitVertices[j].getLabel()));
					travelTimeAfter[i][j] = travelTimeBefore[i][j] +  Math.round(scenario.totalDelays);
				}
		if (error)
			scenario.ps.println(message.toString());
	}


	public File getZipFile() {
		return outputFiles[outputFiles.length-1];
	}

	public void saveResultsToFiles() throws ClassNotFoundException, IOException, SQLException {
		writeMainFile();
		writeTravelTimes();
		writeDetailsFile();
		createZip();
	}
	
	public void createZip() throws IOException {
		FileOutputStream fos = new FileOutputStream(outputFiles[outputFiles.length-1]);
		ZipOutputStream zos = new ZipOutputStream(fos);
		
		for(File f:outputFiles) {
			// the last one is the zip file itself 
			if (f.getName().endsWith(".zip"))
				continue;
			ZipEntry ze = new ZipEntry(f.getName());
			zos.putNextEntry(ze);
			FileInputStream in = new FileInputStream(f);
			
			int len;
			byte[] buffer = new byte[1024];
			while ((len = in.read(buffer)) > 0) {
				zos.write(buffer, 0, len);
			}
			in.close();
			zos.closeEntry();
		}			
		zos.close();
	}
	
	void writeMainFile() throws IOException, ClassNotFoundException, SQLException {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(outputFiles[0]));
			bw.write(getCounties());
			bw.write("Impedance" + delimiter + (scenario.impedance*60) + newLine + newLine);
			bw.write(getIntersections());
			bw.write(getGlobalStats());
			bw.write(getUnitsData());
		} finally {
			if (bw!=null)
				bw.close();
		}
	}

	void writeTravelTimes() throws IOException, ClassNotFoundException, SQLException {		
		BufferedWriter bwa = null;
		BufferedWriter bwb = null;
		BufferedWriter bwc = null;
		try {
			bwa = new BufferedWriter(new FileWriter(outputFiles[1]));
			bwa.write(formatMatrix(scenario.unitFips, travelTimeBefore));			
			bwb = new BufferedWriter(new FileWriter(outputFiles[2]));
			bwb.write(formatMatrix(scenario.unitFips, travelTimeAfter));			
			bwc = new BufferedWriter(new FileWriter(outputFiles[3]));
			diffMatrix = diff(travelTimeAfter,travelTimeBefore);
			bwc.write(formatMatrix(scenario.unitFips,diffMatrix));			
			
		} finally {
			if (bwa!=null)
				bwa.close();
			if (bwb!=null)
				bwb.close();
			if (bwc!=null)
				bwc.close();
		}
	}

	void writeDetailsFile() throws IOException, ClassNotFoundException, SQLException {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(outputFiles[4]));
			bw.write(getUnitsDataWithDiff());
		} finally {
			if (bw!=null)
				bw.close();
		}
	}

	String getUnitsDataWithDiff() throws SQLException, ClassNotFoundException {
		StringBuilder sb = new StringBuilder();
		sb.append(AnalysisUnit.lineHeaderDetails());
		for(int i=0;i<scenario.nbUnits;i++) {
			sb.append(AnalysisUnit.lineOutputDetails(scenario,i));
		}
		return sb.toString();
	}		

	String getUnitsData() throws SQLException, ClassNotFoundException {
		StringBuilder sb = new StringBuilder(scenario.isCT?"CENSUS TRACTS":"BLOCK GROUPS" + newLine);
		sb.append(AnalysisUnit.lineHeader());
		for(int i=0;i<scenario.nbUnits;i++) {
			sb.append(AnalysisUnit.lineOutput(scenario,i));
		}
		StringJoiner joiner = new StringJoiner(Results.delimiter);
		joiner.add("SUM")
		.add(String.valueOf(sumAiBefore)).add(String.valueOf(sumAiAfter))
		.add(String.valueOf(sumPiBefore)).add(String.valueOf(sumPiAfter))
		.add(String.valueOf(sumJiBefore)).add(String.valueOf(sumJiAfter));
		sb.append(joiner.toString() + Results.newLine);
		joiner = new StringJoiner(Results.delimiter);
		joiner.add("AVG")
		.add(String.valueOf(avgAiBefore)).add(String.valueOf(avgAiAfter))
		.add(String.valueOf(avgPiBefore)).add(String.valueOf(avgPiAfter))
		.add(String.valueOf(avgJiBefore)).add(String.valueOf(avgJiAfter));
		sb.append(joiner.toString() + Results.newLine);
		joiner = new StringJoiner(Results.delimiter);
		joiner.add("SUM(ACC*POP)")
		.add(String.valueOf(sumAiPiBefore)).add(String.valueOf(sumAiPiAfter));
		sb.append(joiner.toString() + Results.newLine);
		return sb.toString();
	}		

	String getIntersections() {
		StringBuilder sb = new StringBuilder("INTERSECTIONS" + newLine);
		sb.append(DelayedIntersectionZone.lineHeader());
		double totalDelay = 0;
		for(DelayedIntersectionZone i: scenario.intersections) {
			sb.append(i.lineOutput());
			totalDelay += i.delay;
		}
		sb.append("SUM" + delimiter + delimiter + delimiter + delimiter + delimiter + delimiter);
		sb.append(String.valueOf(totalDelay) + Results.newLine);
		sb.append(newLine);
		return sb.toString();		
	}
	
	String getCounties() throws SQLException, ClassNotFoundException {
		StringBuilder sb = new StringBuilder("COUNTIES" + newLine);
		int sumPopulation = 0;
		int sumJobs = 0;
		int n = scenario.countiesFips.size();
		sb.append(County.lineHeader());
		for(int fips: scenario.countiesFips) {
			sumPopulation+=County.getCounty(fips).population;
			sumJobs+=County.getCounty(fips).jobs;
			sb.append(County.getCounty(fips).lineOutput(scenario));
		}
		if (n>1) {
			StringJoiner joiner = new StringJoiner(Results.delimiter);
			joiner.add("SUM").add("")
			.add(String.valueOf(sumPopulation)).add(String.valueOf(sumPopulation+scenario.populationAdded))
			.add(String.valueOf(sumJobs)).add(String.valueOf(sumJobs+scenario.jobsAdded));
			sb.append(joiner.toString() + Results.newLine);
			joiner = new StringJoiner(Results.delimiter);
			joiner.add("AVG").add("")
			.add(String.valueOf(sumPopulation/n)).add(String.valueOf((sumPopulation+scenario.populationAdded)/n))
			.add(String.valueOf(sumJobs/n)).add(String.valueOf((sumJobs+scenario.jobsAdded)/n));
			sb.append(joiner.toString() + Results.newLine);
		}
		sb.append(newLine);
		return sb.toString();
	}
	
	String getGlobalStats() {
		StringBuilder sb = new StringBuilder();
		sb.append("Total regional accessibility before" + Results.delimiter);
		sb.append(String.valueOf(perCapitaAccessBefore*sumPiBefore) + Results.newLine);
		sb.append(Results.newLine);
		sb.append("Total regional accessibility after" + Results.delimiter);
		sb.append(String.valueOf(perCapitaAccessAfter*sumPiAfter) + Results.newLine);
		sb.append(Results.newLine);
		if (Double.isNaN(elasticityJ) && !Double.isNaN(elasticityP)) {
			sb.append("Accessibility elasticity" + Results.delimiter);
			sb.append(String.valueOf(elasticityP) + Results.newLine);
		} 
		else if (!Double.isNaN(elasticityJ) && Double.isNaN(elasticityP)) {
			sb.append("Accessibility elasticity" + Results.delimiter);
			sb.append(String.valueOf(elasticityJ) + Results.newLine);
		} 
		else if (!Double.isNaN(elasticityJ) && !Double.isNaN(elasticityP)) {
			sb.append("Accessibility elasticity (Pop)" + Results.delimiter);
			sb.append(String.valueOf(elasticityP) + Results.newLine);
			sb.append("Accessibility elasticity (Jobs)" + Results.delimiter);
			sb.append(String.valueOf(elasticityJ) + Results.newLine);			
		} 
		sb.append(Results.newLine);
		return sb.toString();				
	}


	public static String formatMatrix(long[][] matrix) {
		StringBuilder matrixString = new StringBuilder();
		for(int i=0;i<matrix.length;i++) {
			for(int j=0;j<matrix[i].length;j++)
				matrixString.append(matrix[i][j] + ",");
			matrixString.append("\n");
		}
		return matrixString.toString();
	}

	/** I am assume that both a and b have the same sizes */
	static long[][] diff(long[][] a,long[][] b) {
		long[][] diff = new long[a.length][a[0].length];
		for(int i=0;i<diff.length;i++)
			for(int j=0;j<diff[i].length;j++)
				diff[i][j] = a[i][j] - b[i][j];
		return diff;	
	}


	static String formatMatrix(long[] censusTractFips,long[][] travelTimeBefore) {
		StringJoiner joiner = new StringJoiner(Results.delimiter);
		joiner.add("Tract to Tract (seconds)"); //top left cell
		// get first line
		for(int i=0;i<censusTractFips.length;i++)
			 joiner.add(String.valueOf(censusTractFips[i]));
		StringBuilder sb = new StringBuilder(joiner.toString()+Results.newLine);
		// get following lines
		for(int i=0;i<censusTractFips.length;i++) {
			joiner = new StringJoiner(Results.delimiter);
			joiner.add(String.valueOf(censusTractFips[i]));
			for(int j=0;j<censusTractFips.length;j++)
				joiner.add(String.valueOf(travelTimeBefore[i][j]));
			sb.append(joiner.toString()+Results.newLine);			
		}		
		return sb.toString();				
	}	
}
