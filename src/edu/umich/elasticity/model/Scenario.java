package edu.umich.elasticity.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.StringJoiner;

import org.opentripplanner.routing.vertextype.StreetVertex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.umich.elasticity.MailService;
import edu.umich.elasticity.NoPathFoundException;
import edu.umich.elasticity.OSMService;
import edu.umich.elasticity.OTPWrapper;

import java.util.Date;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Scenario implements Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(Scenario.class);
    
	public static File root = System.getProperty("os.name").startsWith("Win")?
			new File("C:/Users/Gui/Documents/workspace/elasticity"):new File("/opt/elasticity");
	public static File workDir = new File(root,"work");
	public static File osmosisExe = System.getProperty("os.name").startsWith("Win")?
			new File(root,"osmosis/bin/osmosis.bat"):new File(root,"osmosis/bin/osmosis");
	public static File osmFilterExe = System.getProperty("os.name").startsWith("Win")?
					new File(root,"osmosis/bin/osmfilter.exe"):new File(root,"osmosis/bin/osmfilter");
	public static String dbUrl = "jdbc:sqlite:" + root + "/data/us_data.sqlite";
	static double degreesLonBuffer = 0.25, degreeLatBuffer = 0.25;
	// in inverse seconds
	public static double defaultImpedance = 0.125;
	static int fastAnalysisMaxUnits = 1000;
	
	public static boolean debug = System.getProperty("os.name").startsWith("Win");
	
	// meta data
	public PrintStream ps;
	Date timeStarted,timeFinished;
	public String userEmail;
	public File directory;
	public Results results;
	
	// geography
	boolean isCT = true;
	boolean fastAnalysis = false;
	public OTPWrapper router;
	StreetVertex[] unitVertices;
	public BoundingBox boundingBox;
	List<Integer> countiesFips;
	public List<String> statesUSPS;	
	int nbUnits;
	public long[] unitFips;
	public Coordinate[] unitCentroids;

	// development data
	long modUnitFips;
	int modUnitIndex = -1;
	int jobsAdded;
	int populationAdded;
	double impedance = defaultImpedance;
	public List<DelayedIntersectionZone> intersections;
	double totalDelays;
	// employment and jobs
	int[] unitPopulationBefore, unitPopulationAfter;
	int[] unitJobsBefore, unitJobsAfter;
	
	
	/** 
	 * Constructor used for testing OSMScenario
	 * @param userEmail
	 * @param countiesFips
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws AnalysisUnitDefinitionException 
	 * @throws NoVertexFoundException 
	 */
	
	public Scenario(String userEmail, ArrayList<Integer> counties, long fipsOfDev, int jobsCreated, 
			int populationAdded,double impedance, List<DelayedIntersectionZone> intersections, boolean isCT,boolean fastAnalysis) 
					throws SQLException, ClassNotFoundException, IOException, AnalysisUnitDefinitionException {
		this.userEmail = userEmail;
		timeStarted = new Date();
		this.countiesFips = counties;

		DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss");
		directory = new File(workDir,userEmail.replace("@", "_at_") + "_" + dateFormat.format(new Date()));
		directory.mkdir();
		ps = new PrintStream(new File(directory,"run.log"));
		
		this.jobsAdded = jobsCreated;
		this.populationAdded = populationAdded;
		this.modUnitFips = fipsOfDev;
		this.impedance = impedance;
		this.intersections = intersections;
		for(DelayedIntersectionZone intersection: intersections)
			totalDelays+=intersection.delay;
		
		this.isCT = isCT;
		this.fastAnalysis = fastAnalysis;
		
		statesUSPS = State.getStatesForCounties(countiesFips);
		boundingBox = County.getBoundingBox(countiesFips);
		populateAnalysisUnits();
		ps.println(this);
	}
	
	String getOsmIdForUnit(int i) {
		return unitVertices[i].getLabel();
	}
	
	void setUnitVertices() {
		unitVertices = new StreetVertex[unitCentroids.length];
		for(int i=0;i<unitCentroids.length;i++) {
			ps.println("Looking for intersection for CT " + unitFips[i]);
			unitVertices[i] = router.getNearestConnectedStreetVertex(unitCentroids[i]);
		}
	}
	
	/** 
	 * This is where the action really happens
	 */
	
	@Override
	public void run() {
		try {
			// this will do nothing if the osm has already been extracted
			OSMService.generateOSMFile(this);
		} catch (IOException e) {
			MailService.send(new String[]{userEmail,MailService.adminEmail},
					String.format("Problem with OSM data in job %s ",directory.getName()),e.toString(), null);
		}

		// In the test routines where we generate random scenarios, 
		// the router has already been set in the constructor
		if (router == null)
			router = new OTPWrapper(directory,getName(),ps);
		
		// check intersections are valid and associate them with StreetVertices
		try {
			router.findVertices(intersections);
		} catch (UnknownIntersectionException e) {
			// warn the user of the intersection issue and terminate the job
			MailService.send(new String[]{userEmail},
				String.format("Problem with intersection coordinates in job %s ",directory.getName()),e.resubmitMessage(), null);
			return;
		}
		
		// associate each analysis unit with a StreetVertex
		setUnitVertices();

		results = new Results(this);

		// calculate travel times, ... before
		try {
			results.travelTimeBefore = router.calculateTimeMatrix(unitVertices);
			// ... and after delays are added
			router.setDelays(intersections);
			results.travelTimeAfter = router.calculateTimeMatrix(unitVertices);

			// we are done with the router and can release it
		} catch (NoPathFoundException e) {
			MailService.send(new String[]{userEmail,MailService.adminEmail},String.format("No Path exception in job %s ",directory.getName()),e.toString(), null);
		}
		router.close();
		
		try {
			results.prepareOutput();
		} catch (ClassNotFoundException | IOException | SQLException e) {
			MailService.send(new String[]{userEmail,MailService.adminEmail},
				String.format("Problem generating output for job %s ",directory.getName()),e.toString(), null);
			return;
		}
		
		timeFinished = new Date();
		ps.println("Scenario with " + nbUnits + " CTs took " + (timeFinished.getTime()-timeStarted.getTime())/1000);
		
		// mail user with results
		if (!debug  && userEmail!=null) 
			MailService.send(new String[] {userEmail},"Results for Job " + directory.getName(),"Result files are zipped in the attachment.",results.getZipFile());
	}

	public String getName() {
		return directory.getName().replace(".", "-");
	}
			
	/**
	 * Go and get all the CTs belonging to the counties. Set the jobs and population before and after.
	 * Also set the index of the modified CT.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws AnalysisUnitDefinitionException 
	 */
	void populateAnalysisUnits() throws ClassNotFoundException, SQLException, AnalysisUnitDefinitionException {
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;

		try {
			// create a database connection
			// could also be DriverManager.getConnection("jdbc:sqlite:C:/work/mydatabase.db");
			connection = DriverManager.getConnection(dbUrl);
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.
			
			// SELECT block_group.* 
			// FROM block_group,census_tract 
			// WHERE block_group.ct_fips = census_tract.fips and census_tract.county_fips in (22001,22220)
			// SELECT census_tract.*
			// FROM census_tract 
			// WHERE county_fips in (22001,22220)

			String selectCount = "SELECT count(*) ";
			String select = isCT? "SELECT census_tract.* "	:"SELECT block_group.* ";
			String from =   isCT? "FROM census_tract "		:"FROM block_group,census_tract ";
			String where =  isCT? "WHERE county_fips in ("	:"WHERE block_group.ct_fips = census_tract.fips and census_tract.county_fips in (";
			
			StringBuilder whereSB = new StringBuilder(where);
			for(int fips: countiesFips)
				whereSB.append(fips+",");
			// remove last ',' and add closing )
			whereSB.deleteCharAt(whereSB.length()-1);
			whereSB.append(")");
			
			// find # of records first, so that we can use arrays
			ResultSet rs = statement.executeQuery(selectCount + from + whereSB.toString());
			rs.next();
			nbUnits = rs.getInt(1);

			if (nbUnits==0) {
				throw new AnalysisUnitDefinitionException("No analysis unit found. Your county(s) fips is probably invalid.");
			}
			
			else if (nbUnits==1)
				throw new AnalysisUnitDefinitionException("Only one single track found in these counties. Scenario is meaningless.");
			
			// set up the arrays
			unitFips = new long[nbUnits];
			unitCentroids = new Coordinate[nbUnits];
			unitPopulationBefore = new int[nbUnits];
			unitPopulationAfter = new int[nbUnits];
			unitJobsBefore = new int[nbUnits];
			unitJobsAfter = new int[nbUnits];
			
			// now populate the arrays
			ps.println("finding units with " + select + from + whereSB.toString());
			rs = statement.executeQuery(select + from + whereSB.toString());
			int i=0;
			while (rs.next()) {
				unitFips[i] = rs.getLong("fips");
				// set the index of the modified CT when you see it
				if (unitFips[i]==modUnitFips)
					modUnitIndex = i;
				
				unitCentroids[i] = new Coordinate(rs.getDouble("latCent"),rs.getDouble("lonCent"));
				try {
					unitPopulationBefore[i] = rs.getInt("population");
					unitPopulationAfter[i] = rs.getInt("population");
				} catch (SQLException e) {
					LOG.error("Null population value for Unit: " + unitFips[i]);
					LOG.error("Exception:" + e);
				}
				try {
					unitJobsBefore[i] = rs.getInt("jobs");
					unitJobsAfter[i] = rs.getInt("jobs");
				} catch (SQLException e) {
					LOG.error("Null jobs value for CT: " + unitFips[i]);
					LOG.error("Exception:" + e);
				}
				i++;
			}
			// this would be bizarre ...
			if (i!=nbUnits)
				throw new SQLException("Expecting " + nbUnits + " census tracts, found " + i);
			if (modUnitIndex==-1)
				throw new AnalysisUnitDefinitionException("The unit of development is not found amont the analysis units");
		} finally {
			if (connection != null)
				connection.close();
		}
		// update population and jobs after development 
		unitPopulationAfter[modUnitIndex]+=populationAdded;
		unitJobsAfter[modUnitIndex]+=jobsAdded;
		
		if (fastAnalysis && nbUnits > fastAnalysisMaxUnits)
			dropLeastPopulatedUnits();
	}
	
	/**
	 * This method is used to keep only the most populated units in a scenario.
	 */
	void dropLeastPopulatedUnits() {
		// Keep info about modUnit as we might need it later
		AnalysisUnit modUnit = 
				new AnalysisUnit(modUnitFips, unitCentroids[modUnitIndex],
						unitPopulationBefore[modUnitIndex], unitJobsBefore[modUnitIndex], isCT);

		// Sort returned units by decreasing population
		AnalysisUnit[] sortedUnitsByPopulation = new AnalysisUnit[nbUnits];
		for(int i=0;i<sortedUnitsByPopulation.length;i++)
			sortedUnitsByPopulation[i] = new AnalysisUnit(unitFips[i],unitCentroids[i],unitPopulationBefore[i],unitJobsBefore[i],isCT);
		Arrays.sort(sortedUnitsByPopulation, new PopulationComparator());

		// recreate new arrays with fastAnalysisMaxUnits elements
		nbUnits = fastAnalysisMaxUnits;
		unitFips = new long[nbUnits];
		unitCentroids = new Coordinate[nbUnits];
		unitPopulationBefore = new int[nbUnits];
		unitPopulationAfter = new int[nbUnits];
		unitJobsBefore = new int[nbUnits];
		unitJobsAfter = new int[nbUnits];
		modUnitIndex = -1;
		
		for(int i=0;i<nbUnits;i++) {
			unitFips[i] = sortedUnitsByPopulation[i].fips;
			unitCentroids[i] = sortedUnitsByPopulation[i].centroid;
			unitPopulationBefore[i] = sortedUnitsByPopulation[i].population;
			unitPopulationAfter[i] = sortedUnitsByPopulation[i].population;
			unitJobsBefore[i] = sortedUnitsByPopulation[i].jobs;
			unitJobsAfter[i] = sortedUnitsByPopulation[i].jobs;			
			if (unitFips[i]==modUnitFips) {
				modUnitIndex = i;				
			}
		}
		// the FIPS of the development is not in the most populous units, but we need it so we substitute
		// the last one by it
		if (modUnitIndex==-1) {
			modUnitIndex = nbUnits-1;
			unitFips[modUnitIndex] = modUnit.fips;
			unitCentroids[modUnitIndex] = modUnit.centroid;
			unitPopulationBefore[modUnitIndex] = modUnit.population;
			unitPopulationAfter[modUnitIndex] = modUnit.population;
			unitJobsBefore[modUnitIndex] = modUnit.jobs;
			unitJobsAfter[modUnitIndex] = modUnit.jobs;	
		}

		unitPopulationAfter[modUnitIndex]+=populationAdded;
		unitJobsAfter[modUnitIndex]+=jobsAdded;
	}
	
	class PopulationComparator implements Comparator<AnalysisUnit> {
		@Override
		public int compare(AnalysisUnit o1, AnalysisUnit o2) {
			return o2.population - o1.population;
		}
	}

	public String toHtml() {
		StringBuffer htmlIntersections = new StringBuffer(); 
		if (intersections!=null)
			for(DelayedIntersectionZone it: intersections)
				htmlIntersections.append("<tr><td>intersection</td><td>"+ it.toString() + "</td></tr>");

		StringJoiner countiesHtml = new StringJoiner("<br>");
		for(int c: countiesFips)
			countiesHtml.add(String.valueOf(c));
		
		String html = String.format("<table>"
				+ "<tr><td>Geographic Unit of Analysis</td><td>%s</td></tr>"
				+ "<tr><td>Counties in Analysis Zone</td><td>%s</td></tr>"
				+ "<tr><td>Number of Units in Analysis Zone</td><td>%d</td></tr>"
				+ "<tr><td>Fips of Development Unit</td><td>%d</td></tr>"
				+ "<tr><td>Population Added</td><td>%d</td></tr>"
				+ "<tr><td>Jobs Added</td><td>%d</td></tr>"
				+ "<tr><td>Impedance</td><td>%f</td></tr>"
				+ "%s"
				+ "<tr><td>Fast Analysis</td><td>%s</td></tr>"
				+ "<tr><td>User email</td><td>%s</td></tr>"
				+ "</table>", 
				(isCT?"Census Tract":"Block Group"),countiesHtml,nbUnits,modUnitFips,
				populationAdded,jobsAdded,impedance*60,htmlIntersections,fastAnalysis?"yes":"no",userEmail);
		return html;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("user email:"+userEmail+"\n");
		for(int c: countiesFips)
			sb.append("county:"+c+"\n");
		sb.append("fast analysis:"+ fastAnalysis +"\n");
		sb.append("analysis units:"+ (isCT?"CT":"BG") +"\n");
		sb.append("nb units:"+nbUnits+"\n");
		sb.append("modUnitFips:"+modUnitFips+"\n");
		sb.append("jobs added:"+jobsAdded+"\n");
		sb.append("population added:"+populationAdded+"\n");
		sb.append("impedance(inverse minutes):"+(impedance*60)+"\n");
		if (intersections!=null)
			for(DelayedIntersectionZone it: intersections)
				sb.append("intersection:"+it.toString());
		return sb.toString();
	}
	
	/******/
	
	/** This automatically fetches all the other CTs in the county to which ctFips belongs */
	public Scenario(long ctFips) throws ClassNotFoundException, SQLException, IOException, AnalysisUnitDefinitionException, Exception {
		timeStarted = new Date();
		AnalysisUnit unit = AnalysisUnit.getUnit(ctFips,true);
		countiesFips = new ArrayList<Integer>();
		countiesFips.add(unit.getCountyFIPS());
		DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss");
		
		directory = new File(workDir,"test_" + dateFormat.format(new Date()));
		directory.mkdir();
		ps = new PrintStream(new File(directory,"run.log"));
		ps.println("Selected CT:" + ctFips);
		
		this.jobsAdded = 15;
		this.populationAdded = 200;
		
		this.modUnitFips = unit.fips;

		statesUSPS = State.getStatesForCounties(countiesFips);
		boundingBox = County.getBoundingBox(countiesFips);
		populateAnalysisUnits();
		
		OSMService.generateOSMFile(this);
		router = new OTPWrapper(directory,getName(),ps);
		intersections = router.getRandomIntersections(50);
		ps.println(this);
	}

	public Scenario(File dir) throws ClassNotFoundException, SQLException, IOException, AnalysisUnitDefinitionException {
		timeStarted = new Date();
		directory = dir;

		modUnitFips = getCTmodFromFile();
		AnalysisUnit unit = AnalysisUnit.getUnit(modUnitFips,true);
		ps = new PrintStream(new File(directory,"run.log"));

		countiesFips = new ArrayList<Integer>();
		countiesFips.add(unit.getCountyFIPS());
				
		this.jobsAdded = 15;
		this.populationAdded = 200;
		
		statesUSPS = State.getStatesForCounties(countiesFips);
		boundingBox = County.getBoundingBox(countiesFips);
		populateAnalysisUnits();
		intersections = new ArrayList<DelayedIntersectionZone>();
		ps.println(this);
	}

	public long getCTmodFromFile() throws IOException {
		File log = new File(directory,"run.log");
		BufferedReader br = new BufferedReader(new FileReader(log));
		String line = null;
		while ((line=br.readLine())!=null) {
			if (line.split(":")[0].equals("CTmod")) {
				LOG.info("line:" + line);
				br.close();
				log.renameTo(new File(directory,"run1.log"));
				LOG.info("returning:" +  line.split(":")[1]);
				return Long.valueOf(line.split(":")[1]);
			}
		}
		br.close();
		return -1;
	}
}
