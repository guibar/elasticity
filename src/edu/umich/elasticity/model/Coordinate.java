package edu.umich.elasticity.model;

/* I could recycle this class to be OSM intersections and use the one from vividsolutions 
 * for generic coordinates.
 */
public class Coordinate {
	public double latitude;
	public double longitude;
	
	public Coordinate(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public Coordinate(com.vividsolutions.jts.geom.Coordinate coord) {
		this.longitude = coord.x;
		this.latitude = coord.y;
	}
	
	public com.vividsolutions.jts.geom.Coordinate getVCoordinate() {
		return new com.vividsolutions.jts.geom.Coordinate(longitude, latitude);
	}
	
	public String toString() {
		return "("+latitude+","+longitude+")";
	}
}
