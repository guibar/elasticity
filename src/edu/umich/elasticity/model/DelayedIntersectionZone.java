package edu.umich.elasticity.model;

import java.util.ArrayList;
import java.util.StringJoiner;

import org.opentripplanner.routing.edgetype.StreetEdge;
import org.opentripplanner.routing.graph.Vertex;
import org.opentripplanner.routing.vertextype.StreetVertex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.umich.elasticity.InvalidDelayFormatException;

public class DelayedIntersectionZone {
    private static final Logger LOG = LoggerFactory.getLogger(DelayedIntersectionZone.class);
	public static final double DELAYED_ZONE_RADIUS = 20;

	String name;
	public Coordinate coordinate;
	// the set of vertices which are in the buffer zone ordered in increasing distance from coordinate
	public ArrayList<StreetVertex> streetVertices;
	public ArrayList<StreetEdge> streetEdgesComingIn;
	public ArrayList<StreetEdge> streetEdgesWithin;
	
	public double delay;
	
	public DelayedIntersectionZone(String line) throws InvalidDelayFormatException {
		String[] splitLine = line.split(",");
		if (splitLine.length!=4)
			throw new InvalidDelayFormatException(line + "<br>There should be exactly 4 fields separated by commas<br>");
		double latitude,longitude;
		try {
			latitude = Double.valueOf(splitLine[1]);
		} catch (NumberFormatException e) {
			throw new InvalidDelayFormatException(line + "<br>Field 2 (" + splitLine[1] + ") is the latitude "
					+ "and should be a real number<br>");
		}
		try {
			longitude = Double.valueOf(splitLine[2]);
		} catch (NumberFormatException e) {
			throw new InvalidDelayFormatException(line + "<br>Field 3 (" + splitLine[2] + ") is the longitude "
					+ "and should be a real number<br>");
		}
		try {
			this.delay = Double.valueOf(splitLine[3]);
		} catch (NumberFormatException e) {
			throw new InvalidDelayFormatException(line + "<br>Field 4 (" + splitLine[3] + ") is the delay "
					+ "and should be a real number<br>");
		}
		this.name = splitLine[0];
		coordinate = new Coordinate(latitude,longitude);
		streetVertices = new ArrayList<StreetVertex>();
		streetEdgesComingIn = new ArrayList<StreetEdge>();
		streetEdgesWithin = new ArrayList<StreetEdge>();
	}
	
	
	public DelayedIntersectionZone(String name, Coordinate coordinate, double delay) {
		this.name = name;
		this.coordinate = coordinate;
		this.delay = delay;
		streetVertices = new ArrayList<StreetVertex>();
		streetEdgesComingIn = new ArrayList<StreetEdge>();
		streetEdgesWithin = new ArrayList<StreetEdge>();
	}
	
	public String getOsmLinks() {
		StringJoiner joiner = new StringJoiner(";");		
		for(StreetVertex sv: streetVertices)
			joiner.add(String.format(Results.osmNodeLinkTemplate,sv.getLabel().split(":")[2]));
		return joiner.toString();
	}

	static String lineHeader() {
		StringJoiner joiner = new StringJoiner(Results.delimiter);
		joiner.add("Name").add("Latitude").add("Longitude");
		joiner.add("Osm node").add("Osm Lat").add("Osm Long").add("Delay (seconds)");
		return joiner.toString() + Results.newLine;		
	}
	
	/* Returns the coordinate of the nearest OSM intersection */
	public String lineOutput() {
		StringJoiner joiner = new StringJoiner(Results.delimiter);
		joiner.add(name);
		joiner.add(String.valueOf(coordinate.latitude)).add(String.valueOf(coordinate.longitude));
		if (streetVertices!=null && !streetVertices.isEmpty()) {
			joiner.add(getOsmLinks());
			joiner.add(String.valueOf(streetVertices.get(0).getLat())).add(String.valueOf(streetVertices.get(0).getLon()));
		}
		joiner.add(String.valueOf(delay));
		return joiner.toString() + Results.newLine;
	}	

	/* Returns the coordinate of the nearest OSM intersection */
	public String lineWithTabs() {
		StringJoiner joiner = new StringJoiner("\t");
		joiner.add(name);
		joiner.add(String.valueOf(coordinate.latitude)).add(String.valueOf(coordinate.longitude));
		if (streetVertices!=null && !streetVertices.isEmpty()) {
			joiner.add(getOsmLinks());
			joiner.add(String.valueOf(streetVertices.get(0).getLat())).add(String.valueOf(streetVertices.get(0).getLon()));
		}
		joiner.add(String.valueOf(delay));
		return joiner.toString() + Results.newLine;
	}	

	public String toString() {
		return lineOutput();
	}	
	
	public void dump() {
		LOG.info("Vertices:");
		for(Vertex sv: streetVertices)
			LOG.info("\t" + sv.getLabel());
		LOG.info("Edges coming in:");
		for(StreetEdge se: streetEdgesComingIn)
			LOG.info("\t" + se.getName() + "from:" +  se.getFromVertex().getLabel() + " to:" + se.getToVertex().getLabel());
		LOG.info("Edges  in:");
		for(StreetEdge se: streetEdgesWithin)
			LOG.info("\t" + se.getName() + "from:" +  se.getFromVertex().getLabel() + " to:" + se.getToVertex().getLabel());
	}
}
