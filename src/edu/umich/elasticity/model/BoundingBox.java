package edu.umich.elasticity.model;

public class BoundingBox {
	public double latSouth;
	public double lonEast;
	public double latNorth;
	public double lonWest;
	
	public BoundingBox(double latSouth, double lonEast, double latNorth, double lonWest) {
		super();
		this.latSouth = latSouth;
		this.lonEast = lonEast;
		this.latNorth = latNorth;
		this.lonWest = lonWest;
	}
	
	void union(BoundingBox other) {
		this.latSouth = Math.min(other.latSouth,this.latSouth); 
		this.lonWest = Math.min(other.lonWest,this.lonWest); 
		this.latNorth = Math.max(other.latNorth,this.latNorth); 
		this.lonEast = Math.max(this.lonEast,this.lonEast); 
	}
}
