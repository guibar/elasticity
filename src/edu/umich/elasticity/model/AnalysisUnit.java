package edu.umich.elasticity.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Random;
import java.util.StringJoiner;

public class AnalysisUnit {
	public long fips;
	Coordinate centroid;
	int population;
	public int jobs;
	boolean isCT;
	
	public AnalysisUnit() {
		super();
	}
	
	public AnalysisUnit(long fips, Coordinate centroid, int population, int jobs, boolean isCT) {
		super();
		this.fips = fips;
		this.centroid = centroid;
		this.population = population;
		this.jobs = jobs;
		this.isCT = isCT;
	}

	public static AnalysisUnit getUnit(long fips,boolean isCT) throws ClassNotFoundException, SQLException {
		// load the sqlite-JDBC driver using the current class loader
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;

		try {
			connection = DriverManager.getConnection(Scenario.dbUrl);
			Statement statement = connection.createStatement();
			String queryStub = "select * from %s where fips = ";
			ResultSet rs = statement.executeQuery(String.format(queryStub, isCT?"census_tract":"block_group") + fips);
			AnalysisUnit unit = new AnalysisUnit();
			while (rs.next()) {
				// read the result set
				unit.fips = rs.getLong("fips");
				unit.centroid = new Coordinate(rs.getDouble("latCent"),rs.getDouble("lonCent"));
				unit.jobs = rs.getInt("jobs");
				unit.population = rs.getInt("population");
				unit.isCT = isCT;
			}
			return unit;
		} finally {
		if (connection != null)
			connection.close();
		}
	}

	public int getCountyFIPS() {
		// we don't use the county_fips as it is not there for block_groups
		return (int)(fips/(isCT?1000000:10000000));		
	}
	
	public static AnalysisUnit getRandomUnit(boolean isCT) throws ClassNotFoundException, SQLException {
		// load the sqlite-JDBC driver using the current class loader
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		long randomFips;
		try {
			connection = DriverManager.getConnection(Scenario.dbUrl);
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(String.format("SELECT fips FROM %s", isCT?"census_tract":"block_group"));
			ArrayList<Long> allCTsFips = new ArrayList<Long>();
			while (rs.next())
				allCTsFips.add(rs.getLong(1));
			Random r = new Random();
			randomFips = allCTsFips.get(Math.abs(r.nextInt())%allCTsFips.size());
		} finally {
		if (connection != null)
			connection.close();
		}
		return getUnit(randomFips,isCT);
	}

	static String lineHeaderDetails() {
		StringJoiner joiner = new StringJoiner(Results.delimiter);
		joiner.add("Fips").add("Unit").add("Acc_Bfr").add("Acc_Aft").add("Acc_Diff")
		.add("Pop_Bfr").add("Pop_Aft").add("Pop_Diff")
		.add("Jobs_Bfr").add("Jobs_Aft").add("Jobs_Diff")
		.add("OSM_Node").add("OSM_Lat").add("OSM_Long");

		return joiner.toString() + Results.newLine;		
	}
	static String lineOutputDetails(Scenario s, int i) {
		String formatWithLeading0 = s.isCT?"%011d":"%012d";
		StringJoiner joiner = new StringJoiner(Results.delimiter);
		joiner.add("\"" + String.format(formatWithLeading0, s.unitFips[i]) + "\"").add(s.isCT?"Census Tract":"Block Group")
		.add(String.valueOf(s.results.accessBefore[i])).add(String.valueOf(s.results.accessAfter[i]))
		.add(String.valueOf(s.results.accessAfter[i]-s.results.accessBefore[i]))
		.add(String.valueOf(s.unitPopulationBefore[i])).add(String.valueOf(s.unitPopulationAfter[i]))
		.add(String.valueOf(s.unitPopulationAfter[i]-s.unitPopulationBefore[i]))
		.add(String.valueOf(s.unitJobsBefore[i])).add(String.valueOf(s.unitJobsAfter[i]))
		.add(String.valueOf(s.unitJobsAfter[i]-s.unitJobsBefore[i]))
		.add(String.format("http://www.openstreetmap.org/node/%s",s.getOsmIdForUnit(i)))
		.add(String.valueOf(s.unitVertices[i].getLat())).add(String.valueOf(s.unitVertices[i].getLon()));
		return joiner.toString() + Results.newLine;
	}	

	static String lineHeader() {
		StringJoiner joiner = new StringJoiner(Results.delimiter);
		joiner.add("Fips").add("Accessibility before").add("Accessibility after");
		joiner.add("Population before").add("Population after");
		joiner.add("Jobs before").add("Jobs after");
		joiner.add("OSM node").add("OSM Lat").add("OSM Long");

		return joiner.toString() + Results.newLine;		
	}

	static String lineOutput(Scenario s, int i) {
		StringJoiner joiner = new StringJoiner(Results.delimiter);
		joiner.add(String.valueOf(s.unitFips[i])).add(String.valueOf(s.results.accessBefore[i]))
		.add(String.valueOf(s.results.accessAfter[i]));
		joiner.add(String.valueOf(s.unitPopulationBefore[i])).add(String.valueOf(s.unitPopulationAfter[i]));
		joiner.add(String.valueOf(s.unitJobsBefore[i])).add(String.valueOf(s.unitJobsAfter[i]));
		joiner.add(String.format("http://www.openstreetmap.org/node/%s",s.getOsmIdForUnit(i)))
		.add(String.valueOf(s.unitVertices[i].getLat())).add(String.valueOf(s.unitVertices[i].getLon()));
		return joiner.toString() + Results.newLine;
	}	
}