package edu.umich.elasticity.model;

public class NoVertexFoundException extends Exception {
	String message;
	public NoVertexFoundException(String message) {
		this.message = message;
	}
	
	public String toString() {
		return message;
	}

	private static final long serialVersionUID = -2428333978042968464L;

}
