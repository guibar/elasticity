package edu.umich.elasticity.setup;

import java.net.URL;

public class NoPopulationDataException extends Exception {
	URL source;
	/**
	 * 
	 */
	private static final long serialVersionUID = -8148532525472955125L;

	public NoPopulationDataException(URL source) {
		this.source = source;
	}

}
