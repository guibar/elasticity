package edu.umich.elasticity.setup;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.umich.elasticity.model.Scenario;

/**
 * A kind of file representing a Lodes data file. It has the year and the state as fields 
 * to ease operations.
 * @author Gui
 *
 */
class LodesFile extends File {
	private static final long serialVersionUID = 1L;
	static final Logger LOG = LoggerFactory.getLogger(PopulationData.class);

	String usps;
	int year;
	final static String lodesBaseURL = "http://lehd.ces.census.gov/data/lodes/LODES7/";
	static File lodesDirectory = new File(Scenario.root,"data/setup/lodes");
	
	/**
	 * This one is private because we have to make sure that the caller calls it with an appropriate filename.
	 * The work is done by the caller of extracting the information from the name. 
	 * @param fileName
	 * @param year
	 * @param usps
	 */
	private LodesFile(String fileName,int year,String usps) {
		super(lodesDirectory,fileName);
		this.year = year;
		this.usps = usps;
	}

	public LodesFile(int year,String usps) {
		super(lodesDirectory, usps + "_wac_S000_JT00_" + year + ".csv.gz");
		this.year = year;
		this.usps = usps;
	}

	/**
	 * Find all the Lodes files locally for that state.
	 * @param uspsCode the state usps code
	 * @return all the local Lodes files belonging to that state
	 */
	public static ArrayList<LodesFile> getLocalLodesFiles(final String uspsCode) {
		ArrayList<LodesFile> stateLodesFiles = new ArrayList<LodesFile>();
		String[] filenames = lodesDirectory.list();
		for(String name: filenames) {
			if (name.matches("^"+uspsCode+"_wac_S000_JT00_" + "(\\d{4})\\.csv.gz$")) {
				int year = Integer.valueOf(name.substring(17,21));
				stateLodesFiles.add(new LodesFile(name,year,uspsCode));
			}
		}
		Collections.sort(stateLodesFiles, new LodesFileYearComparator());
		return stateLodesFiles;
	}

	public static LodesFile getLatestLocalLodesFile(final String uspsCode) {
		ArrayList<LodesFile> stateLodesFiles = getLocalLodesFiles(uspsCode);
		return stateLodesFiles.isEmpty()?null:stateLodesFiles.get(0);
	}

	/**
	 * Given a usps state code, this function compares files looking like a LODES data for that state 
	 * in the lodesDirectory with the latest file for that state on the Lodes site. If a newer version 
	 * exists on the site, it is downloaded and returned. If the latest is found locally, no download
	 * takes place and the file is returned.
	 * 
	 * @param uspsCode the 2 letter code for a US state
	 * @return a file containing the Lodes data for that state
	 * @throws IOException 
	 * @throws NoLodesDataException if no file exists locally and none can be downloaded either
	 */
	public static LodesFile getLodesFile(final String uspsCode) throws IOException,NoLodesDataException {
		LodesFile latest = getLatestLocalLodesFile(uspsCode);
		int thisYear = Calendar.getInstance().get(Calendar.YEAR);
		int oldestYearToLookFor = (latest==null?2002:latest.year+1);
		
		// we will check on LODES site if a newer file is downloadable
		URL wacFileURL = null;
		for(int year=thisYear;year>=oldestYearToLookFor;year--) {
			String wacFileName = uspsCode + "_wac_S000_JT00_" + year + ".csv.gz";
			wacFileURL = new URL(LodesFile.lodesBaseURL + uspsCode + "/wac/" + wacFileName);
			HttpURLConnection connection = (HttpURLConnection) wacFileURL.openConnection();
			connection.setRequestMethod("HEAD");
			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				latest = new LodesFile(wacFileName,year,uspsCode);
				FileUtils.copyURLToFile(wacFileURL, latest);
				LOG.info("Returning downloaded file " + latest);
				return latest;
			} 
			else
				LOG.info("File: " + wacFileURL + " is not available online");
			
		}
		if (latest!=null)
			return latest;			
		// no wac file has been found
		else
			throw new NoLodesDataException();
	}

	public void getEmploymentData(HashMap<Long, Integer> bgData,HashMap<Long, Integer> ctData) 
			throws IOException,NoLodesDataException {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(this))));		
			// skip headers
			br.readLine();
			String dataLine;
			while ((dataLine = br.readLine()) != null) {
				//DataCollection.LOG.debug(dataLine);
				String[] fields = dataLine.split(",");
				// it seems that the codes always have a trailing zero and are 15 digits long. The last four digits are therefore
				// to be ignored since CT fips are 11 digits long with 2 digits for the state. 
				if (fields[0].length()!=15)
					throw new IOException("Assumption about the geocode is violated. Entry will be misunderstood");
				Long ctFips = Long.valueOf(fields[0].substring(0,15-4));
				Long bgFips = Long.valueOf(fields[0].substring(0,15-3));
				int popCT = ctData.containsKey(ctFips)?ctData.get(ctFips):0;
				int popBG = bgData.containsKey(bgFips)?bgData.get(bgFips):0;
				ctData.put(ctFips, popCT + Integer.valueOf(fields[1]));
				bgData.put(bgFips, popBG + Integer.valueOf(fields[1]));
			}
		} finally {
			if (br!=null)
				br.close();
		}
		//LOG.debug(jobsData.keySet().toString());
	}	
}

class LodesFileYearComparator implements Comparator<LodesFile> {
	@Override
	/**
	 * We want the most recent file first after sort, so we set them as the smallest
	 */
	public int compare(LodesFile o1, LodesFile o2) {
		return o2.year-o1.year;
	}		
}
