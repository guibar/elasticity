package edu.umich.elasticity.setup;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.umich.elasticity.model.State;

public class PopulationData {
	static final Logger LOG = LoggerFactory.getLogger(PopulationData.class);
	// queries all the cts for population in a given state, key is variable too
	static String ctPopulationUrlTemplate = "http://api.census.gov/data/2014/acs5?get=B01003_001E&for=tract:*&in=state:%s+county:*&key=%s";
	// cannot query all bg for a state, need to ask per county, hence 2 slots in this template + key
	static String bgPopulationUrlTemplate = "http://api.census.gov/data/2014/acs5?get=B01003_001E&for=block+group:*&in=state:%s+county:%s&key=%s";
	// Census API Key
    static String key = "2a1ecd0c5793eb70c6ed7115058e3d8744132ab3";
	

	public static void getPopulationData() throws ClassNotFoundException,SQLException, IOException {
		// erase all jobs data;
		DatabaseBuilder.clearColumnInDb("census_tract","population");		
		getTractPopulationData();
		DatabaseBuilder.clearColumnInDb("block_group","population");		
		getGroupPopulationData();
		
		SortedSet<Long> missingRows = new TreeSet<Long>();
		DatabaseBuilder.findNullRows("census_tract","population",missingRows);
		LOG.info("CTs with no population data are: " + missingRows.toString());
		missingRows.clear();
		DatabaseBuilder.findNullRows("block_group","population",missingRows);
		LOG.info("BGs with no population data are: " + missingRows.toString());
	}

    public static void getTractPopulationData() throws ClassNotFoundException, SQLException, IOException {
        HashMap<Long, Integer> popData = new HashMap<Long,Integer>();
        for(State state: State.getStatesFips()) {	        
	        // build a URL
	    	LOG.info("Dealing with state " + state.fips);
	        URL url = new URL(String.format(ctPopulationUrlTemplate,state.fips,key));
			readACSjsonData(url, popData);
	
			DatabaseBuilder.loadColumnDataInDb("census_tract","population", popData);
			popData.clear();
		}
    }
	
	public static void getGroupPopulationData() throws ClassNotFoundException, SQLException, IOException {
		HashMap<Long, Integer> popData = new HashMap<Long, Integer>();

		for (State state : State.getStatesFips()) {
			for (int countyFips : state.getCountiesFips()) {
				LOG.info("Dealing with state: " + state.fips + ", county: " + countyFips);
				URL url = new URL(String.format(bgPopulationUrlTemplate, state.fips, countyFips, key));				
				readACSjsonData(url, popData);
				DatabaseBuilder.loadColumnDataInDb("block_group","population", popData);
				popData.clear();
			}
		}
	}

	static void readACSjsonData(URL url, HashMap<Long, Integer> data) throws IOException {
    	LOG.info("Calling " + url);
		String httpContent;
		InputStream in = url.openStream();
		try {
			httpContent = IOUtils.toString(in);
		} finally {
			IOUtils.closeQuietly(in);
		}
		if (httpContent.toString().equals("")) {
			LOG.info("No data returned by :" + url);
			return;
		}

		JSONArray arrayOfArrays;
		arrayOfArrays = new JSONArray(httpContent);
		LOG.info("Reading array of size " + arrayOfArrays.length());
		StringBuilder fipsBuilder = new StringBuilder();
		// skip first array which has headers
		for (int i = 1; i < arrayOfArrays.length(); i++) {
			fipsBuilder.setLength(0);
			JSONArray dataArray = arrayOfArrays.getJSONArray(i);
			for(int j=1;j<dataArray.length();j++)
				fipsBuilder.append(dataArray.getString(j));
			
			Long fips = Long.valueOf(fipsBuilder.toString());
			int pop = dataArray.getInt(0);
			data.put(fips, pop);
			LOG.debug("Fips: " + fips + ", Population: " + dataArray.getInt(0));
		}
	}
}