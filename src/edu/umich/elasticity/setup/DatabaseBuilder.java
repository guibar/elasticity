package edu.umich.elasticity.setup;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.umich.elasticity.model.Scenario;
import edu.umich.elasticity.model.State;
import net.iryndin.jdbf.core.DbfRecord;

public class DatabaseBuilder {
	static final Logger LOG = LoggerFactory.getLogger(DatabaseBuilder.class);

	static File geoDataDir = new File("data/setup/geo");
	static File stateDataDir = new File(geoDataDir,"state");
	static File countyDataDir = new File(geoDataDir,"county");
  	static File ctDataDir = new File(geoDataDir,"ct");
	static File bgDataDir = new File(geoDataDir,"bg");

	static String states2015ShapeURL = "http://www2.census.gov/geo/tiger/TIGER2015/STATE/tl_2015_us_state.zip";
	static String counties2015ShapeURL = "http://www2.census.gov/geo/tiger/TIGER2015/COUNTY/tl_2015_us_county.zip";
	static String ct2015ShapeURLtemplate = "http://www2.census.gov/geo/tiger/TIGER2015/TRACT/tl_2015_%s_tract.zip";
	static String bg2015ShapeURLtemplate = "http://www2.census.gov/geo/tiger/TIGER2015/BG/tl_2015_%s_bg.zip";

	static String dbUrl = Scenario.dbUrl;
	//static String dbUrl = "jdbc:sqlite:" + Scenario.root + "/data/us_data2015.sqlite";
	

	// I did not automate the setting of the max and min Lat/Longs of every county
	// In QGIS, open attribute table, go to edit mode and use the field calculator to create the variables
	// create new field Real number with precision of 6. Leave width to 10.
	// minLon: xmin($geometry) (no equal sign in formula) and same for the others
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException, ParseException {
//		// States
//		File statesDBF = downloadZipExtractAndReturnDbf(states2015ShapeURL,stateDataDir);
//		loadStatesInDb(statesDBF);
//		// Counties
//		File countiesDBF = downloadZipExtractAndReturnDbf(counties2015ShapeURL,countyDataDir);
//		loadCountiesInDb(countiesDBF);
//		// Census Tracts
//		for(State s:State.getStatesFips())
//			downloadZipExtractAndReturnDbf(String.format(ct2015ShapeURLtemplate, String.format("%02d", s.fips)),ctDataDir);
//		loadCensusTractsInDb();
//		// Block Groups
//		for(State s:State.getStatesFips())
//			downloadZipExtractAndReturnDbf(String.format(bg2015ShapeURLtemplate, String.format("%02d", s.fips)),bgDataDir);
//		loadBlockGroupsInDb();
//		
//    	PopulationData.getPopulationData();
    	EmploymentData.getEmploymentData();
    	//DataCollection.fetchOSMFiles();

	}
	
	public static File downloadZipExtractAndReturnDbf(String urlString,File folder) throws IOException {
		URL url = new URL(urlString);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		File zipFile = new File(folder, urlString.split("/")[urlString.split("/").length-1]);
		File dbfFile = new File(folder,zipFile.getName().replace(".zip", ".dbf"));

		if (dbfFile.exists()) {
			LOG.info(String.format("Will use file %s found on local system.",dbfFile.getName()));
			return dbfFile;
		}
		
		if (!zipFile.exists()) {			
			connection.setRequestMethod("HEAD");
			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				FileUtils.copyURLToFile(url, zipFile);
				LOG.info("Downloading " + url);
			} else 
				throw new IOException("Unable to download from " + url);
		} else
			LOG.info("Using file on disk for " + url);

		// get the zip file content
		byte[] buffer = new byte[1024];
		ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
		
		ZipEntry ze = zis.getNextEntry();
		while (ze != null) {
			File f = new File(folder,ze.getName());
			FileOutputStream fos = new FileOutputStream(f);
			int len;
			while ((len = zis.read(buffer)) > 0)
				fos.write(buffer, 0, len);
			fos.close();
			// we are done with the zip file at this point				
			ze = zis.getNextEntry();
		}
		zis.closeEntry();
		zis.close();
		// The dbfFile should now be here, if not raise an exception
		if (!dbfFile.exists())
			throw new IOException(dbfFile.getName() + " is not found after zip extraction");
		return dbfFile;
	}

	public static void loadStatesInDb(File dbfFile) throws ClassNotFoundException,IOException,SQLException {
		// load the sqlite-JDBC driver using the current class loader
		Class.forName("org.sqlite.JDBC");	
		// create a database connection
		Connection connection = DriverManager.getConnection(DatabaseBuilder.dbUrl);
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30); // set timeout to 30 sec.
	
		// Create table
		statement.executeUpdate("drop table if exists state");
		statement.executeUpdate("CREATE TABLE state (fips INTEGER PRIMARY KEY, usps  TEXT, name TEXT)");
		
		// Load states
		DbfIterator dbfIterator = new DbfIterator(dbfFile);
		DbfRecord dbfRec;
		PreparedStatement insertSQL = connection.prepareStatement("INSERT INTO state (fips,name,usps) VALUES (?,?,?)");
		while ((dbfRec=dbfIterator.next())!=null) {
			// don't load Puerto Rico
			if (dbfRec.getBigDecimal("GEOID").intValueExact()>56)
				continue;
			insertSQL.setInt(1, dbfRec.getBigDecimal("GEOID").intValueExact());
			insertSQL.setString(2, dbfRec.getString("NAME"));
			insertSQL.setString(3, dbfRec.getString("STUSPS").toLowerCase());
			insertSQL .executeUpdate();
		}
		if (connection != null)
			connection.close();
	}
	
	public static void loadCountiesInDb(File dbfFile) throws ClassNotFoundException,IOException,SQLException {
		HashSet<Integer> stateFips = new HashSet<Integer>();
		for(State s: State.getStatesFips())
			stateFips.add(s.fips);
		
		// load the sqlite-JDBC driver using the current class loader
		Class.forName("org.sqlite.JDBC");	
		// create a database connection
		Connection connection = DriverManager.getConnection(DatabaseBuilder.dbUrl);
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30); // set timeout to 30 sec.
	
		// Create table
		statement.executeUpdate("drop table if exists county");
		statement.executeUpdate("CREATE TABLE county (fips INTEGER PRIMARY KEY, state_fips INTEGER, name TEXT, "
				+ "minLat REAL, maxLat REAL, minLon REAL, maxLon REAL, "
				+ "FOREIGN KEY(state_fips) REFERENCES state(fips) )");		
		// Load counties
		DbfIterator dbfIterator = new DbfIterator(dbfFile);
		DbfRecord dbfRec;
		PreparedStatement insertSQL = connection.prepareStatement("INSERT INTO county (fips,state_fips,name,minLat,maxLat,minLon,maxLon) "
				+ "VALUES (?,?,?,?,?,?,?)");
		while ((dbfRec=dbfIterator.next())!=null) {
			// only load the counties corresponding to the states already in
			if (stateFips.contains(dbfRec.getBigDecimal("STATEFP").intValueExact())) {
				insertSQL.setInt(1, dbfRec.getBigDecimal("GEOID").intValueExact());
				insertSQL.setInt(2, dbfRec.getBigDecimal("STATEFP").intValueExact());
				insertSQL.setString(3, dbfRec.getString("NAME"));
				insertSQL.setDouble(4, dbfRec.getBigDecimal("minLat").doubleValue());
				insertSQL.setDouble(5, dbfRec.getBigDecimal("maxLat").doubleValue());
				insertSQL.setDouble(6, dbfRec.getBigDecimal("minLon").doubleValue());
				insertSQL.setDouble(7, dbfRec.getBigDecimal("maxLon").doubleValue());
				insertSQL .executeUpdate();
				LOG.info("Processing " + dbfRec.getBigDecimal("GEOID"));
			}
		}			
		if (connection != null)
			connection.close();
	}	
	

	public static void loadCensusTractsInDb() throws ClassNotFoundException,IOException,SQLException {
		// load the sqlite-JDBC driver using the current class loader
		Class.forName("org.sqlite.JDBC");	
		// create a database connection
		Connection connection = DriverManager.getConnection(DatabaseBuilder.dbUrl);
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30); // set timeout to 30 sec.

		// Create table
		statement.executeUpdate("drop table if exists census_tract");
		statement.executeUpdate("CREATE TABLE census_tract (fips INTEGER PRIMARY KEY, county_fips INTEGER, "
				+ "latCent REAL, lonCent REAL, population INTEGER, jobs INTEGER, "
				+ "FOREIGN KEY(county_fips) REFERENCES county(fips) )");

		File[] ctDbfFiles = ctDataDir.listFiles(new FilenameFilter() {			
			public boolean accept(File dir, String name) {
				return name.endsWith(".dbf");
			}
		});
		
		for(File dbfFile: ctDbfFiles) {

			// Load CTs
			DbfIterator dbfIterator = new DbfIterator(dbfFile);
			DbfRecord dbfRec;
			PreparedStatement insertSQL = connection.prepareStatement(
					"INSERT INTO census_tract (fips,county_fips,latCent,lonCent) "
					+ "VALUES (?,?,?,?)");
			while ((dbfRec=dbfIterator.next())!=null) {
				// GEOID too big to be handled like the ones above
				insertSQL.setLong(1, dbfRec.getBigDecimal("GEOID").longValue());
				// use the ssccc format for county fips
				insertSQL.setInt(2,Integer.valueOf(dbfRec.getString("STATEFP") + dbfRec.getString("COUNTYFP")));
				insertSQL.setDouble(3, dbfRec.getBigDecimal("INTPTLAT").doubleValue());
				insertSQL.setDouble(4, dbfRec.getBigDecimal("INTPTLON").doubleValue());
				insertSQL .executeUpdate();
				LOG.info("Processing " + dbfRec.getBigDecimal("GEOID"));
			}			
		}
	}	

	public static void loadBlockGroupsInDb() throws ClassNotFoundException,IOException,SQLException {
		// load the sqlite-JDBC driver using the current class loader
		Class.forName("org.sqlite.JDBC");	
		// create a database connection
		Connection connection = DriverManager.getConnection(DatabaseBuilder.dbUrl);
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30); // set timeout to 30 sec.

		// Create table
		statement.executeUpdate("drop table if exists block_group");
		statement.executeUpdate("CREATE TABLE block_group (fips INTEGER PRIMARY KEY, ct_fips INTEGER, "
				+ "latCent REAL, lonCent REAL, population INTEGER, jobs INTEGER, "
				+ "FOREIGN KEY(ct_fips) REFERENCES census_tract(fips) )");

		File[] bgDbfFiles = bgDataDir.listFiles(new FilenameFilter() {			
			public boolean accept(File dir, String name) {
				return name.endsWith(".dbf");
			}
		});
		
		for(File dbfFile: bgDbfFiles) {
			// Load CTs
			DbfIterator dbfIterator = new DbfIterator(dbfFile);
			DbfRecord dbfRec;
			PreparedStatement insertSQL = connection.prepareStatement(
					"INSERT INTO block_group (fips,ct_fips,latCent,lonCent) "
					+ "VALUES (?,?,?,?)");
			while ((dbfRec=dbfIterator.next())!=null) {
				// GEOID too big to be handled like the ones above
				insertSQL.setLong(1, dbfRec.getBigDecimal("GEOID").longValue());
				// use the ssccc format for county fips
				insertSQL.setLong(2,Long.valueOf(dbfRec.getString("STATEFP") + 
						dbfRec.getString("COUNTYFP") + dbfRec.getString("TRACTCE")));
				insertSQL.setDouble(3, dbfRec.getBigDecimal("INTPTLAT").doubleValue());
				insertSQL.setDouble(4, dbfRec.getBigDecimal("INTPTLON").doubleValue());
				insertSQL .executeUpdate();
				LOG.info("Processing " + dbfRec.getBigDecimal("GEOID"));
			}			
		}
	}

	public static void clearColumnInDb(String tableName, String columnName) 
			throws ClassNotFoundException,SQLException {	
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(dbUrl);
			connection.createStatement().executeUpdate("UPDATE " + tableName + " set " + columnName + " = null");
		} finally {	
			if (connection != null)
				connection.close();
		}
	}

	public static void loadColumnDataInDb(String tableName, String columnName, HashMap<Long, Integer> data) 
			throws ClassNotFoundException,SQLException {
		PopulationData.LOG.info("loading column " + columnName + " in table " + tableName);
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		// create a database connection
		// could also be DriverManager.getConnection("jdbc:sqlite:C:/work/mydatabase.db");
		try {
			connection = DriverManager.getConnection(dbUrl);	
			PreparedStatement updateSQL = connection.prepareStatement("UPDATE " + tableName + " set " + columnName + " = ? WHERE fips = ?");
			for(long ct: data.keySet()) {
				updateSQL.setInt(1, data.get(ct));
				updateSQL.setLong(2, ct);
				//LOG.debug("fips: " + ct + "," + columnName + ": "+ data.get(ct));
				if (updateSQL.executeUpdate()!=1)
					PopulationData.LOG.info("fips " + ct + " not found in DB");
			}			
		} finally {	
			if (connection != null)
				connection.close();
		}	
	}

	public static void findNullRows(String tableName, String columnName, Set<Long> missingTracts) 
			throws ClassNotFoundException, SQLException {
		Class.forName("org.sqlite.JDBC");
		Connection connection = null;
		// create a database connection
		// could also be DriverManager.getConnection("jdbc:sqlite:C:/work/mydatabase.db");
		try {
			connection = DriverManager.getConnection(dbUrl);	
			// find missing data ...
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.
			ResultSet rs = statement.executeQuery(
					"select fips from " + tableName + " where " + columnName + " is null ");
			while (rs.next())
				missingTracts.add(rs.getLong(1));
			
		} finally {	
			if (connection != null)
				connection.close();
		}			
	}

}
