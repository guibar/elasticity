package edu.umich.elasticity.setup;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.FeatureSource;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.Filter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;

public class Utils {
    private static final Logger LOG = LoggerFactory.getLogger(Utils.class);
    
	static File geoDataDir = new File("data/setup/geo");
	static File bgDataDir = new File(geoDataDir,"bg");
	
	public static void readShapeFile() throws Exception {
		File file = new File(bgDataDir,"tl_2015_02_bg.shp");
	    Map<String, Object> map = new HashMap<String, Object>();
	    map.put("url", file.toURI().toURL());

	    DataStore dataStore = DataStoreFinder.getDataStore(map);
	    String typeName = dataStore.getTypeNames()[0];

	    FeatureSource<SimpleFeatureType, SimpleFeature> source = dataStore.getFeatureSource(typeName);
	    Filter filter = Filter.INCLUDE; // ECQL.toFilter("BBOX(THE_GEOM, 10,20,30,40)")

	    FeatureCollection<SimpleFeatureType, SimpleFeature> collection = source.getFeatures(filter);
	    try (FeatureIterator<SimpleFeature> features = collection.features()) {
	        while (features.hasNext()) {
	            SimpleFeature feature = features.next();
	            MultiPolygon geom = (MultiPolygon) feature.getDefaultGeometry();
                Point centroid = geom.getCentroid();
                
	            System.out.print(feature.getID());
	            System.out.print(": ");
	            System.out.println(centroid);
	        }
	    }
	}

	public static HashMap<Long,Integer> readDataFromCsv(File f) throws IOException {
		HashMap<Long, Integer> data = new HashMap<Long, Integer>();			
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(f));		
			// skip headers
			br.readLine();
			String dataLine;
			while ((dataLine = br.readLine()) != null) {
				LOG.info(dataLine);
				String[] fields = dataLine.split(",");
				data.put(Long.valueOf(fields[0]), Integer.valueOf(fields[1]));
			}
		} finally {
			if (br!=null)
				br.close();
		}
		return data;
	}
}