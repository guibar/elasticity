package edu.umich.elasticity.setup;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.umich.elasticity.OSMService;
import edu.umich.elasticity.model.State;

public class OsmDataFetcher {
	private static final String osmBaseURL = "http://download.geofabrik.de/north-america/us/";
	static final Logger LOG = LoggerFactory.getLogger(OsmDataFetcher.class);

	
	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException, ParseException {
		fetchOSMFiles();
	}
	
	public static void fetchOSMFiles() throws IOException, ClassNotFoundException, SQLException {		
		for(State state: State.getStatesFips()) {
			File osmFile = new File(OSMService.osmDataDir,state.usps+".osm.pbf");
			if (!osmFile.exists()) {
				URL osmStateURL = new URL(osmBaseURL + state.name.toLowerCase().replace(" ", "-") + "-latest.osm.pbf" );
				LOG.info("Starting download from " + osmStateURL);
				HttpURLConnection connection = (HttpURLConnection) osmStateURL.openConnection();
				connection.setRequestMethod("HEAD");
				if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
					FileUtils.copyURLToFile(osmStateURL,osmFile);
					LOG.info("downloading file to: " + osmFile.getAbsolutePath());
					//File osmFilteredFile = new File(OSMService.osmDataDir,state.usps+".f.osm.pbf");
					// filter without a bbox
					//OSMService.extractAndFilterWithOsmosis(osmFile, osmFilteredFile, null,true);
				} else
					LOG.info("File: " + osmStateURL + " is not available online");
			} else
				LOG.info("File: " + osmFile + " is alreay here");				
		}
	}

}
