package edu.umich.elasticity.setup;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import net.iryndin.jdbf.core.DbfRecord;
import net.iryndin.jdbf.reader.DbfReader;

class DbfIterator {
	Charset stringCharset = Charset.forName("Cp866");
	InputStream dbf;
	DbfRecord rec;
	DbfReader reader;

	public DbfIterator(String filename) throws IOException {
		dbf = new FileInputStream(filename);
		reader = new DbfReader(dbf);
	}

	public DbfIterator(File f) throws IOException {
		dbf = new FileInputStream(f);
		reader = new DbfReader(dbf);
	}

	DbfRecord next() throws IOException {
		rec = reader.read();
		if (rec!= null)
			rec.setStringCharset(stringCharset);
		return rec;
	}	
}