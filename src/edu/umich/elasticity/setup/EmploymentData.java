package edu.umich.elasticity.setup;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.umich.elasticity.model.Scenario;
import edu.umich.elasticity.model.State;

public class EmploymentData {
	static final Logger LOG = LoggerFactory.getLogger(EmploymentData.class);
	static File maJobsFile = new File(Scenario.root,"data/setup/ma.csv");

	public static void getEmploymentData() throws ClassNotFoundException,SQLException, IOException {
		// erase all jobs data;
		DatabaseBuilder.clearColumnInDb("census_tract","jobs");
		
		for(State state: State.getStatesFips()) {
			try {
				LOG.info("getting data for " + state.name);
				HashMap<Long, Integer> bgData = new HashMap<Long,Integer>();
				HashMap<Long, Integer> ctData = new HashMap<Long,Integer>();
				LodesFile.getLodesFile(state.usps).getEmploymentData(bgData,ctData);
				DatabaseBuilder.loadColumnDataInDb("census_tract","jobs",ctData);
				DatabaseBuilder.loadColumnDataInDb("block_group","jobs",bgData);
			} catch (NoLodesDataException e) {
				LOG.info("State "+ state.name + " does not have any lodes data");
			}
			LOG.info("DONE WITH " + state.name + "\n");
		}
	}

	static HashMap<Long, Integer> summarizePerCT(HashMap<Long, Integer> bgData)   {
		HashMap<Long, Integer> ctData = new HashMap<Long,Integer>();
		Iterator<Long> it = bgData.keySet().iterator();
		while (it.hasNext()) {
			long fips = it.next();
			int jobsCT = ctData.containsKey(fips/10)?ctData.get(fips/10):0;
			ctData.put(fips/10, jobsCT + Integer.valueOf(bgData.get(fips)));
		}
		return ctData;
	}
}
