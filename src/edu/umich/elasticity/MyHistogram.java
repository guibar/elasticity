package edu.umich.elasticity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.CategoryChart;
import org.knowm.xchart.CategoryChartBuilder;
import org.knowm.xchart.Histogram;
import org.knowm.xchart.style.Styler.LegendPosition;

public class MyHistogram {
	static int width = 600;
	static int height = 400;
	static int numberOfBins = 20;
	CategoryChart chart;

	public static void main(String[] args) throws IOException {
		MyHistogram mh = new MyHistogram(getTestData(30), getTestData(30));
		mh.saveToFile(new File("chart.jpg"));
	}

	public MyHistogram(float[][] timeBefore, float[][] timeAfter) {
		ArrayList<Float> timeBeforeSeries = new ArrayList<Float>();
		ArrayList<Float> timeAfterSeries = new ArrayList<Float>();

		for (int i = 0; i < timeBefore.length; i++)
			for (int j = 0; j < timeBefore.length; j++) {
				timeBeforeSeries.add(timeBefore[i][j]);
				timeAfterSeries.add(timeAfter[i][j]);
			}
		// Create Chart
		chart = new CategoryChartBuilder().width(800).height(600).title("Travel Time Histogram")
				.xAxisTitle("Travel Time (minute)").yAxisTitle("Frequency").build();

		// Customize Chart
		chart.getStyler().setLegendPosition(LegendPosition.InsideNW);
		chart.getStyler().setAvailableSpaceFill(.6);
		chart.getStyler().setXAxisLabelRotation(45);
		chart.getStyler().setXAxisDecimalPattern("#");
		// Series
		Histogram histogram1 = new Histogram(timeBeforeSeries, 20);
		Histogram histogram2 = new Histogram(timeAfterSeries, 20);
		chart.addSeries("No Build", histogram1.getxAxisData(), histogram1.getyAxisData());
		chart.addSeries("Build", histogram2.getxAxisData(), histogram2.getyAxisData());

	}

	static float[][] getTestData(int count) {
		float[][] data = new float[count][count];
		Random r = new Random();
		for (int i = 0; i < count; i++)
			for (int j = 0; j < count; j++)
				data[i][j] = (float) r.nextGaussian() * 10;
		return data;
	}

	public void saveToFile(File jpgFile) throws IOException {
		BitmapEncoder.saveJPGWithQuality(chart, jpgFile.getAbsolutePath(), 0.95f);
	}
}
