package edu.umich.elasticity;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintStream;

import edu.umich.elasticity.model.BoundingBox;
import edu.umich.elasticity.model.Scenario;

public class OSMService {
	public static File osmDataDir = new File(Scenario.root,"data/osm");
	static String bboxPbfExtractFilename = "bbox.osm.pbf";
	static String bboxXmlExtractFilename = "bbox.osm.xml";
	static String bboxXmlFilteredFilename = "bbox.filtered.osm.xml";
	Scenario scenario;
	
	public static File[] getPbfFiles(Scenario scenario) {
		return scenario.directory.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".pbf");
			}
		});
	}

	public static File getStateFile(String usps,boolean filtered) {
		if (filtered)
			return new File(OSMService.osmDataDir,usps+".f.osm.pbf");
		else
			return new File(OSMService.osmDataDir,usps+".osm.pbf");
	}
	
	public static void generateOSMFile(Scenario scenario) throws IOException {
		if (getPbfFiles(scenario).length>0)
			return;
		for(String usps: scenario.statesUSPS) {
			File generatedOsmFile = new File(scenario.directory,"bbox." + usps + ".osm.pbf");
			// this applies bbox *WITHOUT* filter to an unfiltered state file
			// filtering was resulting in paths not found
			extractAndFilterWithOsmosis(getStateFile(usps,false), generatedOsmFile,scenario.boundingBox,true,scenario.ps);
			// this applies the bbox only to an already filtered state file
//			extractAndFilterWithOsmosis(getStateFile(usps,true), generatedOsmFile,scenario.boundingBox,false);
		}
		if (scenario.statesUSPS.size()>1)
			joinExtractedFiles(scenario);
	}

	public static void joinExtractedFiles(Scenario scenario) throws IOException {
		// get the list of pbf files prior to the joining
		File[] stateExtractFiles = getPbfFiles(scenario);
		File generatedOsmFile = new File(scenario.directory,bboxPbfExtractFilename);
		String cmd = Scenario.osmosisExe.getAbsolutePath();
		for(File f:stateExtractFiles)
			cmd += " --read-pbf " + f.getAbsolutePath();
		cmd += " --merge --write-pbf " + generatedOsmFile.getAbsolutePath();

		scenario.ps.println("Joining files with command: " + cmd);
		ProcessBuilder pBuilder = new ProcessBuilder(cmd.split(" "));
		pBuilder.inheritIO();
		
		Process process = pBuilder.start();
		try {
		    int exitValue = process.waitFor();
		    if (exitValue!=0)
		    	throw new IOException("Osmosis did not complete successfully");
		    else
		    	// delete the pbf files that were there prior to the join
				for(File f:stateExtractFiles)
					f.delete();
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}				
	}
	
	/**
	 * We can just filter with bbox set to null or just clip with filter set to false
	 */
	public static void extractAndFilterWithOsmosis(File in,File out,BoundingBox bbox,boolean filter,PrintStream ps) throws IOException {
		if (!in.getName().endsWith(".pbf") || !out.getName().endsWith(".pbf"))
			throw new IOException("One of the files has the wrong extension");

		String bboxOption = " --bounding-box top=%s left=%s bottom=%s right=%s";
		String filterOption = " --tf accept-ways "
				+ "highway=motorway,trunk,primary,secondary,tertiary,residential,unclassified,"
				+ "motorway_link,trunk_link,primary_link,secondary_link,tertiary_link";
		String cmd = Scenario.osmosisExe.getAbsolutePath() + " --read-pbf " + in.getAbsolutePath() 
				+ (bbox==null?"":String.format(bboxOption, bbox.latNorth,bbox.lonWest,bbox.latSouth,bbox.lonEast))
				+ (filter?filterOption:"")
				+ " --write-pbf" + " " + out.getAbsolutePath();

		ps.println("Running osmosis with: " + cmd);
		ProcessBuilder pBuilder = new ProcessBuilder(cmd.split(" "));
		pBuilder.inheritIO();
		Process process = pBuilder.start();
		try {
		    int exitValue = process.waitFor();
		    if (exitValue!=0)
		    	throw new IOException("Osmosis did not complete successfully");
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}		
	}

	public static void extractAndOsmFilter(File pbfSourceFile,File xmlExtract,File out,BoundingBox bbox,PrintStream ps) throws IOException {
		if (!pbfSourceFile.getName().endsWith(".pbf") || !pbfSourceFile.getName().endsWith(".pbf"))
			throw new IOException(pbfSourceFile.getName() + " should be a *.pbf file");
		ProcessBuilder pBuilder;
		Process process;
		
		String bboxOption = " --bounding-box top=%s left=%s bottom=%s right=%s";
		String cmd = Scenario.osmosisExe.getAbsolutePath() + " --read-pbf " + pbfSourceFile.getAbsolutePath() 
				+ (bbox==null?"":String.format(bboxOption, bbox.latNorth,bbox.lonWest,bbox.latSouth,bbox.lonEast))
				+ " --write-xml" + " " + xmlExtract.getAbsolutePath();

		ps.println("Running osmosis with: " + cmd);
		pBuilder = new ProcessBuilder(cmd.split(" "));
		pBuilder.inheritIO();
		
		process = pBuilder.start();
		try {
		    int exitValue = process.waitFor();
		    if (exitValue!=0)
		    	throw new IOException("Osmosis did not complete successfully");
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}

		String[] cmdArgs = new String[4]; 
		cmdArgs[0] = Scenario.osmFilterExe.getAbsolutePath();
		cmdArgs[1] = xmlExtract.getAbsolutePath(); 
		cmdArgs[2] = "--keep=\"highway=motorway =trunk =primary =secondary =tertiary =residential =unclassified "
		+ "=motorway_link =trunk_link =primary_link =secondary_link =tertiary_link\"";
		cmdArgs[3] = "-o=" + out.getAbsolutePath();
		
		// with this joint/split, the keep option gets broken down at the spaces and the array has 15 elements.
		// if I feed the cmdArgs directly, it doesn't work.
		ps.println("l " + String.join(" ", cmdArgs).split(" ").length);
		pBuilder = new ProcessBuilder(String.join(" ", cmdArgs).split(" "));
		ps.println("Running osmfilter with: " + pBuilder.command());
		pBuilder.inheritIO();

		process = pBuilder.start();
		try {
		    int exitValue = process.waitFor();
		    if (exitValue!=0)
		    	throw new IOException("Osmfilter did not complete successfully");
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
	}
}
