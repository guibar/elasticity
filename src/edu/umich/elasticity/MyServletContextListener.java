package edu.umich.elasticity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MyServletContextListener implements ServletContextListener {
	static final String EXECUTOR_NAME = "BLAH";
	static int nr_executors = 2;
	private ExecutorService executorService;

	public void contextInitialized(ServletContextEvent event) {
		executorService = Executors.newFixedThreadPool(nr_executors, new DaemonThreadFactory());
		event.getServletContext().setAttribute(EXECUTOR_NAME, executorService);
	    MailService.host = event.getServletContext().getInitParameter("smtp-host");
	    MailService.port = event.getServletContext().getInitParameter("smtp-port");
	    MailService.fromEmail = event.getServletContext().getInitParameter("smtp-from-email");
	    MailService.username = event.getServletContext().getInitParameter("smtp-username");
	    MailService.password = event.getServletContext().getInitParameter("smtp-password");
	    MailService.adminEmail = event.getServletContext().getInitParameter("admin-email");
		MailService.send(new String[]{MailService.adminEmail},"Elasticity webapp started",
				"You are registered as the administrator's email in the web.xml config file.",null);
	}

	public void contextDestroyed(ServletContextEvent arg0) {
		executorService.shutdownNow(); // or process/wait until all pending jobs are done
	}
	
	class DaemonThreadFactory implements ThreadFactory {

	    private final ThreadFactory factory;

	    /**
	     * Construct a ThreadFactory with setDeamon(true) using
	     * Executors.defaultThreadFactory()
	     */
	    public DaemonThreadFactory() {
	        this(Executors.defaultThreadFactory());
	    }

	    public DaemonThreadFactory(ThreadFactory factory) {
	        if (factory == null)
	            throw new NullPointerException("factory cannot be null");
	        this.factory = factory;
	    }

	    public Thread newThread(Runnable r) {
	        final Thread t = factory.newThread(r);
	        t.setDaemon(true);
	        return t;
	    }
	}
}

