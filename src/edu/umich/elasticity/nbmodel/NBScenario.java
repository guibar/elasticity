package edu.umich.elasticity.nbmodel;

import edu.umich.elasticity.MailService;

import java.util.Date;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class NBScenario  {
    static final String adminEmail = "g.barreau@gmail.com";
    
	public static File root = System.getProperty("os.name").startsWith("Win")?
			new File("C:/Users/Gui/Documents/workspace/elasticity"):new File("/opt/elasticity");
	public static File workDir = new File(root,"work");
	
	public static boolean debug = System.getProperty("os.name").startsWith("Win");
	
	// the impedance values given are actually in inverse minutes. We are using seconds so must divide by 60.
	//private static final double[] impedances = {0.050/60, 0.075/60,0.100/60,0.125/60,0.150/60,0.280/60};

	// meta data
	public PrintStream ps;
	Date timeStarted,timeFinished;
	public String userEmail;
	public File directory;
	public NBResults results;
	
	TabularData unitsBeforeData,unitsAfterData;
	int nbUnits;

	double impedance;
	
	// employment and jobs will become [][] with first index being the column index
	// and second column the unit index
	int[][] unitPopulationBefore, unitPopulationAfter,unitJobsBefore, unitJobsAfter;

	// travelTimeBefore[i][j] is the time it take to go from ct[i] to ct[j]
	// before the development and after
	float[][] travelTimeBefore;
	float[][] travelTimeAfter;
	
	public NBScenario(String userEmail,InputStream[] unitsIS,InputStream[] timesIS,
			int[][] populationColumns,int[][] employmentColumns, 
			double impedance) 
					throws IOException,InvalidFileStructureException {
		this.userEmail = userEmail;
		this.impedance = impedance;
		timeStarted = new Date();

		DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss");
		directory = new File(workDir,userEmail.replace("@", "_at_") + "_" + dateFormat.format(new Date()));
		directory.mkdir();
		ps = new PrintStream(new File(directory,"run.log"));
		
	    unitsBeforeData = new TabularData(new BufferedReader(new InputStreamReader(unitsIS[0])));
	    unitsAfterData = new TabularData(new BufferedReader(new InputStreamReader(unitsIS[1])));
	    
	    // the first length of populationColumns is the number of population types
	    unitPopulationBefore = new int[populationColumns.length][];
	    unitPopulationAfter = new int[populationColumns.length][];
	    for(int i=0;i<populationColumns.length;i++) {
	    	unitPopulationBefore[i] = unitsBeforeData.getColumnAsInts(populationColumns[i][0]);
	    	unitPopulationAfter[i] = unitsAfterData.getColumnAsInts(populationColumns[i][1]);
	    }

	    unitJobsBefore = new int[employmentColumns.length][];
	    unitJobsAfter = new int[employmentColumns.length][];
	    for(int i=0;i<employmentColumns.length;i++) {
	    	unitJobsBefore[i] = unitsBeforeData.getColumnAsInts(employmentColumns[i][0]);
	    	unitJobsAfter[i] = unitsAfterData.getColumnAsInts(employmentColumns[i][1]);
	    }
	    
	    nbUnits = unitsBeforeData.nbOfDataLines;
	    
	    TabularData timeBeforeData = new TabularData(new BufferedReader(new InputStreamReader(timesIS[0])));
		travelTimeBefore = timeBeforeData.getMatrix(unitsBeforeData.getColumnAsInts(1), 0, 1, 2);
	    
	    TabularData timeAfterData = new TabularData(new BufferedReader(new InputStreamReader(timesIS[1])));
	    travelTimeAfter = timeAfterData.getMatrix(unitsBeforeData.getColumnAsInts(1), 0, 1, 2);

	    // We only have one column for each type of data. To generalize this,
	    // would have to create NBResults objects for each combination of column
	    results = new NBResults(this,0,0,0,0);
		ps.println(this);
		
		timeFinished = new Date();
		ps.println("Scenario with " + nbUnits + " CTs took " + (timeFinished.getTime()-timeStarted.getTime())/1000);
		
		// mail user with results
		if (!debug  && userEmail!=null) 
			MailService.send(new String[] {userEmail},"Results for Job " + directory.getName(),"Result files are zipped in the attachment.",results.getZipFile());
	}

	public NBScenario(String userEmail,File[] unitsFileIS,File[] buildFileIS,
			double impedance) 
					throws IOException,InvalidFileStructureException {
		this(userEmail,new FileInputStream[] {new FileInputStream(unitsFileIS[0]),new FileInputStream(unitsFileIS[1])},
				new FileInputStream[] {new FileInputStream(buildFileIS[0]),new FileInputStream(buildFileIS[1])},
			impedance);
	
	}
	
	public NBScenario(String userEmail,InputStream[] unitsFileIS,InputStream[] buildFileIS,
			double impedance) 
					throws IOException,InvalidFileStructureException {
		this(userEmail,unitsFileIS,buildFileIS,new int[][]{{2,2}},new int[][]{{3,3}},impedance);
	}

	public String getName() {
		return directory.getName().replace(".", "-");
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("user email:"+userEmail+"\n");
		sb.append("nb units:"+nbUnits+"\n");
		sb.append("impedance(inverse minutes):"+ impedance+"\n");
		return sb.toString();
	}	
}
