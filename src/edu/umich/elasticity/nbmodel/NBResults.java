package edu.umich.elasticity.nbmodel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.StringJoiner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.umich.elasticity.MyHistogram;


public class NBResults {
	static final Logger LOG = LoggerFactory.getLogger(NBResults.class);
	//public static String[] fileNames = new String[]{"main.csv","details.csv"};
	public static String[] fileNames = new String[]{"main.csv","travel-times-histogram.jpg"};

	NBScenario scenario;
	int[] populationDataBefore,employmentDataBefore,populationDataAfter,employmentDataAfter;
	public File[] outputFiles;

	// stats
	public double[] accessBefore;
	public double[] accessAfter;
	
	double sumAiPiBefore, sumAiPiAfter;
	double perCapitaAccessBefore, perCapitaAccessAfter;
	
	public NBResults(NBScenario scenario,int popColumnBefore,int empColumnBefore,
			int popColumnAfter, int empColumnAfter) throws IOException {
		this.scenario = scenario;
		populationDataBefore = scenario.unitPopulationBefore[popColumnBefore];
		employmentDataBefore = scenario.unitJobsBefore[empColumnBefore];
		populationDataAfter = scenario.unitPopulationAfter[popColumnAfter];
		employmentDataAfter = scenario.unitJobsAfter[empColumnAfter];
		
		accessBefore = new double[scenario.nbUnits];
		accessAfter = new double[scenario.nbUnits];

		outputFiles = new File[fileNames.length+1];
		for(int i=0;i<fileNames.length;i++)
			outputFiles[i] = new File(scenario.directory,fileNames[i]);
		outputFiles[outputFiles.length-1] = new File(scenario.directory,scenario.getName()+".zip");
		
		prepareOutput();
	}

	void prepareOutput() throws IOException {
	
		// check that values after are sensible compared to values before
		
		// calculate stats before and after
		double[] statsBefore = 
				calculateSumsAndAccessibility(scenario.travelTimeBefore,
						employmentDataBefore,populationDataBefore,accessBefore,scenario.impedance);
		sumAiPiBefore = statsBefore[0];
		perCapitaAccessBefore = statsBefore[1];

		double[] statsAfter = 
				calculateSumsAndAccessibility(scenario.travelTimeAfter,
						employmentDataAfter,populationDataAfter,accessAfter,scenario.impedance);
		sumAiPiAfter = statsAfter[0];
		perCapitaAccessAfter = statsAfter[1];

		saveResultsToFiles();
	}
	
	
	/**
	 * Fills up the access array with accessibility values
	 * @return an array with sumAiPi and per capita accessibility which is sumAiPi/sumPi
	 */
	static double[] calculateSumsAndAccessibility(float[][] travelTimes, int[] jobs, int[] population, 
			double[] access, double impedance) {
		// Ai = sumj(exp(-impedance * travel timeij) * employmentj)
		for (int i = 0; i < jobs.length; i++) {
			access[i] = 0.0;
			for (int j = 0; j < jobs.length; j++)
				access[i] += Math.exp(-impedance * travelTimes[i][j]) * jobs[j];
		}
		
		// stats[0] is sumAiPi, stats[1] is sumPi, stats[3] is sumAi and stats[4] is sumJi
		double[] stats = new double[2];
		double sumPop = 0d;
		for (int i = 0; i < population.length; i++) {
			stats[0] += access[i] * population[i];
			sumPop += population[i];
		}
		// Per capita accessibility = sumi(Ai * pi)/sumi(pi)
		stats[1] = stats[0] / sumPop;
		return stats;
	}


	public File getZipFile() {
		return outputFiles[outputFiles.length-1];
	}

	public void saveResultsToFiles() throws IOException {
		writeMainFile();
		writeHistogramFile();
		//writeDetailsFile();
		createZip();
	}
	
	public void createZip() throws IOException {
		FileOutputStream fos = new FileOutputStream(outputFiles[outputFiles.length-1]);
		ZipOutputStream zos = new ZipOutputStream(fos);
		
		for(File f:outputFiles) {
			// the last one is the zip file itself 
			if (f.getName().endsWith(".zip"))
				continue;
			ZipEntry ze = new ZipEntry(f.getName());
			zos.putNextEntry(ze);
			FileInputStream in = new FileInputStream(f);
			
			int len;
			byte[] buffer = new byte[1024];
			while ((len = in.read(buffer)) > 0) {
				zos.write(buffer, 0, len);
			}
			in.close();
			zos.closeEntry();
		}			
		zos.close();
	}
	
	void writeMainFile() throws IOException {
		PrintStream ps = null;
		try {
			ps = new PrintStream(outputFiles[0]);
			ps.format("Impedance (inv. minutes),%.4f%n%n",scenario.impedance);
			getGlobalStats(ps);
			getUnitsData(ps);
		} finally {
			if (ps!=null)
				ps.close();
		}
	}

	void writeHistogramFile() throws IOException {
		MyHistogram hist = new MyHistogram(scenario.travelTimeBefore,scenario.travelTimeAfter);
		hist.saveToFile(outputFiles[1]);
	}

	void writeDetailsFile() throws IOException {
	}

	static double[] stats(double[] data) {
		double[] results = new double[5];
		// Sum
		results[0] = 0d;
		// Average
		results[1] = 0d;
		// Min
		results[2] = Double.MAX_VALUE;
		// Max 
		results[3] = Double.MIN_VALUE;
		// Std Dev
		results[4] = 0d;
		
		// calculate sum, max and mean
		for(double d: data) {
			results[0]+=d;
			if (d<results[2])
				results[2] = d;
			if (d>results[3])
				results[3] = d;
		}
		// calculate avg
		results[1]= results[0]/data.length;
		
		for(double d: data) {
			// add the squares of the differences to the average
			results[4] += Math.pow(d-results[1], 2);
		}
		// finish std dev calculation
		results[4]/=data.length;
		results[4] = Math.sqrt(results[4]);
		
		return results;
	}

	static double[] stats(int[] data) {
		double[] results = new double[5];
		// Sum
		results[0] = 0d;
		// Average
		results[1] = 0d;
		// Min
		results[2] = Double.MAX_VALUE;
		// Max 
		results[3] = Double.MIN_VALUE;
		// Std Dev
		results[4] = 0d;
		
		// calculate sum, max and mean
		for(double d: data) {
			results[0]+=d;
			if (d<results[2])
				results[2] = d;
			if (d>results[3])
				results[3] = d;
		}
		// calculate avg
		results[1]= results[0]/data.length;
		
		for(double d: data) {
			// add the squares of the differences to the average
			results[4] += Math.pow(d-results[1], 2);
		}
		// finish std dev calculation
		results[4]/=data.length;
		results[4] = Math.sqrt(results[4]);
		
		return results;
	}

	
	static String lineHeader() {
		StringJoiner joiner = new StringJoiner(",");
		joiner.add("Fips").add("Accessibility before").add("Accessibility after");
		joiner.add("Population before").add("Population after");
		joiner.add("Jobs before").add("Jobs after");
		return joiner.toString();		
	}
	
	void lineOutput(PrintStream ps,int i) {
		ps.format("%d,%.4f,%.4f,%d,%d,%d,%d%n", 
				scenario.unitsBeforeData.getColumnAsInts(1)[i],
				accessBefore[i],accessAfter[i],
				populationDataBefore[i],populationDataAfter[i],
				employmentDataBefore[i],employmentDataAfter[i]);
	}	

	void getUnitsData(PrintStream ps) {
		ps.println(lineHeader());
		for(int i=0;i<scenario.nbUnits;i++)
			lineOutput(ps,i);
		ps.println();
		
		String[] statsName = new String[]{"SUM","AVG","MIN","MAX","STD DEV"};
		double[] statsAccessBefore = stats(accessBefore);
		double[] statsAccessAfter = stats(accessAfter);
		double[] statsPopulationBefore = stats(populationDataBefore);
		double[] statsPopulationAfter = stats(populationDataAfter);
		double[] statsEmploymentBefore = stats(employmentDataBefore);
		double[] statsEmploymentAfter = stats(employmentDataAfter);
				
		for(int i=0;i<5;i++)
			ps.format("%s,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f%n", 
				statsName[i],
				statsAccessBefore[i],statsAccessAfter[i],
				statsPopulationBefore[i],statsPopulationAfter[i],
				statsEmploymentBefore[i],statsEmploymentAfter[i]);
		ps.println();
		ps.format("SUM(ACC*POP),%.4f,%.4f%n", 
				sumAiPiBefore,sumAiPiAfter);
	}		
	
	void getGlobalStats(PrintStream ps) {
		ps.format("Per capita accessibility before,%.4f%n%n",perCapitaAccessBefore);
		ps.format("Per capita accessibility after,%.4f%n%n",perCapitaAccessAfter);
	}
}
