package edu.umich.elasticity.nbmodel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringJoiner;

public class TabularData {
	static String separator = "\t";
	int nbOfDataLines,nbOfColumns;
	boolean hasHeaderLine = true;
	ArrayList<String[]> dataLines;
	String[] arrayOfHeaders;
	HashMap<String, Integer> colNameToIndex;
	Number[][] data;
	
	public TabularData(BufferedReader reader) throws  NumberFormatException,IOException {
		dataLines = new ArrayList<String[]>();
		String line;
		// first line becomes array of headers
		String headerLine = reader.readLine();
		
		// detect if , or \t is the separator
		arrayOfHeaders = headerLine.split(separator);
		if (arrayOfHeaders.length<2) {
			separator = ",";
			arrayOfHeaders = headerLine.split(separator);
			if (arrayOfHeaders.length<2) {
				throw new IOException("Either the wrong separator is used or "
						+ "there are not enough columns in the file");				
			}			
		}
		
		// the number of headers will be taken to be the number of columns
		nbOfColumns = arrayOfHeaders.length;
	
		// map headers to indices
		colNameToIndex = new HashMap<String,Integer>();
		for(int i=0;i<nbOfColumns;i++) 
			colNameToIndex.put(arrayOfHeaders[i],i);
		
		while ((line = reader.readLine()) != null)
			dataLines.add(line.split(separator));
		reader.close();
		
		nbOfDataLines = dataLines.size();
		
		
		// I put the column first so that I can extract columns as arrays easily
		data = new Number[nbOfColumns][nbOfDataLines];
		
		for(int row=0;row<nbOfDataLines;row++) {
			for(int col=0;col<dataLines.get(row).length;col++) {
				// remove potential quotes around number
				String numberAsString = dataLines.get(row)[col].replace("\"", "");
				try {
					data[col][row] = Integer.valueOf(numberAsString);
				} catch (NumberFormatException e1) {
					// if this fails the exception is thrown by the method
					try {
						data[col][row] = Long.valueOf(numberAsString);
					} catch (NumberFormatException e2) {
							data[col][row] = Float.valueOf(numberAsString);						
					}
				}
			}
		}
	}
	
	Number[] getColumn(int oneBasedIndex) {
		return data[oneBasedIndex-1];
	}

	int[] getColumnAsInts(int oneBasedIndex) {
		int[] array = new int[nbOfDataLines];
		for(int i=0;i<nbOfDataLines;i++) {
			array[i] = data[oneBasedIndex-1][i].intValue();
		}
		return array;
	}

	// assume that the points are in the right order in columns 1 and 2 (1,1),(1,2)...(1,n),(2,1),(2,2)....
	float[][] getMatrix(int[] points,int iCol,int jCol,int valCol) throws InvalidFileStructureException {
		float[][] matrix = new float[points.length][points.length];
		for(int i=0;i<points.length;i++)
			for(int j=0;j<points.length;j++) {
				int dataRow = i*points.length+j;
				if (data[iCol][dataRow].intValue() != points[i] ||
						data[jCol][dataRow].intValue()!=points[j]) 
					throw new InvalidFileStructureException(String.format("Expected (%s,%s) found (%s,%s)",
							points[i],points[j],
							data[iCol][dataRow].intValue(),data[jCol][dataRow].intValue()),dataRow+1);
				matrix[i][j]=data[valCol][dataRow].floatValue();
			}
		return matrix;
	}
	
	/* Keep only certain columns */
	void filterToFile(int[] columnsToKeep, File f) throws IOException {
		Number[][] filteredData = new Number[columnsToKeep.length][nbOfDataLines];
		for(int i=0;i<columnsToKeep.length;i++) {
			filteredData[i] = data[columnsToKeep[i]];
		}
		StringJoiner sj = new StringJoiner(separator);
		FileWriter fw = new FileWriter(f);
		
		for(int i=0;i<columnsToKeep.length;i++)
			sj.add(arrayOfHeaders[columnsToKeep[i]]);
		fw.write(sj.toString()+ "\n");
		
		for(int row=0;row<filteredData[0].length;row++) {
			sj = new StringJoiner(separator);
			for (int col=0;col<filteredData.length;col++) {
				sj.add(String.valueOf(filteredData[col][row]));
			}
			fw.write(sj.toString()+"\n");
		}
		fw.close();
	}
	
	static void printMatrix(Number[][] matrix) {
		int maxPrint = 10;
		for(int i=0;i<Math.min(maxPrint,matrix.length);i++) {
			for(int j=0;j<Math.min(maxPrint,matrix[i].length);j++)
				System.out.print(matrix[i][j]+",");
			System.out.println("");
		}
	}	
}
