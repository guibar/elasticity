package edu.umich.elasticity.nbmodel;

public class InvalidFileStructureException extends Exception {
	int problemLineNumber;
	
	public InvalidFileStructureException(String message,int lineNb) {
		super(message);
		this.problemLineNumber = lineNb;
	}
}
