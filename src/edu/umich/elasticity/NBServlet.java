package edu.umich.elasticity;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.umich.elasticity.model.Scenario;
import edu.umich.elasticity.nbmodel.NBScenario;

/**
 * Servlet for Build Scenarios
 */
// @WebServlet("/MainServlet")
@MultipartConfig
public class NBServlet extends HttpServlet {
	private static final long serialVersionUID = 3427067916096169575L;

	public NBServlet() {
		super();
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    response.setContentType("text/html;charset=UTF-8");
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Job Submission Response</title>");
        out.println("</head>");
        out.println("<body>");

        // form Parameters
		String emailP = request.getParameter("email");
		String impedanceP = request.getParameter("impedance");
		
		boolean errorsDetected = false;
		double impedance=Scenario.defaultImpedance;
		
		try {
			// convert impedance to inverse seconds
			impedance = impedanceP.equals("")?Scenario.defaultImpedance:(Double.valueOf(impedanceP));
		} catch (NumberFormatException e) {
			errorsDetected = true;
			out.append("Impedance should be a number between 0.05 and 0.5<br>\n");
		}
		
		if (!errorsDetected) {	
			try {
				InputStream[] unitsIS = new InputStream[2];
				unitsIS[0] = request.getPart("unitsFileBefore").getInputStream();
				unitsIS[1] = request.getPart("unitsFileAfter").getInputStream();
				InputStream[] timesIS = new InputStream[2];
				timesIS[0] = request.getPart("timesFileBefore").getInputStream();
				timesIS[1] = request.getPart("timesFileAfter").getInputStream();

				NBScenario scenario = new NBScenario(emailP, unitsIS, timesIS,impedance);
				out.append("Your job has been successfully submitted with the id: " + scenario.directory.getName());
				out.append("<br>\n");
				out.append("When the job is completed, you will receive an email with the results.");
				
			} catch (Exception e) {
				out.append(e.getMessage());
			}
		    out.println("</body>");
		    out.println("</html>");
		}
	}
}
